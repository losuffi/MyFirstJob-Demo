#Rome-SLG-DevelopmentTips：

## 1. Event批量移除
> 当一个event被类外多个对象同时监听时，一个一个的删除十分不便，在网上找到一种利用反射批量移除的方法。

```c
foreach(var c in eventHandle.GetInvocationList()){
	typeof(eventHandleOwnerClass).GetEvent("eventHandle").
RemoveEventHandler(this,c);
}
```
其中eventHandleOwnerClass 为event的所属类，eventHandle 为一个event对象。

> 可研究方向：typeof()的用法。

##2. Event的调用方式

当在一个监听方法中，绑定另一个方法。如
```
class test{
	public event Action t;
	public void work(){
		if(t!=null)
			t();
	}
}
--------------------------
	test t=new test;
	test.t+=delegate{
		console.write("0");
		test.t+=delegate{
			console.write("1");
		};
	};
	t.work();
```
会输出01 而不是0

> 似乎不是这样，Trade回合多跑，加flag正常什么问题？？？？