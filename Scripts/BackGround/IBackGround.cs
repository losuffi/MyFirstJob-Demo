﻿using UnityEngine;
using System.Collections;
using System;
public abstract class IBackGround{
    public int Res;
    public abstract bool Response();
    public int RegInitTime;
    public int CostTime;
    public event Action<int> ed;
    public virtual void End()
    {
        if (ed != null)
        {
            ed(Res);
        }
    }
    public bool IsPause; //暂停中断。
    public bool IsEnd; //结束中断。
}
