﻿using UnityEngine;
using System.Collections;

public class TimeControl : MonoBehaviour {
    struct GameData
    {
        public int InitYear;
        public int InitMon;
        public int InitDay;
    }
    public static TimeControl Ins;
    private void Awake()
    {
        Ins = this;
    }
    private GameData gd;
    public int TimeSpeed;
    public UILabel Text;
    public GameObject EndButton;
    public delegate void Endp();
    public event Endp ep;
    private void Start()
    {
        gd.InitYear = 0;
        gd.InitMon = 0;
        gd.InitDay = 0;
        UIEventListener.Get(EndButton).onClick = new UIEventListener.VoidDelegate(ar =>
        {
            EndPo();
        });
        TimeDate.TimeSpeed = TimeSpeed;
    }
    //回合结束，时间调用接口
    public void EndPo()
    {
        if (GameStart.Ins.playerfamily.seniorSetCount > 0)
            return;
        TimeDate.TimeCount ++;
        Text.text = TimeConvert(TimeDate.TimeCount);
        if (ep!=null)
        {
            ep();
        }
        UIManager.Ins.SetState("UIS_Empty");
        UIManager.Ins.SetState("UIS_Boardcast");
    }
    public string TimeConvert(int timecount)
    {
        var T = timecount * TimeSpeed;
        gd.InitMon = 1 + (T % 360) / 30;
        gd.InitDay = 1 + (T % 360) % 30;
        gd.InitYear = -146 + T / 360;
        return gd.InitYear + "年" + gd.InitMon + "月";
    }
}
