﻿using System;
using UnityEngine;
using System.Collections;
public class GameStart : MonoBehaviour {
    public static GameStart Ins;
    public GameObject MaskView,UIlayer,Map;
    public event Action InitMethod; 
    private void Awake()
    {
        Ins = this;
    }
    public Family playerfamily;
    private void Start()
    {
        playerfamily = new Family();
        InitGame();
    }
    /// <summary>
    /// 一旦修改数据库，加载内容 需要修改初始化 函数的内容。
    /// </summary>
    /// <returns></returns>
    private void InitGame()
    {
        SingleValuePool.Ins.InPool();
        ArmyPool.Ins.InPool();
        CropPool.Ins.InPool();
        SpecialtyPool.Ins.InPool();
        AreaPool.Ins.InPool();
        FamilyPool.Ins.InPool();
        RelationPool.Ins.InPool();
        MotionPool.Ins.InPool();
        SituationPool.Ins.InPool();
        ArguePool.Ins.InPool();
        AreaRoad.Ins.InPool();
        LawPool.Ins.InPool();
        playerfamily = FamilyPool.Ins.ReadPool(1) as Family;
        MaskView.SetActive(false);
        UIlayer.SetActive(true);
        Map.SetActive(true);
        InitAIFamily();
        InitArea0();
        BoardcastDate.Ins.Rec(SingleValuePool.Ins["SystemNoteMessageTitle"].StringValue, TimeDate.TimeCount, SingleValuePool.Ins["SystemNoteMessageChoseSenate"].StringValue, BoardMessage.MessageKind.Job);
        UIManager.Ins.SetState("UIS_Man");
        BackGroundManager.Ins.RegBGEvent(new BackProFamily());
        BackGroundManager.Ins.RegBGEvent(new EventEmitter());
        BackGroundManager.Ins.RegBGEvent(new BackProOthers());
    }
    void InitAIFamily()
    {
        Family p;
        for (int i = 1; i < FamilyPool.Ins.count + 1; i++)
        {
            p = FamilyPool.Ins.ReadPool(i) as Family;
            for(int j = 0; j < p.FStaff.Count; j++)
            {
                p.FStaff.GetPerson(j).StayArea = AreaPool.Ins.ReadPool(26) as Area;
            }
            if (p.FamilyID == playerfamily.FamilyID)
                continue;
            int c = UnityEngine.Random.Range(3, p.FStaff.Count);
            index = c;
            RndPerson(0, p.FStaff.Count, p);
            p.FStaff.Seniors[UnityEngine.Random.Range(0, p.FStaff.Seniors.Count)].Job = Person.JobKind.UmpiredSenior;
        }
        BackGroundJobs.Ins.ChangeWork();
    }
    void InitArea0()
    {
        for (int i = 1; i < AreaPool.Ins.Count + 1; i++)
        {
            var a = (AreaPool.Ins.ReadPool(i) as Area);
            if (a.country == null)
                a.country = AreaPool.Ins.Capital().country;
        }
    }
    //无重复，随机N（N为随机数）个随机数。
    private int index;
    void RndPerson(int start,int end,Family p)
    {
        if (start == end)
            return;
        if (index <= 0)
            return;
        int mid = UnityEngine.Random.Range(start, end);
        var per = p.FStaff.GetPerson(mid);
        per.Job = Person.JobKind.Senior;
        index--;
        RndPerson(start, mid, p);
        RndPerson(mid+1, end, p);
    }
    private void Update()
    {
        if (InitMethod != null)
        {
            InitMethod();
        }
    }
}
