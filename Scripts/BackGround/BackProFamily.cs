﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 家族的后台计算
/// </summary>
public class BackProFamily : IBackGround
{
    int index;
    int MinTimeCount = SingleValuePool.Ins["ElectionStartMonth"].IntValue; //竞选开始月
    int MaxTimeCount = SingleValuePool.Ins["ElectionEndMonth"].IntValue;    //竞选结束月
    public override bool Response()
    {
        index = 1;
        GameStart.Ins.playerfamily.FAsset.Cash += SingleValuePool.Ins["FamilyCashDrainOfRound"].IntValue; //每回合的资产消耗
        //-------政事不处理的惩罚-------//
        if (GameEventBox.Ins.Count > 0)
        {
            if (UIManager.Ins.states.ContainsKey("UIS_Sci"))
            {
                if (!(UIManager.Ins.states["UIS_Sci"] as UIS_Sci).IsJoined)
                {
                    GameStart.Ins.playerfamily.FAsset.CreditX += SingleValuePool.Ins["FamilyCreditXDrainOfRound"].IntValue;
                }
                (UIManager.Ins.states["UIS_Sci"] as UIS_Sci).IsJoined = false;
            }
            else
            {
                GameStart.Ins.playerfamily.FAsset.CreditX -= 5;
            }
        }
        //----------------------//
        var T = TimeDate.TimeCount * TimeControl.Ins.TimeSpeed;
        if ((1 + (T % 360) / 30) == MinTimeCount)
        {
            BoardcastDate.Ins.Rec(SingleValuePool.Ins["SystemNoteMessageTitle"].StringValue, TimeDate.TimeCount, SingleValuePool.Ins["StrMessageBoxElecNote"].StringValue, BoardMessage.MessageKind.Election);
        }
        else if((1 + (T % 360) / 30) == MaxTimeCount+1)
        {
            BoardcastDate.Ins.Rec(SingleValuePool.Ins["SystemNoteMessageTitle"].StringValue, TimeDate.TimeCount, SingleValuePool.Ins["StrMessageBoxNewOfficialWork"].StringValue, BoardMessage.MessageKind.Job);
            BackGroundJobs.Ins.ChangeWork();
        }
        //----------地产数据处理-------------//
        for(int i = 0; i < GameStart.Ins.playerfamily.FAsset.estates.Count; i++)
        {
            if (GameStart.Ins.playerfamily.FAsset.estates[i].IsWork)
            {
                if((1 + (T % 360) / 30) == SingleValuePool.Ins["CropAccountMonth"].IntValue)
                {
                    var monthes = ((TimeDate.TimeCount - GameStart.Ins.playerfamily.FAsset.estates[i].StartTIme) * TimeDate.TimeSpeed) / 30;
                    var percent = GameStart.Ins.playerfamily.FAsset.estates[i].cro.CropId == GameStart.Ins.playerfamily.FAsset.estates[i].area.SuitableId ? GameStart.Ins.playerfamily.FAsset.estates[i].area.AreaCropPercent : 1;
                    GameStart.Ins.playerfamily.FAsset.Cash += (int)(monthes * percent * GameStart.Ins.playerfamily.FAsset.estates[i].cro.Earning);
                    GameStart.Ins.playerfamily.FAsset.estates[i].StartTIme = TimeDate.TimeCount;
                }
            }
            GameStart.Ins.playerfamily.FAsset.Cash -= GameStart.Ins.playerfamily.FAsset.estates[i].cro.Consume;
            GameStart.Ins.playerfamily.FAsset.estates[i].Value += GameStart.Ins.playerfamily.FAsset.estates[i].cro.Consume;
        }
        //----------经济危机，面对措施-------------//
        if (GameStart.Ins.playerfamily.FAsset.Cash <= 0)
        {
            //var count = GameStart.Ins.playerfamily.FAsset.estates.Count;
            BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxEcoCrisisTitle"].StringValue, TimeDate.TimeCount, SingleValuePool.Ins["StrMessageBoxEcoCrisis"].StringValue, BoardMessage.MessageKind.Estate);
            //for (int i=0;i< count&& GameStart.Ins.playerfamily.FAsset.Cash<=0; i++)
            //{
            //    GameStart.Ins.playerfamily.FAsset.Cash += GameStart.Ins.playerfamily.FAsset.estates[i].Value;
            //    GameStart.Ins.playerfamily.FAsset.estates.Remove(GameStart.Ins.playerfamily.FAsset.estates[i]);
            //}
            //if(GameStart.Ins.playerfamily.FAsset.Cash <= 0)
            //{
            //    Debug.Log("GameOver!");
            //}
        }
        //---------人物回合触发-----------//
        while (FamilyPool.Ins.ReadPool(index) != null)
        {
            Person();
            index++;
        }
        return false;
    }

    private void Person()
    {
        var cc = FamilyPool.Ins.ReadPool(index) as Family;
        for(int i = 0; i < cc.FStaff.Count; i++)
        {
            if (cc.FStaff.GetPerson(i) == null)
                continue;
            cc.FStaff.GetPerson(i).UpdateAge();
            cc.FStaff.GetPerson(i).UpdateTask();
        }
    }
}
