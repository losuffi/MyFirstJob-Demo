﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class BackGroundJobs {
    private int index;
    private static BackGroundJobs ins;
    private BackGroundJobs() { }
    public static BackGroundJobs Ins
    {
        get
        {
            if (ins == null)
                ins = new BackGroundJobs();
            return ins;
        }
    }
    public List<Person> Umpires = new List<Person>();
    public List<Person> NewUmpires = new List<Person>();
    public Person Consul, NewConsul;
    private List<Area> Province = new List<Area>();
    private Area Capital;
    public  delegate void tt();
    public event tt cw;
    public void Umpire(Person a)
    {
        a.Idel = false;
        NewUmpires.Add(a);
    }
    public void BeConsul(Person a)
    {
        NewConsul = a;
    }
    public void ChangeWork()
    {
        //构建行省集合
        Province.Clear();
        for (int i = 1; i < AreaPool.Ins.Count+1; i++)
        {
            if((AreaPool.Ins.ReadPool(i) as Area).Subjection == 0&& !(AreaPool.Ins.ReadPool(i) as Area).IsTheFalledArea)
            {
                Province.Add((AreaPool.Ins.ReadPool(i) as Area));
            }
            else if((AreaPool.Ins.ReadPool(i) as Area).Subjection == 1)
            {
                Capital = (AreaPool.Ins.ReadPool(i) as Area);
            }
        }
        //老裁判官卸任。
        for (int i = 0; i < Umpires.Count; i++)
        {
            if (Umpires[i].Job == Person.JobKind.ConsulUmpire)
            {
                Umpires[i].Job = Person.JobKind.ConsuledSenior;
                Umpires[i].StayArea = AreaPool.Ins.ReadPool(26) as Area;
            }
            else
            {
                Umpires[i].Job = Person.JobKind.UmpiredSenior;
                Umpires[i].StayArea = AreaPool.Ins.ReadPool(26) as Area;
            }
        }
        if (Consul != null)
        {
            Consul.Job = Person.JobKind.ConsuledSenior; //老执政官卸任
            Umpire(Consul);
        }
        if (NewConsul == null)
        {
            while (true)
            {
                var rnd = (int)Random.Range(1, FamilyPool.Ins.count + 1);
                if (rnd == GameStart.Ins.playerfamily.FamilyID)
                    continue;
                var sfm = FamilyPool.Ins.ReadPool(rnd) as Family;
                var person = sfm.FStaff.UmpiredSenior[Random.Range(0, sfm.FStaff.UmpiredSenior.Count)];
                if (!person.Idel)
                    continue;
                else
                {
                    NewConsul = person;
                    break;
                }
            }
        }            //在Ai里选执政官。
        NewConsul.Job = Person.JobKind.Consul;
        Consul = NewConsul; //新执政官上台。
        Capital.Umpire = Consul;
        Consul.StayArea = Capital;
        Consul.TaskGO(12, Person.TaskKind.Official, Capital);
        BackGroundManager.Ins.RegBGEvent(new UmpiresWork(Consul));
        NewConsul = null;
        Umpires.Clear();
        //新裁判上台。
        for(int i = 0; i < NewUmpires.Count; i++)
        {
            if(NewUmpires[i].Job== Person.JobKind.ConsuledSenior)
            {
                NewUmpires[i].Job = Person.JobKind.ConsulUmpire;
            }
            else
            {
                NewUmpires[i].Job = Person.JobKind.Umpire;
            }
            BackGroundManager.Ins.RegBGEvent(new UmpiresWork(NewUmpires[i]));
            Umpires.Add(NewUmpires[i]);
        }
        NewUmpires.Clear();
        //如果裁判数目不够，则向ai 家族要人。
        if (Umpires.Count < Province.Count)
        {
            while (Umpires.Count < Province.Count)
            {
                var rnd = (int)Random.Range(1, FamilyPool.Ins.count+1);
                if (rnd == GameStart.Ins.playerfamily.FamilyID)
                    continue;
                else
                {
                    var sfm = FamilyPool.Ins.ReadPool(rnd) as Family;
                    if (sfm.FStaff.Seniors.Count < 1)
                        continue;
                    var person = sfm.FStaff.Seniors[Random.Range(0, sfm.FStaff.Seniors.Count)];
                    if (!person.Idel)
                    {
                        continue;
                    }
                    else
                    {
                        if (person.Job == Person.JobKind.ConsuledSenior)
                        {
                            continue;
                        }
                        else
                        {
                            person.Job = Person.JobKind.Umpire;
                        }
                        person.Job = Person.JobKind.Umpire;
                        BackGroundManager.Ins.RegBGEvent(new UmpiresWork(person));
                        Umpires.Add(person);
                    }
                }
            }
        }
        index = 0;
        //随机分配地区,从地区2开始分配，地区1为罗马
        if (cw != null)
            cw();
        RndArea(0, Province.Count);
    }
    void RndArea(int start,int end)
    {
        if (index > Umpires.Count)
            return;
        if (start == end)
        {
            return;
        }
        else
        {
            int mid = Random.Range(start, end);
            if(WarEventBackPro.WorldWarEvent.Find(ar=>ar.Leader.Equals( Umpires[index]))==null)
                Umpires[index].StayArea = Province[mid];
            Umpires[index].TaskGO(12, Person.TaskKind.Official, Province[mid]);
            Province[mid].Umpire = Umpires[index];
            index++;
            RndArea(start, mid);
            RndArea(mid + 1, end);
        }
    }
}
