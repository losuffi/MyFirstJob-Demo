﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class BackGroundManager : MonoBehaviour {
    public static BackGroundManager Ins;
    private void Awake()
    {
        Ins = this;
        backgrounds = new List<IBackGround>();
        garbage = new List<IBackGround>();
    }
    private List<IBackGround> backgrounds;
    private List<IBackGround> garbage;
    /// <summary>
    /// 注册后台计算类，被注册的类，将于每一个回合结束调用它的响应函数
    /// </summary>
    private void Start()
    {
        TimeControl.Ins.ep += Stimulate;
    }
    public void Stimulate()
    {
        for(int i=0;i<backgrounds.Count;i++)
        {
            if (backgrounds[i].Response())
            {
                backgrounds[i].End();
                garbage.Add(backgrounds[i]);
            }
        }
        foreach(var z in garbage)
        {
            backgrounds.Remove(z);
        }
        garbage.Clear();
    }
    public void RegBGEvent(IBackGround t)
    {
        t.RegInitTime = TimeDate.TimeCount;
        backgrounds.Add(t);
    }
}
