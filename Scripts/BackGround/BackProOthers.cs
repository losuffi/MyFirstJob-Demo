﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 1.一些常量的浮动，如贸易价格变动
/// </summary>
public class BackProOthers : IBackGround
{
    int TradeValueChangeMonthCD = SingleValuePool.Ins["TradeValueChangeMonthCD"].IntValue;
    int TaxTimeCount = SingleValuePool.Ins["EcoCountryTaxMonth"].IntValue;// 收税月份
    int ProletariatUpdateCd = SingleValuePool.Ins["ProletariatUpdateCd"].IntValue;    //无产游民 变化CD
    int GrainConsumeMonth = SingleValuePool.Ins["GrainConsumeMonth"].IntValue; //发粮月份
    int GrainBuyMonth = SingleValuePool.Ins["GrainBuyMonth"].IntValue; //买粮月份
    int ArmyMilitaryPayMonth = SingleValuePool.Ins["ArmyMilitaryPayMonth"].IntValue;//发军饷的月份
    int ShipConsumeMonth = SingleValuePool.Ins["ShipConsumeMonth"].IntValue;//船队要钱的月份
    int TimeMonthCache, ProletariatUpdateCdCache;
    public BackProOthers()
    {
        TimeMonthCache = 0;
        ProletariatUpdateCdCache = 0;
    }
    public override bool Response()
    {
        //-------提案动态改变--------//
        //----出兵---//
        if (AreaPool.Ins.Capital().Umpire.StayArea.Equals(AreaPool.Ins.Capital()))
        {
            (MotionPool.Ins[2] as Motion).Nature = 0;
            (MotionPool.Ins[23] as Motion).Nature = 8;
        }
        else
        {
            (MotionPool.Ins[2] as Motion).Nature = 1;
            (MotionPool.Ins[23] as Motion).Nature = 1;
        }
        //-------贸易价格浮动--------//
        if (TimeDate.TimeCount - TimeMonthCache >= TradeValueChangeMonthCD)
        {
            for (int i = 1; i < SpecialtyPool.Ins.Count + 1; i++)
            {
                var spe = (SpecialtyPool.Ins.ReadPool(i) as Specialty);
                spe.SpecialtyValueReal = UnityEngine.Random.Range(spe.SpecialtyValueMin, spe.SpecialtyValueMax + 1);
            }
            TimeMonthCache = TimeDate.TimeCount;
        }
        //---------国家税收变化---------//
        var T = TimeDate.TimeCount * TimeControl.Ins.TimeSpeed;
        for (int i = 1; i < AreaPool.Ins.Count + 1; i++)
        {
            var a = (AreaPool.Ins.ReadPool(i) as Area);
            if (a.country == null || a.IsTheFalledArea)
                continue;
            if (a.Subjection < 2)
            {
                a.TaxTotal += (int)(a.Tax * (1 - a.Laws.Find(ar => ar.ID == 4).Value * 0.01));
            }
            else
            {
                a.TaxTotal += (int)(a.Tax * (1 - a.AreaTaxPercent));
            }
        }
        if ((1 + (T % 360) / 30) == TaxTimeCount)
        {
            for (int i = 1; i < AreaPool.Ins.Count + 1; i++)
            {
                var a = (AreaPool.Ins.ReadPool(i) as Area);
                if (a.country == null||a.IsTheFalledArea)
                    continue;
                a.country.Cash += a.TaxTotal;
                a.TaxTotal = 0;
            }
        }
        //-------国家无产游民数目变化---------------//
        //if (TimeDate.TimeCount - ProletariatUpdateCdCache >= ProletariatUpdateCd)
        //{
        //    var cap = (AreaPool.Ins.ReadPool(26) as Area);
        //    cap.country.PersonCount += SingleValuePool.Ins["ProletariatUpdateCount"].IntValue;
        //}
        //--------国家发军饷 以及补充军团------//
        if ((1 + (T % 360) / 30) == ArmyMilitaryPayMonth)
        {
            var cap = (AreaPool.Ins.ReadPool(26) as Area);
            int bx = 0;
            for(int i = 1; i < AreaPool.Ins.Count + 1; i++)
            {
                var area = AreaPool.Ins.ReadPool(i) as Area;
                if (area.Subjection < 2)
                {
                    bx += (int)area.Laws.Find(ar => ar.ID == 2).Value * area.armys.Count;
                }
            }
            if (cap.country.Cash < bx)
            {
                BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxCountryNoteTitle"].StringValue, TimeDate.TimeCount, SingleValuePool.Ins["StrMessageBoxPayArmyNoMoney"].StringValue, BoardMessage.MessageKind.None);
            }
            else
            {
                cap.country.Cash -= bx;
                BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxCountryNoteTitle"].StringValue, TimeDate.TimeCount, SingleValuePool.Ins["StrMessageBoxPayArmy"].StringValue, BoardMessage.MessageKind.None);
            }
        }
        //--------国家船队消耗--------//
        //if ((1 + (T % 360) / 30) == ShipConsumeMonth)
        //{
        //    var cap = (AreaPool.Ins.ReadPool(26) as Area);
        //    var bx = SingleValuePool.Ins["ShipConsumeValue"].IntValue;
        //    if (cap.country.Cash < bx)
        //    {
        //        BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxCountryNoteTitle"].StringValue, TimeDate.TimeCount, SingleValuePool.Ins["StrMessageBoxPayShipNoMoney"].StringValue, BoardMessage.MessageKind.None);
        //    }
        //    else
        //    {
        //        cap.country.Cash -= bx;
        //        BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxCountryNoteTitle"].StringValue, TimeDate.TimeCount, SingleValuePool.Ins["StrMessageBoxPayShip"].StringValue, BoardMessage.MessageKind.None);
        //    }
        //}
        //--------国家粮食消耗------------//
        //if ((1 + (T % 360) / 30) == GrainConsumeMonth)
        //{
        //    var cap = (AreaPool.Ins.ReadPool(26) as Area);
        //    var bx = int.Parse(LawPool.Ins[1].Values[1].ToString()) * int.Parse(LawPool.Ins[1].Values[2].ToString());
        //    if (cap.country.Rice < bx)
        //    {
        //        BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxCountryNoteTitle"].StringValue, TimeDate.TimeCount, SingleValuePool.Ins["StrMessageBoxGrainConsumeNotEnough"].StringValue, BoardMessage.MessageKind.None);
        //    }
        //    else
        //    {
        //        cap.country.Rice -= bx;
        //        cap.country.Cash += bx * int.Parse(LawPool.Ins[1].Values[0].ToString());
        //        BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxCountryNoteTitle"].StringValue, TimeDate.TimeCount, SingleValuePool.Ins["StrMessageBoxGrainConsume"].StringValue, BoardMessage.MessageKind.None);
        //    }
        //}
        //-------国家补充粮食---------//
        //if ((1 + (T % 360) / 30) == GrainBuyMonth)
        //{
        //    var cap = (AreaPool.Ins.ReadPool(26) as Area);
        //    var bx = int.Parse(LawPool.Ins[1].Values[1].ToString()) * int.Parse(LawPool.Ins[1].Values[2].ToString());
        //    if (cap.country.Cash >= bx)
        //    {
        //        cap.country.Rice += bx;
        //        cap.country.Cash -= (bx * SingleValuePool.Ins["GrainBuyValue"].IntValue);
        //        BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxCountryNoteTitle"].StringValue, TimeDate.TimeCount, SingleValuePool.Ins["StrMessageBoxBuyGrain"].StringValue, BoardMessage.MessageKind.None);
        //    }
        //    else
        //    {
        //        BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxCountryNoteTitle"].StringValue, TimeDate.TimeCount, SingleValuePool.Ins["StrMessageBoxBuyGrainNoMoney"].StringValue, BoardMessage.MessageKind.None);
        //    }
        //}
        return false;
    }
}
