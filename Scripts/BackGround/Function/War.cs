﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
public class War : IBackGround
{
    Area TrigArea;
    bool IsBattleStart;
    WarEventBackPro Attack, Defense, ResW;
    public int attacksub, defensesub, attackarmysub, defensearmysub;
    bool AisL, BisL;
    int Timer;
    public override bool Response()
    {
        if (!IsBattleStart)
        {
            IsPause = false;
            return false;
        }
        else
        {
            if (IsPause)  //暂停中断。
                return false;
            if (IsEnd) //结束中断
            {
                Res = 5;
                return true;
                //TODO:中途结束战斗，结束。
            }
            Timer++;
            double a, b;
            a = 0;
            b = 0;
            ZUck();
            for(int i = 0; i < Attack.armys.Count; i++)
            {
                Attack.armys[i].State = Army.states.Battle;
                a += Attack.Leader.Force * UnityEngine.Random.Range((float)Attack.armys[i].ArmyAttiMin, (float)Attack.armys[i].ArmyAttiMax) * Attack.armys[i].ArmyValue;
            }
            for(int j = 0; j < Defense.armys.Count; j++)
            {
                Defense.armys[j].State = Army.states.Battle;
                b += Defense.Leader.Force * UnityEngine.Random.Range((float)Defense.armys[j].ArmyAttiMin, (float)Defense.armys[j].ArmyAttiMax) * Defense.armys[j].ArmyValue;
            }
            if (a > b)
            {
                Attack.IsWinner = true;
                Defense.IsWinner = false;
                attacksub = (int)(Attack.TotalCount * ((float)SingleValuePool.Ins["WarWastagePercentWinner"].IntValue / 100) *
                    ((float)UnityEngine.Random.Range(SingleValuePool.Ins["WarWastagePercentWinnerRndMin"].IntValue, SingleValuePool.Ins["WarWastagePercentWinnerRndMax"].IntValue) / 100)
                    );
                defensesub = (int)(Defense.TotalCount * ((float)SingleValuePool.Ins["WarWastagePercentLoser"].IntValue / 100) *
                    ((float)UnityEngine.Random.Range(SingleValuePool.Ins["WarWastagePercentLoserRndMin"].IntValue, SingleValuePool.Ins["WarWastagePercentLoserRndMax"].IntValue) / 100)
                    );
            }
            else
            {
                Attack.IsWinner = false;
                Defense.IsWinner = true;
                attacksub = (int)(Attack.TotalCount * ((float)SingleValuePool.Ins["WarWastagePercentLoser"].IntValue / 100) *
                    ((float)UnityEngine.Random.Range(SingleValuePool.Ins["WarWastagePercentLoserRndMin"].IntValue, SingleValuePool.Ins["WarWastagePercentLoserRndMax"].IntValue) / 100)
                    );
                defensesub = (int)(Defense.TotalCount * ((float)SingleValuePool.Ins["WarWastagePercentWinner"].IntValue / 100) *
                           ((float)UnityEngine.Random.Range(SingleValuePool.Ins["WarWastagePercentWinnerRndMin"].IntValue, SingleValuePool.Ins["WarWastagePercentWinnerRndMax"].IntValue) / 100)
                            );

            }
            attackarmysub = Attack.armys.Count;
            defensearmysub = Defense.armys.Count;
            bool aa = Sub(attacksub, Attack);
            bool bb = Sub(defensesub, Defense);
            attackarmysub = attackarmysub - Attack.armys.Count;
            defensearmysub = defensearmysub - Defense.armys.Count;
            Attack.SubArmyCount = attackarmysub;
            Attack.SubCount = attacksub;
            Defense.SubArmyCount = defensearmysub;
            Defense.SubCount = defensesub;
            if (aa & bb)
            {
                BoardcastDate.Ins.Rec("战争信息", TimeDate.TimeCount, Defense.Leader.StayArea.AreaName + "的战争有了新的情报", BoardMessage.MessageKind.War, Attack, Defense, false);
                return false;
            }
            else
            {
                //:TODO:结果处理.援军的各回各家。。。
                if (aa && !bb)
                {
                    string str = Defense.Leader.StayArea.AreaName + "的战争结束了，进攻方取得了胜利";
                    if (UnityEngine.Random.Range(0, (Attack.Leader.Force + Timer) * 10) <= 10)
                    {
                        AisL = false;
                        str += Attack.Leader.Name + "死于本战";
                        Attack.Leader.Dead();
                        UnitPool.Ins.Push(Attack.leadReal);
                    }
                    if (UnityEngine.Random.Range(0, ((Defense.Leader.Force + Timer) / 2) * 10) <= 10)
                    {
                        BisL = false;
                        str += Defense.Leader.Name + "死于本战";
                        Defense.Leader.Dead();
                        UnitPool.Ins.Push(Defense.leadReal);
                    }
                    BoardcastDate.Ins.Rec("战争信息", TimeDate.TimeCount, str, BoardMessage.MessageKind.War, Attack, Defense, true);
                    for (int i = 0; i < Defense.armys.Count; i++)
                    {
                        Defense.armys[i].ArmyMember = 0;
                        UnitPool.Ins.Push(Defense.armyReal[i]);
                    }
                }
                else if (bb && !aa)
                {
                    string str = Defense.Leader.StayArea.AreaName + "的战争结束了，防守方取得了胜利";
                    if (UnityEngine.Random.Range(0, (Defense.Leader.Force + Timer) * 10) <= 10)
                    {
                        BisL = false;
                        str += Defense.Leader.Name + "死于本战";
                        Defense.Leader.Dead();
                        UnitPool.Ins.Push(Defense.leadReal);
                    }
                    if (UnityEngine.Random.Range(0, ((Attack.Leader.Force + Timer) / 2) * 10) <= 10)
                    {
                        AisL = false;
                        str += Attack.Leader.Name + "死于本战";
                        Attack.Leader.Dead();
                        UnitPool.Ins.Push(Attack.leadReal);
                    }
                    //else
                    //{
                    //    Attack.leadReal.GetComponent<ManUnit>().MoveOn(Attack.Leader.StayArea, Attack.armys[0].OwnerArea, 1);
                    //}
                    BoardcastDate.Ins.Rec("战争信息", TimeDate.TimeCount, str, BoardMessage.MessageKind.War, Attack, Defense, true);
                    for (int i = 0; i < Attack.armys.Count; i++)
                    {
                        Attack.armys[i].ArmyMember = 0;
                        UnitPool.Ins.Push(Attack.armyReal[i]);
                    }
                }
                else
                {
                    string str = Defense.Leader.StayArea.AreaName + "的战争结束了，两败俱伤";
                    if (UnityEngine.Random.Range(0, ((Defense.Leader.Force + Timer) / 2) * 10) <= 10)
                    {
                        BisL = false;
                        str += Defense.Leader.Name + "死于本战";
                        Defense.Leader.Dead();
                        UnitPool.Ins.Push(Defense.leadReal);
                    }
                    if (UnityEngine.Random.Range(0, ((Attack.Leader.Force + Timer) / 2) * 10) <= 10)
                    {
                        AisL = false;
                        str += Attack.Leader.Name + "死于本战";
                        Attack.Leader.Dead();
                        UnitPool.Ins.Push(Attack.leadReal);
                    }
                    BoardcastDate.Ins.Rec("战争信息", TimeDate.TimeCount, str, BoardMessage.MessageKind.War, Attack, Defense, true);
                    for (int i = 0; i < Attack.armys.Count; i++)
                    {
                        Attack.armys[i].ArmyMember = 0;
                        UnitPool.Ins.Push(Attack.armyReal[i]);
                    }
                    for (int i = 0; i < Defense.armys.Count; i++)
                    {
                        Defense.armys[i].ArmyMember = 0;
                        UnitPool.Ins.Push(Defense.armyReal[i]);
                    }
                }
                GetRes(aa, bb);
                return true;
            }
        }
    }
    public War(WarEventBackPro a,WarEventBackPro b)
    {
        a.waring = this;
        b.waring = this;
        Attack = new WarEventBackPro();
        Defense = new WarEventBackPro();
        BoxWarEvent(a, Attack);
        IsBattleStart = false;
        a.leadReal.GetComponent<ManUnit>().MoveOn(a.Leader.StayArea, b.Leader.StayArea, 1);
        a.leadReal.GetComponent<ManUnit>().mp += a.MoveArmy;
        a.leadReal.GetComponent<ManUnit>().cp += delegate {
            b.Defense();
            BoxWarEvent(b, Defense);
            IsBattleStart = true;
            float zz = 2;
            float _x = ((Attack.armys.Count + Defense.armys.Count) / 5 + 2) * (zz / 2);
            Attack.leadReal.transform.position -= _x * (Attack.leadReal.transform.forward);
            Defense.leadReal.transform.LookAt(Attack.leadReal.transform);
            Defense.leadReal.transform.position -= _x * (Defense.leadReal.transform.forward);
            Attack.leadReal.transform.LookAt(Defense.leadReal.transform);
            MoveArmy(Attack, zz);
            MoveArmy(Defense, zz);
            Timer = 0;
            AisL = true;
            BisL = true;
            Attack.IsRoot = true;
            Defense.IsRoot = true;
            Res = 0;
            TrigArea = Defense.Leader.StayArea;
            TrigArea.IsBattleArea = true;
        };
        BackGroundManager.Ins.RegBGEvent(this);
    }      //传统从一个地方打另一个地方
    public War(WarEventBackPro a,WarEventBackPro b,bool t)  //自己区域打自己区域
    {
        a.waring = this;
        b.waring = this;
        Attack = new WarEventBackPro();
        Defense = new WarEventBackPro();
        BoxWarEvent(a, Attack);
        IsBattleStart = false;
        Attack.leadReal.GetComponent<ManUnit>().StayOn(Attack.Leader.StayArea);
        Attack.MoveArmy();
        b.Defense();
        BoxWarEvent(b, Defense);
        Defense.MoveArmy();
        IsBattleStart = true;
        float zz = 2;
        float _x = ((Attack.armys.Count + Defense.armys.Count) / 5 + 2) * (zz / 2);
        Attack.leadReal.transform.position -= _x * (Attack.leadReal.transform.forward);
        Defense.leadReal.transform.LookAt(Attack.leadReal.transform);
        Defense.leadReal.transform.position -= _x * (Defense.leadReal.transform.forward);
        Attack.leadReal.transform.LookAt(Defense.leadReal.transform);
        MoveArmy(Attack, zz);
        MoveArmy(Defense, zz);
        Timer = 0;
        AisL = true;
        BisL = true;
        Attack.IsRoot = true;
        Defense.IsRoot = true;
        Res = 0;
        TrigArea = Defense.Leader.StayArea;
        TrigArea.IsBattleArea = true;
        BackGroundManager.Ins.RegBGEvent(this);
    }
    void MoveArmy(WarEventBackPro temp,float c)
    {
        int RowNum = temp.armys.Count / 5 + 1;
        int RowedCount = 0;
        int CurrentI = 0;
        int zindex = 0;
        float N = c;
        while (RowNum > 0)
        {
            if (temp.armys.Count - RowedCount * 5 == 0)
                break;
            if ((temp.armys.Count - RowedCount * 5) >= 5)
            {
                zindex = 5;
            }
            else
            {
                zindex = (temp.armys.Count - RowedCount * 5) % 5;
            }
            zindex = zindex == 0 ? 5 : zindex;
            for (int i = 0; i < zindex; i++)
            {
                var _x = (-N + (5 - zindex) * N / 4) + i * N / 2;
                if (CurrentI >= temp.armyReal.Count)
                    break;
                temp.armyReal[CurrentI].transform.position = temp.leadReal.transform.position;
                temp.armyReal[CurrentI].transform.rotation = temp.leadReal.transform.rotation;
                temp.armyReal[CurrentI].transform.position += N / 2*RowNum * temp.armyReal[CurrentI].transform.forward;
                temp.armyReal[CurrentI].transform.Rotate(Vector3.up * 90);
                temp.armyReal[CurrentI].transform.position += _x * temp.armyReal[CurrentI].transform.forward;
                temp.armyReal[CurrentI].transform.Rotate(Vector3.up * -90);
                temp.armys[CurrentI].CurrentArea = temp.Leader.StayArea;
                CurrentI++;
            }
            RowedCount++;
            RowNum--;
        }
    }
    /// <summary>
    /// 平均分配损耗给每个军团 ，而不是现在的从第一个军团开始扣
    /// </summary>
    /// <param name="sub"></param>
    /// <param name="t"></param>
    /// <returns></returns>
    bool Sub(int sub,WarEventBackPro t)
    {
        if ((t.MaxCount*SingleValuePool.Ins["WarArmyNullPercent"].IntValue)/100 >(t.TotalCount-sub) )
            return false;
        WarEventBackPro p = t;

        while (p.Adder != null)
        {
            int cSub = sub * p.Adder.WarEventBackProWeight / t.WarEventBackProWeight;
            p.Adder.SubCount = cSub;
            p.Adder.SubArmyCount = p.Adder.armys.Count;
            while (cSub > 0)
            {

                if (p.Adder.armys[0].ArmyMember - cSub < 0)
                {
                    cSub -= t.armys[0].ArmyMember;
                    UnitPool.Ins.Push(p.Adder.armyReal[0]);
                    t.armys.Remove(p.Adder.armys[0]);
                    t.armyReal.Remove(p.Adder.armyReal[0]);
                    p.Adder.armys.RemoveAt(0);
                    p.Adder.armyReal.RemoveAt(0);
                }
                else
                {
                    p.Adder.armys[0].ArmyMember -= cSub;
                    break;
                }
            }
            p.Adder.SubArmyCount = p.Adder.SubArmyCount - p.Adder.armys.Count;
            p = p.Adder;
        }
        return true;
    }
    public void ZUck()
    {
        float zz = 2;
        float _x = ((Attack.armys.Count / 5 + Defense.armys.Count / 5) + 2) * (zz / 2);
        Attack.leadReal.transform.position += _x * (Attack.leadReal.transform.forward);
       // Attack.leadReal.GetComponent<Renderer>().material.color = Color.red;
        Defense.leadReal.transform.LookAt(Attack.leadReal.transform);
        Defense.leadReal.transform.position += _x * (Defense.leadReal.transform.forward);
        //Attack.leadReal.transform.LookAt(Defense.leadReal.transform);
        MoveArmy(Attack, zz);
        MoveArmy(Defense, zz);
        Attack.leadReal.transform.position -= _x * (Attack.leadReal.transform.forward);
        Defense.leadReal.transform.position -= _x * (Defense.leadReal.transform.forward);
        Defense.leadReal.transform.LookAt(Attack.leadReal.transform);
        Attack.leadReal.transform.LookAt(Defense.leadReal.transform);
        MoveArmy(Attack, zz);
        MoveArmy(Defense, zz);
    }
    void BoxWarEvent(WarEventBackPro r,WarEventBackPro t)
    {
        t.waring = r.waring;
        t.Leader = r.Leader;
        t.leadReal = r.leadReal;
        t.armyReal.AddRange(r.armyReal);
        t.armys.AddRange(r.armys);
        t.MaxCount += r.MaxCount;
        t.MaxArmyCount += r.MaxArmyCount;
        t.WarEventBackProWeight += r.WarEventBackProWeight;
        t.Adder = r;
        r.Root = t;
    }
    /// <summary>
    /// 获取哪支军队是与玩家相关的军队，根据该军队获取战斗结果，返回4种情况值。
    /// 遍历攻击方和防守方的Adder，若有一支Adder，则跳出循环，即该军队与玩家相关。
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    void GetRes(bool a,bool b)
    {
        WarEventBackPro p = Attack;
        WarEventBackPro c = Defense;
        for (int i = 0; i < Defense.armyReal.Count; i++)
        {
            UnitPool.Ins.Push(Defense.armyReal[i]);
        }
        Defense.armyReal.Clear();
        UnitPool.Ins.Push(Defense.leadReal);
        for (int i = 0; i < Attack.armyReal.Count; i++)
        {
            UnitPool.Ins.Push(Attack.armyReal[i]);
        }
        Attack.armyReal.Clear();
        UnitPool.Ins.Push(Attack.leadReal);
        int Ares = 0;
        int Bres = 0;
        while (p.Adder != null)
        {
            Ares += p.Res;
            p = p.Adder;
        }
        while (c.Adder != null)
        {
            Bres += p.Res;
            c = c.Adder;
        }
        if(Ares>Bres)
        {
            Attack.IsRes = true;
            if (a && !b)
            {
                if (AisL)
                    Res = 1;
                else
                    Res = 2;
            }
            else
            {
                if (AisL)
                    Res = 3;
                else
                    Res = 4;
            }
        }
        else 
        {
            Defense.IsRes = true;
            if (b && !a)
            {
                if (BisL)
                    Res = 1;
                else
                    Res = 2;
            }
            else
            {
                if (BisL)
                    Res = 3;
                else
                    Res = 4;
            }
        }
    }
    public override void End()
    {
        base.End();
        IsEnd = true;
        TrigArea.IsBattleArea = false;
    }
}
