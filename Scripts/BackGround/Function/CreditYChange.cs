﻿using UnityEngine;
using System.Collections;
using System;

public class CreditYChange : IBackGround
{
    private Family Fam;
    private int ChangeValue;
    public override bool Response()
    {
        if (RegInitTime + CostTime < TimeDate.TimeCount)
        {
            Fam.FAsset.CreditY += ChangeValue;
            BoardcastDate.Ins.Rec("公众事件", TimeDate.TimeCount, "公众对于" + Fam.FamilyName + "家族的看法或多或少产生了变化！", BoardMessage.MessageKind.None);
            return true;
        }
        return false;
    }
    public CreditYChange(int cost,int value,Family fam)
    {
        CostTime = cost;
        ChangeValue = value;
        Fam = fam;
    }
}
