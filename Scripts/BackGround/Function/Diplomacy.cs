﻿using UnityEngine;
using System.Collections;
using System;

public class Diplomacy : IBackGround
{
    private int ChangeValue;
    private Family ffam, sfam;
    public override bool Response()
    {
        if (ffam == sfam)
        {
            return true;
        }
        if (RegInitTime + CostTime < TimeDate.TimeCount)
        {
            var s = ffam.FDiplomacy.GetRelation(sfam);
            var l = RelationPool.Ins.ReadByFirstID(sfam.FamilyID);
            System.Text.StringBuilder str = new System.Text.StringBuilder("和家族：" + sfam.FamilyName + " 关系增益了：" + ChangeValue);
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].Coefficient != 0)
                {
                    if (l[i].FirstFamilyId == sfam.FamilyID)
                    {
                        var otherfamilyrela = ffam.FDiplomacy.GetRelation(FamilyPool.Ins.ReadPool(l[i].SecondFamilyId) as Family);
                        otherfamilyrela.ChageValue((float)(otherfamilyrela.Value + ChangeValue * l[i].Coefficient));
                        str.AppendLine("连锁：" + (FamilyPool.Ins.ReadPool(l[i].SecondFamilyId) as Family).FamilyName + "关系增益了：" + ChangeValue * l[i].Coefficient);
                    }
                    else
                    {
                        var otherfamilyrela = ffam.FDiplomacy.GetRelation(FamilyPool.Ins.ReadPool(l[i].FirstFamilyId) as Family);
                        otherfamilyrela.ChageValue((float)(otherfamilyrela.Value + ChangeValue * l[i].Coefficient));
                        str.AppendLine("连锁：" + (FamilyPool.Ins.ReadPool(l[i].FirstFamilyId) as Family).FamilyName + "关系增益了：" + ChangeValue * l[i].Coefficient);
                    }
                }
            }
            s.ChageValue(s.Value + ChangeValue);
            BoardcastDate.Ins.Rec("交际效应", TimeDate.TimeCount, str.ToString(), BoardMessage.MessageKind.FamRelation);
            return true;
        }
        return false;
    }
    public Diplomacy(int cvalue,Family fam1,Family fam2)
    {
        ChangeValue = cvalue;
        ffam = fam1;
        sfam = fam2;
        CostTime = 0;
    }
}
