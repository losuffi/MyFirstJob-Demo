﻿using UnityEngine;
using System.Collections;
using System;

public class UmpiresWork : IBackGround
{
    Person person;
    public override bool Response()
    {
        var T = TimeDate.TimeCount * TimeControl.Ins.TimeSpeed;
        if ((1 + (T % 360) / 30) == SingleValuePool.Ins["ElectionEndMonth"].IntValue)
        {
            return true;
        }
        person.staff.fam.FAsset.Cash += (int)(person.StayArea.Tax*person.StayArea.AreaTaxPercent);
        return false;
    }
    public UmpiresWork(Person p)
    {
        person = p;
        CostTime = 12;
    }
}
