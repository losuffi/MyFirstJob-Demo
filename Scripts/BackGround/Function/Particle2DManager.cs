﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 留有结算接口 pro  GameEventBackPro 型。 可定义该类型的注册方式。
/// </summary>
public class Particle2DManager : MonoBehaviour {
    public GameObject ModelPrefab;
    public static Particle2DManager ins;
    public GameEventBackPro pro,playerpro;    //辩论胜利的 提案   玩家的提案。
    private void Awake()
    {
        ins = this;
    }
    public float EndRadius,PRadius;
    public int AngleCount;
    public float Speed;
    public Transform endposA, endposB;
    private List<Person> mapA=new List<Person>();
    private List<Person> mapB = new List<Person>();
    private List<GameObject> childs = new List<GameObject>();
    private Stack<GameObject> pool = new Stack<GameObject>();
    private bool IsCompelete;
    public int count { get; set; }
    public Vector3 GetEndPos(bool isA,Person a)
    {
        Vector3 end;
        if (isA)
        {
            var t = (((mapA.Count % AngleCount) * Mathf.PI * 2) / AngleCount);
            end = endposA.localPosition + (100)*((int)(mapA.Count / AngleCount) + 1) * PRadius * new Vector3(Mathf.Cos(t), Mathf.Sin(t));
            mapA.Add(a);
        }
        else
        {
            var t = (((mapB.Count % AngleCount) * Mathf.PI * 2) / AngleCount);
            end = endposB.localPosition + (100) * ((int)(mapB.Count / AngleCount) + 1) * PRadius * new Vector3(Mathf.Cos(t), Mathf.Sin(t));
            mapB.Add(a);
        }
        return end;
    }
    /// <summary>
    /// 结算公式 ：
    /// 变量1：人物倾向—— 保守、中立、激进：-2、1、1、2    A
    /// 变量2：提案认同度——由玩家出牌控制    B
    /// 变量3：人物与提案人物的好感度 0-100    C
    /// 变量4：提案的倾向性  -1、1    D
    /// 结果： B >= (60 - (A * D + A * D > 0 ? 0 : 1) * 20) - (C / 25) * (2 - (A * D - A * D > 0 ? 1 : 0) * 5)
    /// 常量：90（60+30）、25、5
    /// 60：初始情况下，当提案与本人倾向不同时的认同限度
    /// 25：好感变化对认同影响的单位步长。
    /// 5：单位影响值。微分  一致时，步长为1个5、中立时 都为2个5、不一致时为3个5.
    /// </summary>
    /// <param name="a"></param>
    /// <returns></returns>
    public bool GetRes(Person a)
    {
        //TODO:计算函数，若支持玩家的提案 返回1
        int A = a.Nature;
        int B = pro.npo;
        if (pro.person.staff.fam == a.staff.fam)
            return true;
        float  C = pro.person.staff.fam.FDiplomacy.GetRelation(a.staff.fam).Value;
        int D = pro.MotiNature;
        if (B >= (60 - (A * D + (A * D > 0 ? 0 : 1)) * 20) - (C / 25) * ((2 - (A * D - A * D > 0 ? 1 : 0)) * 5))          //修改，4个倾向。e
            return true;
        else
            return false;
    }
    public IEnumerator Work()
    {
        IsCompelete = false;
        GetComponent<UI_ShowText>().texts[0].text = "结算中...";
        count = 0;
        for(int i = 1; i < FamilyPool.Ins.count+1; i++)
        {
            var fam = FamilyPool.Ins.ReadPool(i) as Family;
            foreach (var cc in fam.FStaff.Seniors)
            {
                GameObject temp = Pop();
                temp.SetActive(true);
                temp.transform.position = transform.position;
                temp.transform.localScale = Vector3.one;
                temp.GetComponent<Particle2D>().person = cc;
                temp.GetComponent<Particle2D>().Work();
                childs.Add(temp);
                yield return 0;
            }
        }
        while (count < childs.Count)
            yield return 0;
        for (int i = 1; i < FamilyPool.Ins.count + 1; i++)
        {
            var fam = FamilyPool.Ins.ReadPool(i) as Family;
            int temp = 0;
            foreach (var cc in fam.FStaff.Seniors)
            {
                if (mapA.Contains(cc))
                {
                    temp++;
                }
                else
                {
                    temp--;
                }
            }
            if (fam == pro.person.staff.fam)
                continue;
            fam.FDiplomacy.GetRelation(pro.person.staff.fam).ChageValue(fam.FDiplomacy.GetRelation(pro.person.staff.fam).Value + (temp > 0 ? pro.moti.RelaADD : pro.moti.RelaSub));
            yield return 0;
        }
        pro.person.staff.fam.FAsset.CreditX += pro.moti.CreditX;
        pro.person.staff.fam.FAsset.CreditY += pro.moti.CreditY;
        pro.person.staff.fam.FamilyScore += pro.moti.Score;
        UITopMessage.ins.UpdateMessage();
        GetComponent<UI_ShowText>().texts[0].text = "结算结束：法案";
        if (mapA.Count > mapB.Count)
        {
            GetComponent<UI_ShowText>().texts[0].text += "通过！";
            pro.isValid = true;
        }
        else
        {
            pro.isValid = false;
            GetComponent<UI_ShowText>().texts[0].text += "不通过！";
        }
        if (AreaPool.Ins.Capital().Umpire.staff.fam.Equals(GameStart.Ins.playerfamily))
        {
            GetComponent<UI_ShowText>().child[0].SetActive(true);
            UIEventListener.Get(GetComponent<UI_ShowText>().child[1]).onClick = new UIEventListener.VoidDelegate(ar =>
              {
                  playerpro.isValid = true;
                  playerpro.person.staff.fam.FAsset.CreditX -= SingleValuePool.Ins["PassByConsulCreditXValue"].IntValue;
                  pro = playerpro;
                  UITopMessage.ins.UpdateMessage();
              });
            UIEventListener.Get(GetComponent<UI_ShowText>().child[2]).onClick = new UIEventListener.VoidDelegate(ar =>
            {
                GetComponent<UI_ShowText>().texts[1].text = "点击结束";
                IsCompelete = true;
            });
        }
        else
        {
            GetComponent<UI_ShowText>().texts[1].text = "点击结束";
            IsCompelete = true;
        }
        if (pro.isValid)
        {
            GameEventBox.Ins.PassGebp(pro);
        }
        else
        {
            GameEventBox.Ins.PassGebp(new GameEventBackPro(pro.situ));
        }
    }
    private void Update()
    {
        if (IsCompelete)
        {
            if (Input.GetMouseButtonDown(0))
            {
                for (int j = 0; j < childs.Count; j++)
                {
                    Push(childs[j]);
                }
                childs.Clear();
                mapA.Clear();
                mapB.Clear();
                UIManager.Ins.StateIsLock = false;
                GetComponent<UI_ShowText>().texts[1].text = " ";
                UIManager.Ins.SetState("UIS_Empty");
            }
        }
    }
    GameObject Pop()
    {
        if (pool.Count == 0)
        {
            GameObject temp = Instantiate<GameObject>(ModelPrefab);
            temp.transform.SetParent(transform);
            temp.transform.localPosition = Vector3.zero;
            return temp;
        }
        else
        {
            return pool.Pop();
        }
    }
    public void Push(GameObject t)
    {
        var temp = t.GetComponent<Particle2D>();
        temp.fflag = false;
        temp.flag = false;
        temp.sflag = false;
        t.SetActive(false);
        t.GetComponent<UIButton>().Ohe = null;
        t.GetComponent<UIButton>().Nhe = null;
        t.GetComponent<UIButton>().IsChangeColor = true;
        pool.Push(t);
    }
    public void StartWork()
    {
        GetComponent<UI_ShowText>().child[0].SetActive(false);
        StartCoroutine(Work());
    }
    public void ChangeColor(FamilyStaff s,bool state)
    {
        for(int i = 0; i < childs.Count; i++)
        {
            if (childs[i].GetComponent<Particle2D>().person.staff != s)
            {
                if (state)
                    childs[i].GetComponent<UISprite>().color = Color.black;
                else
                    childs[i].GetComponent<Particle2D>().ChangeColor();
            }
        }
        if (state)
            GetComponent<UI_ShowText>().texts[1].text = s.fam.FamilyName;
        else {
            GetComponent<UI_ShowText>().texts[1].text = IsCompelete ? "点击结束" : "";
        }
    }
}
