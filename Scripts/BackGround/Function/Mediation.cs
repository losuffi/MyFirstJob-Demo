﻿using UnityEngine;
using System.Collections;
using System;

public class Mediation : IBackGround
{
    GameObject m;
    Area start,Target;
    int Weight;
    Person p;
    bool flag;
    public override bool Response()
    {
        if (flag)
        {
            if ((p.Brain + SingleValuePool.Ins["MediationCharmValue"].IntValue * p.Charm)*3 > Target.country.CountryValue + Weight * SingleValuePool.Ins["MediationFullSuccessedValue"].IntValue)
            {
                //Debug.Log((p.Brain + SingleValuePool.Ins["MediationCharmValue"].IntValue * p.Charm) * 3 + "+" + Target.country.CountryValue + Weight * SingleValuePool.Ins["MediationFullSuccessedValue"].IntValue);
                //Debug.Log(Target.country.CountryValue + "+" + Weight + "+" + SingleValuePool.Ins["MediationFullSuccessedValue"].IntValue);
                Res = 1;
                BoardcastDate.Ins.Rec(SingleValuePool.Ins["SystemNoteMessageTitle"].StringValue, TimeDate.TimeCount, Target.AreaName +p.Name+ SingleValuePool.Ins["StrMessageBoxMediationSuccessed"].StringValue, BoardMessage.MessageKind.None);
            }
            else
            {
                //Debug.Log((p.Brain + SingleValuePool.Ins["MediationCharmValue"].IntValue * p.Charm) * 3 + "+" + Target.country.CountryValue + Weight * SingleValuePool.Ins["MediationFullSuccessedValue"].IntValue);
                //Debug.Log(Target.country.CountryValue+"+"+ Weight + "+" + SingleValuePool.Ins["MediationFullSuccessedValue"].IntValue);
                if (UnityEngine.Random.Range(0, 100) < Weight * 2)
                {
                    Res = 3;
                    m.GetComponent<ManUnit>().Stop();
                    UnitPool.Ins.Push(m);
                    BoardcastDate.Ins.Rec(SingleValuePool.Ins["SystemNoteMessageTitle"].StringValue, TimeDate.TimeCount, Target.AreaName+p.Name + SingleValuePool.Ins["StrMessageBoxMediationFailDead"].StringValue, BoardMessage.MessageKind.None);
                }
                else
                {
                    BoardcastDate.Ins.Rec(SingleValuePool.Ins["SystemNoteMessageTitle"].StringValue, TimeDate.TimeCount, Target.AreaName + p.Name + SingleValuePool.Ins["StrMessageBoxMediationFail"].StringValue, BoardMessage.MessageKind.None);
                    Res = 2;
                }

            }
            return true;
        }
        return false;

    }
    public Mediation(Person t,Area target,int weight)
    {
        var man = UnitPool.Ins.Pop("Man");
        start = t.StayArea;
        p = t;
        man.GetComponent<ManUnit>().p = p;
        man.GetComponent<ManUnit>().MoveOn(t.StayArea, target, 2);
        man.GetComponent<ManUnit>().cp += delegate
        {
            UnitPool.Ins.Push(m);
        };
        man.GetComponent<ManUnit>().sp += delegate { flag = true;
            man.GetComponent<ManUnit>().Stop(1);
        };
        Target = target;
        m = man;
        Weight = weight;
        Res = 0;
        flag = false;
        BackGroundManager.Ins.RegBGEvent(this);
    }
}
