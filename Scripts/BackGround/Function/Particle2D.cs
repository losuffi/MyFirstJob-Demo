﻿using UnityEngine;
using System.Collections;

public class Particle2D : MonoBehaviour {
    public Person person { get; set; }
    float Speed;  //位移速度
    public bool flag, sflag,fflag;//开关
    private void Awake()
    {
        flag = false;
        sflag = false;
        fflag = false;
    }
    public void Work()
    {
        ChangeColor();
        flag = true;
        r = Particle2DManager.ins.EndRadius;
        rr = Particle2DManager.ins.PRadius;
        Speed = Particle2DManager.ins.Speed;
        if (Particle2DManager.ins.GetRes(person))
            EndArea = Particle2DManager.ins.endposA.localPosition;
        else
            EndArea = Particle2DManager.ins.endposB.localPosition;
        float t = Random.Range(0, Mathf.PI * 2);
        starpos = transform.localPosition + Random.Range(10f, 20f) * r * new Vector3(Mathf.Cos(t), Mathf.Sin(t), 0);
        GetComponent<UIButton>().Ohe = ShowMsg;
        GetComponent<UIButton>().Nhe = HiddenMsg;
        GetComponent<UIButton>().IsChangeColor = false;
    }
    float r,rr;
    Vector3 end,EndArea,starpos;
    private void Update()
    {
        if (!flag)
            return;
        if (!fflag)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, starpos, 0.5f * Speed * Time.deltaTime);
            if (Vector3.Distance(starpos, transform.localPosition) <= 20 * rr)
            {
                fflag = true;
            }
            return;
        }
        if (!sflag)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, EndArea, Speed * Time.deltaTime);
            if (Vector3.Distance(EndArea, transform.localPosition) <= 50f*r)
            {
                sflag = true;
                Particle2DManager.ins.count++;
                end = Particle2DManager.ins.GetEndPos(Particle2DManager.ins.GetRes(person),person);
            }
        }
        else
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, end, Speed * Time.deltaTime);
            if (Vector3.Distance(end, transform.localPosition) <= 1f * rr)
            {
                flag = false;
            }
        }
    }
    void ShowMsg()
    {
        Particle2DManager.ins.ChangeColor(person.staff,true);
    }
    void HiddenMsg()
    {
        Particle2DManager.ins.ChangeColor(person.staff,false);
    }
    public void ChangeColor()
    {
        switch (person.Nature)
        {
            case 1:
                GetComponent<UISprite>().color = Color.yellow;
                break;
            case -1:
                GetComponent<UISprite>().color = Color.green;
                break;
            case -2:
                GetComponent<UISprite>().color = Color.blue;
                break;
            default:
                GetComponent<UISprite>().color = Color.red;
                break;
        }
    }
}
