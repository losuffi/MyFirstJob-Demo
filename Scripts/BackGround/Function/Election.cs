﻿using UnityEngine;
using System.Collections;
using System;

public class Election : IBackGround
{
    public class JobPer
    {
        public ElectionKind t;
        public Person per;
        public int CostValue;
    }
    public JobPer senior, umpire, consual;
    public enum ElectionKind
    {
        Senior,
        Umpire,
        Consual
    }
    private ElectionKind kind;
    public override bool Response()
    {
        if (RegInitTime + CostTime < TimeDate.TimeCount)
        {
            if (umpire.per != null)
            {
                if (umpire.per.staff.fam.FAsset.CreditY >= umpire.CostValue)
                {
                    umpire.per.staff.fam.FAsset.CreditY -= umpire.CostValue;
                    BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxElecRes"].StringValue, TimeDate.TimeCount, umpire.per.Name + "裁判官" + SingleValuePool.Ins["StrMessageBoxElecResSuccess"].StringValue, BoardMessage.MessageKind.Job);
                    BackGroundJobs.Ins.Umpire(umpire.per);
                }
                else
                {
                    BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxElecRes"].StringValue, TimeDate.TimeCount, umpire.per.Name + "裁判官" + SingleValuePool.Ins["StrMessageBoxElecResFailed"].StringValue, BoardMessage.MessageKind.Job);
                }
            }
            if (senior.per != null)
            {
                if (senior.per.staff.fam.FAsset.CreditY >= senior.CostValue)
                {
                    senior.per.Job = Person.JobKind.Senior;
                    senior.per.staff.fam.FAsset.CreditY -= senior.CostValue;
                    BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxElecRes"].StringValue, TimeDate.TimeCount, senior.per.Name + "元老" + SingleValuePool.Ins["StrMessageBoxElecResSuccess"].StringValue, BoardMessage.MessageKind.Job);
                }
                else
                {
                    BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxElecRes"].StringValue, TimeDate.TimeCount, senior.per.Name + "元老" + SingleValuePool.Ins["StrMessageBoxElecResFailed"].StringValue, BoardMessage.MessageKind.Job);
                }
            }
            if (consual.per != null)
            {
                if (consual.per.staff.fam.FAsset.CreditY >= consual.CostValue)
                {
                    consual.per.staff.fam.FAsset.CreditY -= consual.CostValue;
                    BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxElecRes"].StringValue, TimeDate.TimeCount, consual.per.Name + "执政官" + SingleValuePool.Ins["StrMessageBoxElecResSuccess"].StringValue, BoardMessage.MessageKind.Job);
                    BackGroundJobs.Ins.BeConsul(consual.per);
                }
                else
                {
                    BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxElecRes"].StringValue, TimeDate.TimeCount, consual.per.Name + "执政官" + SingleValuePool.Ins["StrMessageBoxElecResFailed"].StringValue, BoardMessage.MessageKind.Job);
                }
            }
            return true;
        }
        return false;
    }
    public Election(int costtime)
    {
        CostTime = costtime;
        umpire = new JobPer();
        senior = new JobPer();
        consual = new JobPer();
    }
}
