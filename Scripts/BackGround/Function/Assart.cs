﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 需要重构
/// </summary>
public class Assart : IBackGround
{
    Estate estate;
    public override bool Response()
    {
        if (estate.CostTime <= 0)
        {
            BoardcastDate.Ins.Rec(
            SingleValuePool.Ins["StrMessageBoxAssartUpdateTitle"].StringValue,
            TimeDate.TimeCount,
             estate.area.AreaName + estate.cro.CropName + SingleValuePool.Ins["StrMessageBoxAssartUpdate"].StringValue,
             BoardMessage.MessageKind.Estate);
            estate.IsWork = true;
            estate.StartTIme = TimeDate.TimeCount;
            return true;
        }
        if (estate.area.SuitableId != -1) //-1 代表地产工作暂停，临时标志
        {
            estate.CostTime--;
        }
        return false;
    }
    public Assart(Estate tem)
    {
        estate = tem;
    }
}
