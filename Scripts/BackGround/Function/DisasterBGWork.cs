﻿using UnityEngine;
using System.Collections;

public class DisasterBGWork : IBackGround {

    public int IBG_TimeOfDuration;
    public Area TrigArea;
    int AreaSuitableCropID;
    public override bool Response()
    {
        if (IBG_TimeOfDuration <= 0)
        {
            TrigArea.SuitableId = AreaSuitableCropID;
            return true;
        }
        TrigArea.SuitableId = -1;   //记录地区的优势地产物，做临时标志，置-1 意为停止建设。
        return false;
    }
    public DisasterBGWork(Area trigArea)
    {
        TrigArea = trigArea;
        AreaSuitableCropID = TrigArea.SuitableId;
        IBG_TimeOfDuration = 1;
        BackGroundManager.Ins.RegBGEvent(this);
    }
}
