﻿using UnityEngine;
using System.Collections;
using System;

public class AddWar : IBackGround
{
    WarEventBackPro target, Adder;
    Person Leader;
    bool IsStart;
    public override bool Response()
    {
        if(!IsStart)
            return false;
        else
        {
            return true;
        }
    }
    public AddWar(WarEventBackPro e, WarEventBackPro t, Person L,Action Ended=null)
    {
        target = t;
        Adder = e;
        Leader = L;
        IsStart = false;
        Adder.leadReal.GetComponent<ManUnit>().MoveOn(e.Leader.StayArea, t.Leader.StayArea, 1);
        Adder.leadReal.GetComponent<ManUnit>().mp += delegate { Adder.MoveArmy();
            if (target.waring.IsEnd)
            {
                Adder.leadReal.GetComponent<ManUnit>().Stop();
                if (Ended != null)
                {
                    Ended();
                }
                //返回出发地
            }
        };
        Adder.leadReal.GetComponent<ManUnit>().cp += delegate
        {
            if (target.waring.IsEnd)
            {
                Adder.leadReal.GetComponent<ManUnit>().Stop();
                if (Ended != null)
                {
                    Ended();
                }
                return;
                //返回出发地
            }
            IsStart = true;
            target.armys.AddRange(Adder.armys);
            //Adder.leadReal.transform.position = target.leadReal.transform.position;
            //Adder.leadReal.transform.rotation = target.leadReal.transform.rotation;
            UnitPool.Ins.Push(Adder.leadReal);
            target.Leader = Leader;
            Adder.leadReal = target.leadReal;
            target.leadReal.GetComponent<ManUnit>().p = L;
            target.MaxCount += Adder.MaxCount;
            target.MaxArmyCount += Adder.MaxArmyCount;
            target.WarEventBackProWeight += Adder.WarEventBackProWeight;
            target.armyReal.AddRange(Adder.armyReal);
            WarEventBackPro p = target;
            Adder.ChangeStatus(Army.states.Battle);
            while (p.Adder != null)
            {
                p = p.Adder;
            }
            p.Adder = Adder;
            if (target.Root != null)
                Adder.Root = target.Root;
            else
                Adder.Root = target;
            Adder.waring = target.waring;
            target.waring.ZUck();
        };
        BackGroundManager.Ins.RegBGEvent(this);
    }
}
