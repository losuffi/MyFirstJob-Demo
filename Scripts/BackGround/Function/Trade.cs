﻿# define NewVerSionTrade
using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 需要改变表现形式，目前阶段，一次贸易的路程表现上不可变。
/// </summary>
public class Trade : IBackGround
{
#if OldVerSionTrade
    public string tt;
    private Area TargetArea;
    private Person per;
    private int Rnd, Earnings;
    ManUnit perReal;
    GameObject man;
    public Trade(Specialty spe,Person p,Area a)
    {
        TargetArea = a;
        per = p;
        CostTime = a.Distance * 2;
        Rnd = spe.SpecialtyRnd;
        Earnings = spe.SpecialtyValueReal;
        RndMethod();
        DashPerson();
        man = UnitPool.Ins.Pop("Man");
        man.SetActive(true);
        perReal = man.GetComponent<ManUnit>();
        perReal.MoveOn(AreaPool.Ins.ReadPool(26) as Area, TargetArea, 2);
        perReal.p = per;
    }
    public override bool Response()
    {
        if(RegInitTime+CostTime< TimeDate.TimeCount)
        {
            per.staff.fam.FAsset.Cash += (int)(per.Force)*Earnings;
            BoardcastDate.Ins.Rec("贸易结束", TimeDate.TimeCount,
                "家族成员的贸易结果已经传回", BoardMessage.MessageKind.Trade);
            per.StayArea = AreaPool.Ins.ReadPool(1) as Area;
            UnitPool.Ins.Push(man);
            return true;
        }
        else
        {
            per.StayArea = TargetArea; //得改，需计算正在的位置，同步过去。
            return false;
        }
    }
    void RndMethod()
    {
        for(int i = 0; i < Rnd; i++)
        {
            int b = Random.Range(1, 5);
            switch (b)
            {
                case 1:
                    RndMoneyValue();
                    break;
                case 3:
                    Dead();
                    break;
                case 4:
                    RndTimeCost();
                    break;
                default:
                    break;
            }
            
        }
    }
    void RndMoneyValue()
    {
        int b = Random.Range(1, 11);
        switch (b)
        {
            case 1:
                Earnings = (int)(Earnings * 1.05);
                break;
            case 2:
                Earnings = (int)(Earnings * 1.1);
                break;
            case 3:
                Earnings = (int)(Earnings * 1.15);
                break;
            case 4:
                Earnings = (int)(Earnings * 1.2);
                break;
            case 5:
                Earnings = (int)(Earnings * 1.5);
                break;
            case 6:
                Earnings = (int)(Earnings * 0.95);
                break;
            case 7:
                Earnings = (int)(Earnings * 0.9);
                break;
            case 8:
                Earnings = (int)(Earnings * 0.85);
                break;
            case 9:
                Earnings = (int)(Earnings * 0.8);
                break;
            case 10:
                Earnings = (int)(Earnings * 0.5);
                break;

        }
    }
    void EndTrade()
    {

    }
    void Dead()
    {

    }
    void RndTimeCost()
    {
        int b = Random.Range(1, 5);
        switch (b)
        {
            case 1:
                CostTime -= 1;
                break;
            case 2:
                CostTime -= 2;
                break;
            case 3:
                CostTime += 1;
                break;
            case 4:
                CostTime += 2;
                break;
        }
        CostTime = CostTime < 0 ? 0 : CostTime;
    }
    void DashPerson()
    {
        per.TaskGO(CostTime, Person.TaskKind.Trade,TargetArea);
    }
    public override void End()
    {
        base.End();
        per.TaskGO(0, Person.TaskKind.None, TargetArea);
    }
#endif
#if NewVerSionTrade
    Specialty spe;
    Person per;
    Area target, start;
    double EarningPercent;
    int Dis, Count; //Dis为完成工作剩余的步数 起始为：罗马——Target+Target——罗马  Count为随机次数。
    GameObject Man;
    public override bool Response()
    {
        if (target.Specialty == 0)
        {
            Res = 2;
            return true;
        }
        if (flag)
        {
            Res = 1;
            return true;
        }
        return false;
    }
    public Trade(Specialty s,Person p,Area a)
    {
        Res = 0;
        spe = s;
        per = p;
        target = a;
        EarningPercent = 1;
        Count = spe.SpecialtyRnd;
        start = p.StayArea;
        Dis = AreaRoad.Ins.GetRoadDistance(start.AreaID, a.AreaID) * 2;
        Man = UnitPool.Ins.Pop("Man");
        Man.SetActive(true);
        per.TaskGO(Person.TaskKind.Trade, target);
        Man.GetComponent<ManUnit>().p = per;
        Man.GetComponent<ManUnit>().MoveOn(start, target, 1);
        Man.GetComponent<ManUnit>().mp +=delegate{
            if (Count <= 0)
                return;
            Dis = AreaRoad.Ins.GetRoadDistance(per.StayArea.AreaID, target.AreaID) + AreaRoad.Ins.GetRoadDistance(target.AreaID, start.AreaID);
            SeedRnd();
        };
        Man.GetComponent<ManUnit>().cp +=delegate{
            flag = true; };
        flag = false;
    }
    bool flag; //用于停一回合，end绑定方法，会在当前回合 直接跑一遍新绑定的方法。
    public override void End()
    {
        base.End();
        if (Res == 1)
        {
            per.TaskGO(Person.TaskKind.Trade, start);
            Man.GetComponent<ManUnit>().MoveOn(target, start, 1);
            Man.GetComponent<ManUnit>().SpeedUp(1);
            Man.GetComponent<ManUnit>().mp += delegate
            {
                if (Count <= 0)
                    return;
                Dis = AreaRoad.Ins.GetRoadDistance(per.StayArea.AreaID, start.AreaID);
                SeedRnd();
            };
            Man.GetComponent<ManUnit>().cp += delegate {
                per.Relax();
                Man.GetComponent<ManUnit>().Stop();
                UnitPool.Ins.Push(Man);
                int Earning = (int)(spe.SpecialtyValueReal * EarningPercent) * per.Force;
                BoardcastDate.Ins.Rec(SingleValuePool.Ins["SystemNoteMessageTitle"].StringValue, TimeDate.TimeCount, per.Name + "-" + SingleValuePool.Ins["StrMessageBoxTradeComplete"].StringValue+"-"+Earning, BoardMessage.MessageKind.Trade);
                per.staff.fam.FAsset.Cash += Earning; };
        }
        else if(Res==2)
        {
            per.TaskGO(Person.TaskKind.Trade, start);
            Man.GetComponent<ManUnit>().MoveOn(target, start, 1);
            Man.GetComponent<ManUnit>().cp += delegate {
                per.Relax();
                Man.GetComponent<ManUnit>().Stop();
                UnitPool.Ins.Push(Man);
            };
        }
    }
    void SeedRnd()
    {
        int seed = UnityEngine.Random.Range(0, Dis + 1);
        if (Count >= seed)
        {
            Count--;
            Rnd();
        }
    }
    void Rnd()
    {
        int b = UnityEngine.Random.Range(1, 5);
        switch (b)
        {
            case 1:
                RndMoneyValue();
                break;
            case 3:
                Dead();
                break;
            case 2:
                RndDis();
                break;
            case 4:
                RndCount();
                break;
            default:
                break;
        }
    }
    //--------具体随机 方法------//
    void RndMoneyValue()
    {
        int b = UnityEngine.Random.Range(1, 11);
        switch (b)
        {
            case 1:
                EarningPercent = (EarningPercent * 1.05);
                break;
            case 2:
                EarningPercent = (EarningPercent * 1.1);
                break;
            case 3:
                EarningPercent = (EarningPercent * 1.15);
                break;
            case 4:
                EarningPercent = (EarningPercent * 1.2);
                break;
            case 5:
                EarningPercent = (EarningPercent * 1.5);
                break;
            case 6:
                EarningPercent = (EarningPercent * 0.95);
                break;
            case 7:
                EarningPercent = (EarningPercent * 0.9);
                break;
            case 8:
                EarningPercent = (EarningPercent * 0.85);
                break;
            case 9:
                EarningPercent = (EarningPercent * 0.8);
                break;
            case 10:
                EarningPercent = (EarningPercent * 0.5);
                break;

        }
    }
    void Dead()
    {

    }
    void RndCount()
    {
        int b = UnityEngine.Random.Range(1, 5);
        switch (b)
        {
            case 1:
                Count -= 1;
                break;
            case 2:
                Count -= 2;
                break;
            case 3:
                Count += 1;
                break;
            case 4:
                Count += 2;
                break;
        }
        Count = Count < 0 ? 0 : Count;
    }
    void RndDis()
    {
        int b = UnityEngine.Random.Range(1, 5);
        int wait = 0;
        int Step = 0;
        switch (b)
        {
            case 1:
                wait += 1;
                break;
            case 2:
                wait += 2;
                break;
            case 3:
                Step += 1;
                break;
            case 4:
                Step += 2;
                break;
        }
        Man.GetComponent<ManUnit>().Stop(wait);
        Man.GetComponent<ManUnit>().SpeedUp(Step);
    }
#endif
}
