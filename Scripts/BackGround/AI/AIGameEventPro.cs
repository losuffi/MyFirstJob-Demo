﻿#define PlayerMotionByAI
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class AIGameEventPro{
#if AIMotionByPlayer
    public static GameEventBackPro CreateBackPro(Family a,Situation s,Motion m)
    {
        var rnd = (int)Random.Range(1, FamilyPool.Ins.count);
        if (rnd == a.FamilyID)
            return CreateBackPro(a, s, m);
        var p = FamilyPool.Ins.ReadPool(rnd) as Family;
        Person per = null;
        if (p.FStaff.SeniorCount < 3)
        {
            int c = Random.Range(3, p.FStaff.Count);
            for (int i = 0; i < c; i++)
            {
                p.FStaff.GetPerson(Random.Range(0, p.FStaff.Count)).Job=Person.JobKind.Senior;

            }
        }
        for (int i = 0; i < p.FStaff.Count; i++)
        {
            var pe = p.FStaff.GetPerson(i);
            if (!(pe.Job==Person.JobKind.Senior) || !pe.Idel || !pe.IsLiving)
                continue;
            if (per == null)
            {
                per = pe;
                continue;
            }
            else if ((pe.Charm + pe.Brain) - (per.Brain + per.Charm) > 0)
            {
                per = pe;
            }
        }
        Motion Mo = null;
        for (int i = 0; i < s.motions.Count; i++)
        {
            if (s.motions[i] == m) 
            {
                Mo = s.motions[Rndmotion(i, s.motions.Count)];
                break;
            }
        }
        var temp= new GameEventBackPro(s, per, Mo);
        RndExct(Mo, temp, s);
        if (temp.excts.Count == 0)
        {
            temp.MotiWeight = temp.moti.Weight;
        }
        return temp;
    }
#endif
#if PlayerMotionByAI
    public static GameEventBackPro CreateBackPro(Situation s)
    {
        Person p = null;
        if (GameStart.Ins.playerfamily != AreaPool.Ins.Capital().Umpire.staff.fam)
        {
            p = AreaPool.Ins.Capital().Umpire;
        }
        else
        {
            var rnd = (int)Random.Range(1, FamilyPool.Ins.count);
            if (rnd == GameStart.Ins.playerfamily.FamilyID)
                return CreateBackPro(s);
            var fam = FamilyPool.Ins.ReadPool(rnd) as Family;
            for (int i = 0; i < fam.FStaff.Count; i++)
            {
                var pe = fam.FStaff.GetPerson(i);
                if (!(pe.Job == Person.JobKind.Senior) || !pe.Idel || !pe.IsLiving)
                    continue;
                if (p == null)
                {
                    p = pe;
                    continue;
                }
                else if ((pe.Charm + pe.Brain) - (p.Brain + p.Charm) > 0)
                {
                    p = pe;
                }
            }
        }
        Motion m = null;
        m = s.motions[Random.Range(0, s.motions.Count)];
        var temp = new GameEventBackPro(s, p, m);
        RndExct(temp);
        if (temp.excts.Count == 0)
        {
            temp.MotiWeight = temp.moti.Weight;
        }
        return temp;
    }
#endif
    public static Argue CreateArgue(GameEventBackPro pro)
    {
        if (pro.cost < 2)
            return null;
        int i = Random.Range(1, ArguePool.Ins.Count + 1);
        Argue temp = ArguePool.Ins[i] as Argue;
        if (temp.Cost > pro.cost)
            return CreateArgue(pro);
        else
        {
            return temp;
        }
    }
    static List<Area> areas = new List<Area>();
    static void GetAlliedNationsArea()
    {
        for (int j = 0; j < AreaPool.Ins.Count; j++)
        {
            var a = AreaPool.Ins.ReadPool(j + 1) as Area;
            if (a.Subjection == 2)
            {
                if (a.Umpire != null)
                {
                    areas.Add(a);
                }
            }
        }
    }
    static void GetNeighboringArea(Area pos)
    {
        for (int j = 0; j < AreaPool.Ins.Count; j++)
        {
            var a = AreaPool.Ins.ReadPool(j + 1) as Area;
            if (Mathf.Abs(a.pos_X - pos.pos_X) + Mathf.Abs(a.pos_Y - pos.pos_Y) <= SingleValuePool.Ins["MaxNearDistance"].IntValue)
            {
                if (a.Subjection < 2)
                {
                    if (a.Umpire != null)
                    {
                        areas.Add(a);
                    }
                }

            }
        }
    }
    static void RndExct(GameEventBackPro ai)
    {
        ai.excts.Clear();
        switch (ai.moti.Nature)
        {
            case 1:
                var fam = (FamilyPool.Ins.ReadPool(Random.Range(1, FamilyPool.Ins.count)) as Family).FStaff;
                ai.excts.Add(1, fam.GetPerson(Random.Range(0, fam.Count)));
                ai.MotiWeight = ai.moti.Weight + ((ai.excts[1] as Person).Nature + 2) * 0.3f;
                break;
            case 3:
                int x = 500 * Random.Range(1, 4);
                ai.excts.Add(0, x);
                ai.MotiWeight = ai.moti.Weight + (((int)ai.excts[0]) / 500) * 0.3f;
                break;
            case 5:
                x = 500 * Random.Range(1, 4);
                ai.excts.Add(0, x);
                ai.MotiWeight = ai.moti.Weight + (((int)ai.excts[0]) / 500) * 0.3f;
                break;
            case 4:
                areas.Clear();
                GetNeighboringArea(ai.situ.TriggerArea);
                ai.excts.Add(1, areas[Random.Range(0, areas.Count)].Umpire);
                ai.MotiWeight = ai.moti.Weight + ((ai.excts[1] as Person).Nature + 2) * 0.3f;
                break;
            case 6:
                var c = Random.Range(0, 2);
                ai.excts.Add(1, c == 0 ? (WarEventBackPro.WorldWarEvent.Find(ar => ar.Leader == ai.situ.TriggerArea.MinisterA) != null ? ai.situ.TriggerArea.MinisterA : ai.situ.TriggerArea.Umpire) : ai.situ.TriggerArea.MinisterB);
                ai.MotiWeight = ai.moti.Weight + c * 0.3f;
                break;
            case 7:
                areas.Clear();
                GetAlliedNationsArea();
                ai.excts.Add(1, areas[Random.Range(0, areas.Count)].Umpire);
                ai.MotiWeight = ai.moti.Weight + ((ai.excts[1] as Person).Nature + 2) * 0.3f;
                break;
            case 8:
                areas.Clear();
                GetAlliedNationsArea();
                ai.excts.Add(2, areas[Random.Range(0, areas.Count)]);
                ai.MotiWeight = ai.moti.Weight + ((ai.excts[2] as Area).AreaID) * 0.01f;
                break;
            default:
                break;
        }
    }
#if AIMotionByPlayer
    static int Rndmotion(int L,int max)
    {
        int res = Random.Range(0, max);
        if (res == L && max > 1)
            return Random.Range(L, max);
        return res;
    }
    /// <summary>
    /// 随机挂额外变量。这一步 还需改进， AI的提案无法保证在提同一个加挂变量的提案时，保证变量与玩家的不一致。且Ai的提案仅会在提案唯一时，与玩家
    /// 提一样的加挂变量提案。
    /// </summary>
    /// <param name="t"></param>
    /// <param name="y"></param>
    static void RndExct(Motion t,GameEventBackPro y,Situation ss)
    {
        y.excts.Clear();
        if (t.Nature == 1)
        {
            var fam = (FamilyPool.Ins.ReadPool(Random.Range(1, FamilyPool.Ins.count)) as Family).FStaff;
            y.excts.Add(1, fam.GetPerson(Random.Range(0, fam.Count)));
            y.MotiWeight = y.moti.Weight + ((y.excts[1] as Person).Nature + 2) * 0.3f;
        }
        else if (t.Nature == 3 || t.Nature == 5)
        {
            int x = 500 * Random.Range(1, 4);
            y.excts.Add(0, x);
            y.MotiWeight = y.moti.Weight + (((int)y.excts[0]) / 500) * 0.3f;
        }
        else if (t.Nature == 4)
        {
            var  ares = new List<Area>();
            for (int j = 0; j < AreaPool.Ins.Count; j++)
            {
                var a = AreaPool.Ins.ReadPool(j + 1) as Area;
                if (Mathf.Abs(a.pos_X - ss.TriggerArea.pos_X) + Mathf.Abs(a.pos_Y - ss.TriggerArea.pos_Y) <= SingleValuePool.Ins["MaxNearDistance"].IntValue)
                {
                    if (a.Umpire != null)
                    {
                        ares.Add(a);
                    }

                }

            }
            y.excts.Add(1, ares[Random.Range(0,ares.Count)].Umpire);
            y.MotiWeight = y.moti.Weight + ((y.excts[1] as Person).Nature + 2) * 0.3f;
        }
    }
#endif
}
