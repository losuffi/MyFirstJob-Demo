﻿using UnityEngine;
using System.Collections;
using System;

public class EventEmitter : IBackGround
{
    bool F;
    public EventEmitter()
    {
        F = false;
    }
    public override bool Response()
    {
        GameEventBox.Ins.GetResSitu();
        if (UnityEngine.Random.Range(0, 100) > 1)
        {
            var t = RNDCreateSituation();
            if(t!=null)
                GameEventBox.Ins.AddSitus(t);
        }
        CreateLawSituation();
        return false;
    }
    Situation RNDCreateSituation()
    {
        if (F)
            return null;
        int sit = Initlization.Ins.StartSituID;
        var t = SituationPool.Ins[sit] as Situation;
        // int area = UnityEngine.Random.Range(0, t.areas.Count);
        int area = Initlization.Ins.StartAreaIndex;
        Debug.Log(t.Name+t.areas.Count);
        t.TriggerArea = t.areas[area];
        F = true;
        return t;
    } 
    void CreateLawSituation()
    {
        var topics = LawPool.Ins.Topics;
        for (int i = 0; i < topics.Count; i++)
        {
            if (GameEventBox.Ins.Situs.Find(ar => ar.IsLaw && ar.Sid == i) != null)
                continue;
            var situ = new Situation();
            situ.Sid = i;
            situ.Name = topics[i];
            situ.effects = new System.Collections.Generic.List<IEffect>();
            situ.motions = new System.Collections.Generic.List<Motion>();
            situ.Kind = -2;
            situ.IsLaw = true;
            CreateLawMotion(situ);
            //Debug.Log(situ.Name + "," + situ.motions[0].Name);
            GameEventBox.Ins.AddSitus(situ);
        }
        //TODO:创建法律情景。
    }
    void CreateLawMotion(Situation s)
    {
        var topic = s.Name;
        var laws = LawPool.Ins.GetLawsByTopic(topic);
        for (int i = 0; i < laws.Count; i++)
        {
            Motion new_Motion = new Motion();
            new_Motion.Name = laws[i].Name;
            new_Motion.Weight = 0;
            new_Motion.Value = 0;
            new_Motion.Score = 0;
            new_Motion.RelaSub = 0;
            new_Motion.RelaADD = 0;
            new_Motion.Nature = laws[i].Command;
            new_Motion.ID = laws[i].ID;
            s.motions.Add(new_Motion);
        }
        //TODO:创建法律提案（法案更改可选项）
    }
}
