﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class PoolGameObjX
{
    public string name;
    public GameObject obj;
}
public class UnitPool : MonoBehaviour {
    public static UnitPool Ins;
    public List<PoolGameObjX> models;
    private Dictionary<string, Stack<GameObject>> map = new Dictionary<string, Stack<GameObject>>();
    private void Awake()
    {
        Ins = this;
    }
    public GameObject Pop(string name)
    {
        GameObject temp = null;
        if (!map.ContainsKey(name))
        {
            map.Add(name, new Stack<GameObject>());
            temp = Instantiate(models.Find(ar => { if (ar.name == name) return true; else return false; }).obj);
            temp.GetComponent<IUnit>().Name = name;
            temp.name = temp.GetComponent<IUnit>().Name;
            temp.transform.SetParent(transform);
            return temp;
        }
        else
        {
            if (map[name].Count < 1)
            {
                temp = Instantiate(models.Find(ar => { if (ar.name == name) return true; else return false; }).obj);
                temp.GetComponent<IUnit>().Name = name;
                temp.name = temp.GetComponent<IUnit>().Name;
                temp.transform.SetParent(transform);
                return temp;
            }
        }
        temp = map[name].Pop();
        temp.SetActive(true);
        return temp;
    }

    public void Push(GameObject temp)
    {
        if (temp.GetComponent<IUnit>() == null)
        {
            Destroy(temp);
            return;
        }
        if (temp.activeInHierarchy)
        {
            temp.SetActive(false);
            map[temp.GetComponent<IUnit>().Name].Push(temp);
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            var rome = AreaPool.Ins.ReadPool(26) as Area;
            var target = AreaPool.Ins.ReadPool(5) as Area;
            new Mediation(new Person(true, null, rome), target, 5);
        }
    }
}
