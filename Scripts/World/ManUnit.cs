﻿using UnityEngine;
using System.Collections;
using System;

public class ManUnit : IUnit {
    public Person p { get; set; }

    protected override void ArriPoint(Area o)
    {
        p.StayArea = o;
    }
}
