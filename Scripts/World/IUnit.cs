﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public abstract class IUnit : MonoBehaviour {
    public bool IsSelected;
    public string Name { get; set; }
    protected List<Point> road;
    protected Transform one, two;
    protected int Index;
    public bool IsOn { get; set; }
    public Point TargetPoint, NextPoint;
    public Area mStart, End;
    public int count;
    public event Action cp, mp, sp;
    private void Awake()
    {
        IsOn = false;
        IsSelected = false;
    }
    public void MoveOn(Area start,Area target,int Count)
    {
        if (start.Equals(target))
        {
            if (cp != null)
                cp();
            return;
        }
        Index = 1;
        IsOn = true;
        road= AreaRoad.Ins.GetRoad(start.AreaID, target.AreaID);
        if (road == null)
        {
            if (cp != null)
                cp();
            return;
        }
        TargetPoint = new Point(start.AreaID, -1, 0);
        NextPoint = road.Find(ar => ar.Parent==TargetPoint.AreaID);
        one = GameObject.Find("Map/Plane").transform.GetChild(TargetPoint.AreaID - 1);
        transform.position = one.position + new Vector3(0, 1, 0);
        two = GameObject.Find("Map/Plane").transform.GetChild(NextPoint.AreaID - 1);
        transform.LookAt(two.position + new Vector3(0, 1, 0));
        TimeControl.Ins.ep += Move;
        mStart = start;
        End = target;
        count = Count;
    }
    /// <summary>
    /// 完全终结。
    /// </summary>
    public void Stop()
    {
        IsOn = false;
        TimeControl.Ins.ep -= Move;
        if (mp != null)
        {
            var cpevents = mp.GetInvocationList();
            for (int i = 0; i < cpevents.Length; i++)
            {
                typeof(IUnit).GetEvent("mp").RemoveEventHandler(this, cpevents[i]);
            }
        }
        if (cp != null)
        {
            var cpevents = cp.GetInvocationList();
            for (int i = 0; i < cpevents.Length; i++)
            {
                typeof(IUnit).GetEvent("cp").RemoveEventHandler(this, cpevents[i]);
            }
        }
    }
    int waitcount;
    /// <summary>
    /// 停止一定回合
    /// </summary>
    /// <param name="x">回合数</param>
    public void Stop(int x)
    {
        if (x <= 0)
            return;
        IsOn = false;
        waitcount = x - 1;
        TimeControl.Ins.ep += Wait;
    }
    /// <summary>
    /// 加速
    /// </summary>
    /// <param name="x">加速回合数</param>
    public void SpeedUp(int x)
    {
        if (x <= 0)
            return;
        for (int i = 0; i < x; i++)
        {
            Move();
        }
    }
    void Wait()
    {
        if (waitcount <= 0)
        {
            IsOn = true;
            TimeControl.Ins.ep -= Wait;
            return;
        }
        waitcount--;
    }
    void Move()
    {
        if (!IsOn)
            return;
        one = GameObject.Find("Map/Plane").transform.GetChild(TargetPoint.AreaID - 1);
        two = GameObject.Find("Map/Plane").transform.GetChild(NextPoint.AreaID - 1);
        transform.position = one.position + (Index) * (two.position - one.position) / NextPoint.Weight + new Vector3(0, 1, 0);
        transform.LookAt(two.position+new Vector3(0, 1, 0));
        Index++;
        if (Index > NextPoint.Weight)
        {
            Index = 1;
            TargetPoint = NextPoint;
            ArriPoint(AreaPool.Ins.ReadPool(TargetPoint.AreaID) as Area);
            if (NextPoint.AreaID == End.AreaID)
            {
                IsOn = false;
                TimeControl.Ins.ep -= Move;
                Complete();
                return;
            }
            NextPoint = road.Find(ar => { if (ar.Parent == TargetPoint.AreaID) return true; else return false; });
        }
        if (mp != null)
        {
            mp();
        }
    }
    void Complete()
    {
        count--;
        if (count > 0)
        {
            if (sp != null)
                sp();
            MoveOn(End, mStart, count);
        }
        else
        {
            if (cp != null)
                cp();
            Stop();
        }
    }
    public void StayOn(Area stay)
    {
        one = GameObject.Find("Map/Plane").transform.GetChild(stay.AreaID - 1);
        transform.position = one.position + new Vector3(0, 1, 0);
    }
    protected abstract void ArriPoint(Area o);
}
