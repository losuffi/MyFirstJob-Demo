﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ArmyUnit : IUnit {
    public Army army;

    protected override void ArriPoint(Area o)
    {
        army.CurrentArea = o;
    }
}
