﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class UI_ShowText : MonoBehaviour {
    public List<UILabel> texts = new List<UILabel>();
    public List<GameObject> child = new List<GameObject>();
    public void Work(int i,string content)
    {
        texts[i].text = content;
    }
    public void Hidden(int i)
    {
        child[i].SetActive(false);
    }
    public void View(int i)
    {
        child[i].SetActive(true);
    }
}
