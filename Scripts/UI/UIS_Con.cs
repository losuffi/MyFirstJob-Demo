﻿using UnityEngine;
using System.Collections;
using System;

public class UIS_Con : UIState
{
    public Transform LBperson;
    public GameObject ViewT, Dash;
    private Family second;
    protected override void OnInit(params object[] argss)
    {
        LBperson = m_UIDate.UIModel.transform.Find("Dash/ListBox");
        ViewT = m_UIDate.UIModel.transform.Find("View").gameObject;
        LBperson.GetComponent<ListBox>().DateID = GameStart.Ins.playerfamily.FamilyID;
        LBperson.GetComponent<ListBox>().CreateElement(FamilyPool.Ins.count);
        LBperson.GetComponent<ListBox>().SetClick(ChoseFamily);
        TimeControl.Ins.ep += ClearDiplomacyMessage;
    }

    protected override void OnStart()
    {
        MapSelect.Ins.Islock = true;
        ViewT.SetActive(false);
    }

    protected override void OnStop()
    {
        MapSelect.Ins.Islock = false;
    }
    void ChoseFamily(int i)
    {

        ViewT.SetActive(true);
        second = FamilyPool.Ins.ReadPool(i) as Family;
        if (second == GameStart.Ins.playerfamily)
        {
            ViewT.GetComponent<UI_ShowText>().child[0].SetActive(false);
            ViewT.GetComponent<UI_ShowText>().child[1].SetActive(false);
        }
        else
        {
            ViewT.GetComponent<UI_ShowText>().child[0].SetActive(true);
            ViewT.GetComponent<UI_ShowText>().child[1].SetActive(true);
        }
        ViewT.GetComponent<UI_ShowText>().texts[0].text = second.FamilyName;
        ViewT.GetComponent<UI_ShowText>().texts[1].text = second.FStaff.Count.ToString();
        ViewT.GetComponent<UI_ShowText>().texts[2].text = second.FAsset.CreditX.ToString();
        if (GameStart.Ins.playerfamily != second)
            ViewT.GetComponent<UI_ShowText>().texts[3].text = "家族关系："+(GameStart.Ins.playerfamily.FDiplomacy.GetRelation(second)).Value.ToString();
        else
            ViewT.GetComponent<UI_ShowText>().texts[3].text = "";
        var t = RelationPool.Ins.ReadByFirstID(second.FamilyID);
        Family temp = null;
        double tj = 0;
        for(int j = 0; j < t.Count; j++)
        {
            if (t[j].Coefficient<=tj )
            {
                tj = t[j].Coefficient;
                temp = (FamilyPool.Ins.ReadPool(t[j].FirstFamilyId == second.FamilyID ? t[j].SecondFamilyId : t[j].FirstFamilyId) as Family);
            }
        }
        ViewT.GetComponent<UI_ShowText>().texts[5].text = temp == null ? "无" : temp.FamilyName;
        System.Text.StringBuilder a = new System.Text.StringBuilder();
        foreach(var c in BoardcastDate.Ins.GetOtherLists("Diplomacy"))
        {
            if(c.Title== "player_" + second.FamilyName)
            {
                a.AppendLine(c.Content);
            }
        }
        ViewT.GetComponent<UI_ShowText>().texts[4].text = a.ToString();
        UIEventListener.Get(ViewT.GetComponent<UI_ShowText>().child[0]).onClick = new UIEventListener.VoidDelegate(ar =>
          {
              Fate();
          });
        UIEventListener.Get(ViewT.GetComponent<UI_ShowText>().child[1]).onClick = new UIEventListener.VoidDelegate(ar =>
        {
            Satire();
        });
    }
    void Fate()
    {
        if (second == null)
            return;
        if (GameStart.Ins.playerfamily.FAsset.Cash - 200 < 0)
        {
            BoardcastDate.Ins.RecOther("Diplomacy", "player_" + second.FamilyName, "现金不够，无法宴请！   \n");
            ViewT.GetComponent<UI_ShowText>().texts[4].text += BoardcastDate.Ins.GetOtherLists("Diplomacy").Find(ar =>
            {
                if (ar.Title == "player_" + second.FamilyName && ar.Time == BoardcastDate.Ins.GetOtherLists("Diplomacy").Count - 1)
                {
                    return true;
                }
                return false;
            }).Content;
            return;
        }
        GameStart.Ins.playerfamily.FAsset.Cash -= 200;
        Diplomacy t = new Diplomacy(20, GameStart.Ins.playerfamily, second);
        BoardcastDate.Ins.RecOther("Diplomacy", "player_" + second.FamilyName, "花费200，宴请了一次！   \n");
        BackGroundManager.Ins.RegBGEvent(t);
        ViewT.GetComponent<UI_ShowText>().texts[4].text += BoardcastDate.Ins.GetOtherLists("Diplomacy").Find(ar =>
        {
            if (ar.Title == "player_" + second.FamilyName && ar.Time == BoardcastDate.Ins.GetOtherLists("Diplomacy").Count - 1)
            {
                return true;
            }
            return false;
        }).Content;
        UITopMessage.ins.UpdateMessage();
    }
    private int SatiCount = -1;
    void Satire()
    {
        if (SatiCount == TimeDate.TimeCount)
            return;
        if (second == null)
            return;
        Diplomacy t = new Diplomacy(2, GameStart.Ins.playerfamily, second);
        BoardcastDate.Ins.RecOther("Diplomacy", "player_" + second.FamilyName, "抠门的PY了一次"+second.FamilyName+"家族！   \n");
        BackGroundManager.Ins.RegBGEvent(t);
        ViewT.GetComponent<UI_ShowText>().texts[4].text += BoardcastDate.Ins.GetOtherLists("Diplomacy").Find(ar =>
        {
            if (ar.Title == "player_" + second.FamilyName && ar.Time == BoardcastDate.Ins.GetOtherLists("Diplomacy").Count - 1)
            {
                return true;
            }
            return false;
        }).Content;
        SatiCount = TimeDate.TimeCount;
    }
    void ClearDiplomacyMessage()
    {
        BoardcastDate.Ins.GetOtherLists("Diplomacy").Clear();
    }
}
