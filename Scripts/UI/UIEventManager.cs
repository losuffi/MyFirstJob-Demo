﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class EventUnit
{
    public string Respond;
    public GameObject Trigger;
    [HideInInspector]
    public UIEventListener uie;
    public void BindEvent()
    {
        uie = UIEventListener.Get(Trigger);
        uie.onClick = new UIEventListener.VoidDelegate(ar =>
        {
            if (UIManager.Ins.Currentstate.m_UIDate.UIStateId != 0)
            {
                if(UIManager.Ins.Currentstate.m_UIDate.UIStateName==Respond)
                    UIManager.Ins.SetState("UIS_Empty");
                else
                {
                    UIManager.Ins.SetState("UIS_Empty");
                    UIManager.Ins.SetState(Respond);
                }
                return;
            }
            UIManager.Ins.SetState(Respond);
        });
    }
}
public class UIEventManager : MonoBehaviour {
    public List<EventUnit> events = new List<EventUnit>();
    //private List<UIEventListener> trigs = new List<UIEventListener>();
    private void Start()
    {
        foreach(var eve in events)
        {
            eve.BindEvent();
        }
    }
}
