﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ListBoxValue : IBoxElement
{
    private int poolindex;
    private Area pos;
    private List<Area> areas;
    int LawID, Section;
    private void Awake()
    {
        areas = new List<Area>();
    }
    public override void Work(int i)
    {
        index = i;
        if (DateID == 1)
        {
            GetComponent<UI_ShowText>().texts[0].text = (FamilyPool.Ins.ReadPool(i + 1) as Family).FamilyName;
            GetComponent<UI_ShowText>().texts[1].text = GameStart.Ins.playerfamily.FDiplomacy.GetRelation(FamilyPool.Ins.ReadPool(i + 1) as Family).Value.ToString();
        }
        else if (DateID == 2)
        {
            var p = (FamilyPool.Ins.ReadPool(poolindex) as Family).FStaff.GetPerson(i);
            GetComponent<UI_ShowText>().texts[0].text = p.Name;
            GetComponent<UI_ShowText>().texts[1].text = "倾向："+GameEventBackPro.ConverNature(p.Nature)+",魄：" + p.Force + ",智：" + p.Brain + ",魅：" + p.Charm;
        }
        else if(DateID == 3)
        {
            GetComponent<UI_ShowText>().texts[0].text = "小麦补偿";
            GetComponent<UI_ShowText>().texts[1].text = (500 + i * 500).ToString();
        }
        else if (DateID == 4)
        {
            var p = areas[i].Umpire;
            GetComponent<UI_ShowText>().texts[0].text = areas[i].AreaName + "总督：" + p.Name;
            GetComponent<UI_ShowText>().texts[1].text = "倾向：" + GameEventBackPro.ConverNature(p.Nature) + ",魄：" + p.Force + ",智：" + p.Brain + ",魅：" + p.Charm;
        }
        else if (DateID == 5)
        {
            GetComponent<UI_ShowText>().texts[0].text = "金钱补偿";
            GetComponent<UI_ShowText>().texts[1].text = (500 + i * 500).ToString();
        }
        else if (DateID == 6)
        {
            GetComponent<UI_ShowText>().texts[0].text = "支持：";
            GetComponent<UI_ShowText>().texts[1].text = i == 0 ? (WarEventBackPro.WorldWarEvent.Find(ar => ar.Leader == pos.MinisterA) != null ? pos.MinisterA : pos.Umpire).Name : pos.MinisterB.Name;
        }
        else if (DateID == 7)
        {
            var p = areas[i].Umpire;
            GetComponent<UI_ShowText>().texts[0].text = areas[i].AreaName + "国王：" + p.Name;
            GetComponent<UI_ShowText>().texts[1].text = "倾向：" + GameEventBackPro.ConverNature(p.Nature) + ",魄：" + p.Force + ",智：" + p.Brain + ",魅：" + p.Charm;
        }
        else if (DateID == 8)
        {
            var army = areas[i].armys[0];
            GetComponent<UI_ShowText>().texts[0].text = areas[i].AreaName+":"+army.ArmyName;
            GetComponent<UI_ShowText>().texts[1].text = "战斗力：" + army.ArmyValue + ",组建费用；" + army.ArmyCost;
        }
        else if (DateID == 0)
        {
            //TODO:法案的加挂变量
            GetComponent<UI_ShowText>().texts[0].text = LawPool.Ins[Section].Name.ToString();
            GetComponent<UI_ShowText>().texts[1].text = LawPool.Ins[Section].Sections[i].ToString();
        }
        else if (DateID == 10)
        {
            var area = areas[i];
            GetComponent<UI_ShowText>().texts[0].text = "法案作用区域：";
            GetComponent<UI_ShowText>().texts[1].text = area.AreaName;
        }
        else if (DateID == 11|| DateID == 18||DateID==14)
        {
            var area = areas[i];
            GetComponent<UI_ShowText>().texts[0].text = "指令目标区域：";
            GetComponent<UI_ShowText>().texts[1].text = area.AreaName;
        }
    }
    void GetAlliedNationsArea()
    {
        for (int j = 0; j < AreaPool.Ins.Count; j++)
        {
            var a = AreaPool.Ins.ReadPool(j + 1) as Area;
            if (a.Subjection == 2)
            {
                if (a.Umpire != null)
                {
                    areas.Add(a);
                }
            }
        }
    }
    void GetProvinceArea(int i)
    {
        for (int j = 0; j < AreaPool.Ins.Count; j++)
        {
            var a = AreaPool.Ins.ReadPool(j + 1) as Area;
            if (a.Subjection < i)
            {
                if (a.Umpire != null)
                {
                    areas.Add(a);
                }
            }
        }
    }
    void GetOtherArea(int i)
    {
        for (int j = 0; j < AreaPool.Ins.Count; j++)
        {
            var a = AreaPool.Ins.ReadPool(j + 1) as Area;
            if (a.Subjection > i)
            {
                if (a.Umpire != null)
                {
                    areas.Add(a);
                }
            }
        }
    }
    void GetNeighboringArea()
    {
        for (int j = 0; j < AreaPool.Ins.Count; j++)
        {
            var a = AreaPool.Ins.ReadPool(j + 1) as Area;
            if (Mathf.Abs(a.pos_X - pos.pos_X) + Mathf.Abs(a.pos_Y - pos.pos_Y) <= SingleValuePool.Ins["MaxNearDistance"].IntValue)
            {
                if (a.Subjection < 2)
                {
                    if (a.Umpire != null)
                    {
                        areas.Add(a);
                    }
                }

            }
        }
    }
    public override void SetDateId(params object[] args)
    {
        base.SetDateId(args);
        if(DateID==2&&args.Length>1)
            poolindex = (int)args[1]+1;
        else if(DateID == 4 && args.Length > 1)
        {
            pos = args[1] as Area;
            areas.Clear();
            GetProvinceArea(1);
        }
        else if (DateID == 6 && args.Length > 1)
        {
            pos = args[1] as Area;
        }
        else if (DateID == 7)
        {
            areas.Clear();
            GetAlliedNationsArea();
        }else if (DateID == 8)
        {
            areas.Clear();
            GetAlliedNationsArea();
        }
        else if (DateID == 0)
        {
            LawID = (int)args[1];
            Section = (int)args[2];
        }
        else if (DateID == 10)
        {
            areas.Clear();
            GetProvinceArea(2);
        }
        else if (DateID == 11||DateID==14||DateID==18)
        {
            areas.Clear();
            GetOtherArea(1);
        }
    }
}
