﻿using UnityEngine;
using System.Collections;
using System;
public class P4Anim : MonoBehaviour {
    public Action click;
    public void Work(Argue a,Argue b)
    {
        gameObject.SetActive(true);
        var Aa = GetComponent<UI_ShowText>().child[2].GetComponent<UI_ShowText>();
        Aa.texts[0].text = a.ValueA.ToString();
        Aa.texts[1].text = a.ValueB.ToString();
        switch (a.Kind)
        {
            case 1:
                Aa.child[3].SetActive(false);
                Aa.child[2].SetActive(false);
                Aa.child[1].SetActive(true);
                break;
            case 2:
                Aa.child[3].SetActive(false);
                Aa.child[2].SetActive(true);
                Aa.child[1].SetActive(false);
                break;
            default:
                Aa.child[3].SetActive(true);
                Aa.child[2].SetActive(false);
                Aa.child[1].SetActive(false);
                break;
        }
        Aa.texts[3].text = a.Cost.ToString();
        Aa.texts[4].text = a.Dec;
        var Bb = GetComponent<UI_ShowText>().child[3].GetComponent<UI_ShowText>();
        Bb.texts[0].text = b.ValueA.ToString();
        Bb.texts[1].text = b.ValueB.ToString();
        switch (b.Kind)
        {
            case 1:
                Bb.child[3].SetActive(false);
                Bb.child[2].SetActive(false);
                Bb.child[1].SetActive(true);
                break;
            case 2:
                Bb.child[3].SetActive(false);
                Bb.child[2].SetActive(true);
                Bb.child[1].SetActive(false);
                break;
            default:
                Bb.child[3].SetActive(true);
                Bb.child[2].SetActive(false);
                Bb.child[1].SetActive(false);
                break;
        }
        Bb.texts[3].text = b.Cost.ToString();
        Bb.texts[4].text = b.Dec;
    }
	void Update () {
        if (Input.GetMouseButton(0))
        {
            gameObject.SetActive(false);
            if (click != null)
                click();
        }
	}
}
