﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ListBoxLawValues : IBoxElement
{
    string Topic;
    List<ALaw> laws;
    public override void Work(int i)
    {
        index = i;
        GetComponent<UI_ShowText>().texts[0].text= laws[i].Name;
        GetComponent<UI_ShowText>().texts[1].text = laws[i].Value.ToString();
    }
    public override void SetDateId(params object[] args)
    {
        base.SetDateId(args);
        Topic = args[1].ToString();
        laws = LawPool.Ins.GetLawsByTopic(Topic);
    }
}
