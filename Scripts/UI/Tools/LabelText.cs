﻿using UnityEngine;
using System.Collections;

public class LabelText : MonoBehaviour {
    public string SingleValueName;
    private void Start()
    {
        GameStart.Ins.InitMethod += Show;
    }
    void Show()
    {
        //Debug.Log(transform.parent.name+gameObject.name);
        if (!SingleValuePool.Ins.Countain(SingleValueName))
        {
            Debug.Log(gameObject.name);
        }
        else
        {
            GetComponent<UILabel>().text = SingleValuePool.Ins[SingleValueName].StringValue;
        }
        GameStart.Ins.InitMethod -= Show;
    }
}
