﻿using UnityEngine;
using System.Collections;
using System;

public class ListBoxEstate : IBoxElement
{
    Family fam;
    public override void SetDateId(params object[] args)
    {
        base.SetDateId(args);
        fam = FamilyPool.Ins.ReadPool(DateID) as Family;
    }
    public override void Work(int i)
    {
        var temp = fam.FAsset.estates[i];
        GetComponent<UI_ShowText>().Work(0, temp.cro.Consume.ToString());
        GetComponent<UI_ShowText>().Work(1, temp.area.AreaName);
        GetComponent<UI_ShowText>().Work(2, temp.cro.Month.ToString());
        GetComponent<UI_ShowText>().Work(3, temp.cro.Earning.ToString());
        GetComponent<UI_ShowText>().Work(4, temp.cro.CropName);
        GetComponent<UI_ShowText>().Work(5, temp.IsWork ? "运作中" : ("开垦中: \n 剩余建造时间" + temp.CostTime));
    }
}
