﻿using UnityEngine;
using System.Collections;
using System;

public class ListBoxBoardcast : IBoxElement
{
    public override void Work(int i)
    {
        //throw new NotImplementedException();
        var c = BoardcastDate.Ins.Get(i);
        index = i;
        GetComponent<UI_ShowText>().texts[0].text = TimeControl.Ins.TimeConvert(c.Time);
        GetComponent<UI_ShowText>().texts[1].text = c.Title;
        GetComponent<UI_ShowText>().texts[2].text = c.Content;
    }

}
