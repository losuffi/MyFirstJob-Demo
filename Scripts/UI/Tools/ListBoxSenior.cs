﻿using UnityEngine;
using System.Collections;
using System;

public class ListBoxSenior :IBoxElement {
    public override void Work(int i)
    {
        if (FamilyPool.Ins == null)
            return;
        index = i;
        Family t = FamilyPool.Ins.ReadPool(DateID) as Family;
        var x = t.FStaff.Seniors[i];
        var showtext = GetComponent<UI_ShowText>();
        showtext.texts[0].text = x.Name;
        showtext.texts[1].text = x.Force.ToString();
        showtext.texts[2].text = x.Brain.ToString();
        showtext.texts[3].text = x.Charm.ToString();
        if (x.Sex)
            showtext.texts[4].text = "Man";
        else
            showtext.texts[4].text = "Woman";
        showtext.texts[5].text = x.Age.ToString();
        if (x.Idel)
            showtext.texts[6].text = "Idel";
        else
            showtext.texts[6].text = "Busy";
        showtext.child[0].GetComponent<UISprite>().spriteName = ListBoxPerson.JobSprite(x.Job);
    }

}
