﻿using UnityEngine;
using System.Collections;
using System;

public class ListBoxSituation :IBoxElement
{
    public override void Work(int i)
    {
        index = i;
        var temp = GameEventBox.Ins[i];
        if (temp == null)
            return;
        if (temp.IsLaw)
            GetComponent<UI_ShowText>().texts[0].text = "法案更改";
        else
            GetComponent<UI_ShowText>().texts[0].text = temp.TriggerArea != null ? temp.TriggerArea.AreaName : "";
        GetComponent<UI_ShowText>().texts[1].text = temp.Name;
        GetComponent<UI_ShowText>().texts[2].text = "";
    }
}
