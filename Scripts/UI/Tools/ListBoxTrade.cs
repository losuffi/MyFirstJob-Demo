﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ListBoxTrade : IBoxElement
{
    Family fm;
    List<Person> tradeper;
    public override void SetDateId(params object[] args)
    {
        base.SetDateId(args);
        fm = FamilyPool.Ins.ReadPool(DateID) as Family;
        tradeper = new List<Person>();
        for (int j = 0; j < fm.FStaff.Count; j++)
        {
            var per = fm.FStaff.GetPerson(j);
            if (per.taskKind == Person.TaskKind.Trade)
            {
                tradeper.Add(per);
            }
        }
    }
    public override void Work(int i)
    {
        var showtext = GetComponent<UI_ShowText>();
        var x = tradeper[i];
        showtext.texts[0].text = x.Name;
        showtext.texts[1].text = x.Force.ToString();
        showtext.texts[2].text = x.Brain.ToString();
        showtext.texts[3].text = x.Charm.ToString();
        if (x.Sex)
            showtext.texts[4].text = "Man";
        else
            showtext.texts[4].text = "Woman";
        showtext.texts[5].text = x.Age.ToString();
        if (x.Idel)
            showtext.texts[6].text = "Idel";
        else
            showtext.texts[6].text = "Busy";
        showtext.child[0].GetComponent<UISprite>().spriteName = ListBoxPerson.JobSprite(x.Job);
        if (x.Idel)
        {
            showtext.texts[7].text = "";
        }
        else
        {
            showtext.texts[7].text = "贸易中-当前位置-目标位置：\n" + x.StayArea.AreaName + "-" + x.TargetArea.AreaName;
        }
    }
}
