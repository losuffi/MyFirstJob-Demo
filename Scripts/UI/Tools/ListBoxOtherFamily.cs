﻿using UnityEngine;
using System.Collections;
using System;

public class ListBoxOtherFamily :IBoxElement
{
    public int Id;

    public override void SetDateId(params object[] args)
    {
        base.SetDateId(args);
    }
    public override void Work(int i)
    {
        if (RelationPool.Ins == null|| FamilyPool.Ins == null)
            return;
        index = i+1;
        var show_text = GetComponent<UI_ShowText>();
        var x = FamilyPool.Ins.ReadPool(index) as Family;
        if (x == GameStart.Ins.playerfamily)
        {
            show_text.child[0].GetComponent<UISprite>().color = Color.red;
        }
        show_text.texts[0].text = x.FamilyName;
    }
        
}
