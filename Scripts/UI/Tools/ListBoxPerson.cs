﻿using UnityEngine;
using System.Collections;
using System;
public class ListBoxPerson : IBoxElement {
    int PerListKind;
    public override void SetDateId(params object[] args)
    {
        base.SetDateId(args);
        if (args.Length > 1)
            PerListKind = (int)args[1];
    }
    public override void Work(int i)
    {
        if (FamilyPool.Ins == null)
            return;
        index = i;
        Family t=null;
        t = FamilyPool.Ins.ReadPool(DateID) as Family;
        Person x = null;
        if (PerListKind != 1)
            x = t.FStaff.GetPerson(i);
        else
            x = t.FStaff.IdelPerson[i];
        var showtext = GetComponent<UI_ShowText>();
        showtext.texts[0].text = x.Name;
        showtext.texts[1].text = x.Force.ToString();
        showtext.texts[2].text = x.Brain.ToString();
        showtext.texts[3].text = x.Charm.ToString();
        if(x.Sex)
            showtext.texts[4].text = "男";
        else
            showtext.texts[4].text = "女";
        showtext.texts[5].text = x.Age.ToString();
        if (x.Idel)
            showtext.texts[6].text = "空闲";
        else
            showtext.texts[6].text = "忙碌";
        showtext.child[0].GetComponent<UISprite>().spriteName = JobSprite(x.Job);
        if (x.Idel)
        {
            showtext.texts[7].text = "";
        }
        else
        {
            if(x.TargetArea!=null&& (TimeDate.TimeCount - x.TaskRegCount)<= (x.TaskCost))
                showtext.texts[7].text = x.TargetArea.AreaName + ":" + (TimeDate.TimeCount - x.TaskRegCount) + "/" + (x.TaskCost);
        }
    }
    public static string JobSprite(Person.JobKind t)
    {
        switch (t)
        {
            case Person.JobKind.Consul:
                return "Consual";
            case Person.JobKind.ConsuledSenior:
                return "ConsualSenior";
            case Person.JobKind.ConsulUmpire:
                return "ConsualUmpire";
            case Person.JobKind.Senior:
                return "Senior";
            case Person.JobKind.Umpire:
                return "Umpire";
            case Person.JobKind.UmpiredSenior:
                return "UmpireSenior";
            default:
                return "None";
        }
    }
}
