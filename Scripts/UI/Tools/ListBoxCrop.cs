﻿using UnityEngine;
using System.Collections;
using System;

public class ListBoxCrop : IBoxElement
{
    public override void Work(int i)
    {
        index = i + 1;
        var temp = CropPool.Ins.ReadPool(index) as Crop;
        GetComponent<UI_ShowText>().Work(0, temp.CropName);
        GetComponent<UI_ShowText>().Work(1, temp.Consume.ToString());
        GetComponent<UI_ShowText>().Work(2, temp.Cd.ToString());
        GetComponent<UI_ShowText>().Work(3, temp.Month.ToString());
        GetComponent<UI_ShowText>().Work(4, temp.Earning.ToString());
    }
}
