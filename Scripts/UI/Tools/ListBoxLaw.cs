﻿using UnityEngine;
using System.Collections;
using System;

public class ListBoxLaw : IBoxElement
{
    public override void Work(int i)
    {
        index = i;
        var t = LawPool.Ins.Topics[index];
        int count = GetComponent<UI_ShowText>().child[1].transform.childCount;
        GetComponent<UI_ShowText>().texts[0].text = t;
        GetComponent<UI_ShowText>().child[0].GetComponent<ListBox>().DateID = i + 1;
        GetComponent<UI_ShowText>().child[0].GetComponent<ListBox>().CreateElement(LawPool.Ins.GetLawsByTopic(t).Count - count, t);
    }
}
