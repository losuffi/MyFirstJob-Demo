﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class ListBox : MonoBehaviour {
    public bool IsHorizon;
    [HideInInspector]
    public Transform Content;
    public UILabel mess;
    private int heigt;
    public  int startindex,viewcount;
    public int pacing,cheigt;
    public GameObject Model;
    public int DateID;
    private void Awake()
    {
        Content = transform.Find("Content");
        heigt = Content.GetComponent<UIWidget>().height;
        viewcount = heigt / cheigt;
        startindex = 0;
    }
    public void View()
    {

    }
    public void OnGUI()
    {
        if (!gameObject.activeInHierarchy)
            return;
        if (IsHorizon)
        {
            heigt = Content.GetComponent<UIWidget>().height;
            viewcount = heigt / cheigt;
            mess.text = (startindex / viewcount + 1) + "/" + (Content.childCount / viewcount + 1);
            for (int i = 0; (i < Content.childCount); i++)
            {
                if (i >= startindex && i < startindex + viewcount)
                {
                    var c = Content.GetChild(i).GetComponent<UIWidget>();
                    c.height = cheigt - pacing;
                    if (i == startindex)
                    {
                        Content.GetChild(i).localPosition = new Vector3(0, heigt / 2) - new Vector3(0, c.height / 2);
                    }
                    else
                    {
                        Content.GetChild(i).localPosition = Content.GetChild(i - 1).localPosition - new Vector3(0, cheigt);
                    }
                    //c.SetAnchor(Content.gameObject, (Content.GetComponent<UIWidget>().width - c.width) / 2,
                    //    c.height * (startindex+viewcount - 1 - i), -(Content.GetComponent<UIWidget>().width - c.width) / 2, -c.height * (i - startindex)
                    //    );
                    Content.GetChild(i).GetComponent<IBoxElement>().Work(i);
                    c.gameObject.SetActive(true);
                }
                else
                {
                    Content.GetChild(i).gameObject.SetActive(false);
                }
            }
        }
        else
        {
            heigt = Content.GetComponent<UIWidget>().width;
            viewcount = heigt / cheigt;
            mess.text = (startindex / viewcount + 1) + "/" + (Content.childCount / viewcount + 1);
            for (int i = 0; (i < Content.childCount); i++)
            {
                if (i >= startindex && i < startindex + viewcount)
                {
                    var c = Content.GetChild(i).GetComponent<UIWidget>();
                    c.width = cheigt - pacing;
                    if (i == startindex)
                    {
                        Content.GetChild(i).localPosition = new Vector3(-heigt / 2, 0) + new Vector3(c.height / 2, 0);
                    }
                    else
                    {
                        Content.GetChild(i).localPosition = Content.GetChild(i - 1).localPosition + new Vector3(cheigt, 0);
                    }
                    //c.SetAnchor(Content.gameObject, (Content.GetComponent<UIWidget>().width - c.width) / 2,
                    //    c.height * (startindex+viewcount - 1 - i), -(Content.GetComponent<UIWidget>().width - c.width) / 2, -c.height * (i - startindex)
                    //    );
                    Content.GetChild(i).GetComponent<IBoxElement>().Work(i);
                    c.gameObject.SetActive(true);
                }
                else
                {
                    Content.GetChild(i).gameObject.SetActive(false);
                }
            }
        }
    }
    public void CreateElement(int i)
    {
        for(int j = 0; j < i; j++)
        {
            var t = Instantiate(Model) as GameObject;
            t.GetComponent<IBoxElement>().SetDateId(DateID);
            t.transform.SetParent(Content);
            t.transform.localScale = Vector3.one;
            t.transform.localPosition = Vector3.zero;
        }
    }
    public void CreateElement(int i,int m)
    {
        for (int j = 0; j < i; j++)
        {
            var t = Instantiate(Model) as GameObject;
            t.GetComponent<IBoxElement>().SetDateId(DateID,m);
            t.transform.SetParent(Content);
            t.transform.localScale = Vector3.one;
            t.transform.localPosition = Vector3.zero;
        }
    }
    public void CreateElement(int i, int m,int n)
    {
        for (int j = 0; j < i; j++)
        {
            var t = Instantiate(Model) as GameObject;
            t.GetComponent<IBoxElement>().SetDateId(DateID, m, n);
            t.transform.SetParent(Content);
            t.transform.localScale = Vector3.one;
            t.transform.localPosition = Vector3.zero;
        }
    }
    public void CreateElement(int i, string str)
    {
        for (int j = 0; j < i; j++)
        {
            var t = Instantiate(Model) as GameObject;
            t.GetComponent<IBoxElement>().SetDateId(DateID,str);
            t.transform.SetParent(Content);
            t.transform.localScale = Vector3.one;
            t.transform.localPosition = Vector3.zero;
        }
    }
    public void CreateElement(int i, Area ar)
    {
        for (int j = 0; j < i; j++)
        {
            var t = Instantiate(Model) as GameObject;
            t.GetComponent<IBoxElement>().SetDateId(DateID, ar);
            t.transform.SetParent(Content);
            t.transform.localScale = Vector3.one;
            t.transform.localPosition = Vector3.zero;
        }
    }
    public void CreateElement(int i, Situation ar)
    {
        for (int j = 0; j < i; j++)
        {
            var t = Instantiate(Model) as GameObject;
            t.GetComponent<IBoxElement>().SetDateId(DateID, ar);
            t.transform.SetParent(Content);
            t.transform.localScale = Vector3.one;
            t.transform.localPosition = Vector3.zero;
        }
    }
    public void Next()
    {
        if (startindex + viewcount > Content.childCount)
            return;
        startindex += viewcount;
        View();
    }
    public void Pre()
    {
        if (startindex - viewcount < 0)
            return;
        startindex -= viewcount;
        View();
    }
    public void SetClick(Action<int> t)
    {
        for (int i = 0; (i < Content.childCount); i++)
        {
            Content.GetChild(i).GetComponent<IBoxElement>().SetClick(t);
        }

    }
    public void Clear()
    {
        Content.DestroyChildren();
    }
}
