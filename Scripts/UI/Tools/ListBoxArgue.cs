﻿using UnityEngine;
using System.Collections;
using System;

public class ListBoxArgue : IBoxElement
{

    public override void Work(int i)
    {
        index = i;
        var temp = ArguePool.Ins.GetKind(DateID)[i];
        gameObject.GetComponent<UI_ShowText>().texts[0].text = temp.ValueA.ToString();
        gameObject.GetComponent<UI_ShowText>().texts[1].text = temp.ValueB.ToString();
        switch (temp.Kind)
        {
            case 1:
                gameObject.GetComponent<UI_ShowText>().child[3].SetActive(false);
                gameObject.GetComponent<UI_ShowText>().child[2].SetActive(false);
                gameObject.GetComponent<UI_ShowText>().child[1].SetActive(true);
                break;
            case 2:
                gameObject.GetComponent<UI_ShowText>().child[3].SetActive(false);
                gameObject.GetComponent<UI_ShowText>().child[2].SetActive(true);
                gameObject.GetComponent<UI_ShowText>().child[1].SetActive(false);
                break;
            default:
                gameObject.GetComponent<UI_ShowText>().child[3].SetActive(true);
                gameObject.GetComponent<UI_ShowText>().child[2].SetActive(false);
                gameObject.GetComponent<UI_ShowText>().child[1].SetActive(false);
                break;
        }
        gameObject.GetComponent<UI_ShowText>().texts[3].text = temp.Cost.ToString();
        gameObject.GetComponent<UI_ShowText>().texts[4].text = temp.Dec;
    }
}
