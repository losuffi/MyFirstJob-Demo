﻿using UnityEngine;
using System.Collections;
using System;

public class ListBoxMotion : IBoxElement
{
    Situation situ;
    public override void Work(int i)
    {
        index = i;
        var temp = situ.motions[i];
        if (situ.IsLaw)
        {
            foreach (var c in GetComponent<UI_ShowText>().child)
            {
                c.SetActive(false);
            }
            GetComponent<UI_ShowText>().child[7].SetActive(true);
            GetComponent<UI_ShowText>().texts[7].text = LawPool.Ins[temp.ID].CreditXCost.ToString();
        }
        else
        {
            foreach (var c in GetComponent<UI_ShowText>().child)
            {
                c.SetActive(true);
            }
            GetComponent<UI_ShowText>().child[7].SetActive(false);
            GetComponent<UI_ShowText>().texts[0].text = temp.Name;
            GetComponent<UI_ShowText>().texts[1].text = temp.Value.ToString();
            GetComponent<UI_ShowText>().texts[2].text = temp.RelaADD.ToString();
            GetComponent<UI_ShowText>().texts[3].text = temp.CreditX.ToString();
            GetComponent<UI_ShowText>().texts[4].text = temp.CreditY.ToString();
            GetComponent<UI_ShowText>().texts[5].text = temp.Score.ToString();
            GetComponent<UI_ShowText>().texts[6].text = temp.RelaSub.ToString();
        }
        GetComponent<UI_ShowText>().child[0].SetActive(true);
        GetComponent<UI_ShowText>().texts[0].text = temp.Name;
    }
    public override void SetDateId(params object[] args)
    {
        base.SetDateId(args);
        situ = args[1] as Situation;
    }
}
