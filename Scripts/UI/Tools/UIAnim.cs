﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class Target
{
    public float X;
    public float Y;
    public float Time;
}
public class UIAnim : MonoBehaviour {
    public bool IsPlay { get; set; }
    public bool IsPosition;
    public Target _tar;
    float initTime = 0;
    private void Awake()
    {
        IsPlay = false;
        initTime = Time.time;
    }
    private void Update()
    {
        if (!IsPlay)
            return;
        if (Time.time - initTime < _tar.Time)
        {
            transform.localPosition += new Vector3(_tar.X / _tar.Time, _tar.Y / _tar.Time);
        }
    }
}
