﻿using UnityEngine;
using System.Collections;
using System;
public abstract class IBoxElement :MonoBehaviour  {
    protected int DateID, index;
    public abstract void Work(int i);
    public virtual void SetDateId(params object[] args)
    {
        DateID = (int)args[0];
    }
    public virtual void SetClick(Action<int> onclick)
    {
        if (UIEventListener.Get(gameObject) == null)
            return;
        UIEventListener.Get(gameObject).onClick = new UIEventListener.VoidDelegate(ar =>
        {
            if (onclick != null)
                onclick(index);
        });
    }
}
