﻿using UnityEngine;
using System.Collections;

public class ViewControl : MonoBehaviour {
    public float Speed;
    public float RollSpeed;
    public float MoveSpeed;
    void Update() {
        if (!MapSelect.Ins.Islock)
        {
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                float height = -Input.GetAxis("Mouse ScrollWheel");
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y + height * Speed, transform.position.z), RollSpeed * Time.deltaTime);
            }
            if (Input.GetMouseButton(0))
            {
                if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
                {
                    float x = -Input.GetAxis("Mouse X");
                    float y = -Input.GetAxis("Mouse Y");
                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x + x * Speed, transform.position.y, transform.position.z + y * Speed), MoveSpeed * Time.deltaTime);
                }
            }
        }
        if (Input.GetMouseButton(1))
        {
            if (GameStart.Ins.playerfamily.seniorSetCount > 0)
                return;
            UIManager.Ins.SetState("UIS_Empty");
        }
    }
}
