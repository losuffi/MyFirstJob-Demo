﻿using UnityEngine;
using System.Collections;

public class Cube : MonoBehaviour {
    public int AreaId;
    public Area AreaDate;
    public bool IsSelected;
    private Material m;
    private Camera mc, nc;
    private Transform lab;
    public UILabel FontSize;
    private void Awake()
    {
        IsSelected = false;
        m = GetComponent<Renderer>().material;
        lab = FontSize.transform;
        mc = NGUITools.FindCameraForLayer(gameObject.layer);
        nc = NGUITools.FindCameraForLayer(lab.gameObject.layer);
        AreaId=int.Parse(transform.name.Substring(4));
        C_province = new Color(0, 0.56f, 0, 1);
        C_league = new Color(0, 0.4f, 1, 1);
        C_indifference = new Color(0.3f, 0, 0, 1);
    }
    Color C_province;
    Color C_league;
    Color C_indifference;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && IsSelected)
        {
            if (MapSelect.Ins.Islock)
                return;
            if (AreaDate.Subjection == 1)
            {
                UIManager.Ins.SetState("UIS_Capital");
                return;
            }
            UIManager.Ins.SetState("UIS_Area");
        }
    }
    private void LateUpdate()
    {
        if (AreaDate == null)
        {
            AreaDate = AreaPool.Ins.ReadPool(AreaId) as Area;
            lab.GetComponent<UILabel>().text = AreaDate.AreaName;
            //transform.position = 4 * (new Vector3(AreaDate.pos_X, 0, AreaDate.pos_Y));
            return;
        }
        var temp = mc.WorldToScreenPoint(transform.position);
        temp.z = 0;
        temp = nc.ScreenToWorldPoint(temp);
        lab.position = temp;
        FontSize.fontSize = 45 - (int)(15 * Vector3.Distance(transform.position, mc.transform.position) / 30);
        if (MapSelect.Ins.Islock)
        {
            return;
        }
        if (!IsSelected)
        {
            //m.color = Color.white;
            if (AreaDate.Subjection == 2)//同盟
            {
                m.color = C_league;
            }
            else if (AreaDate.Subjection == 4)//冷淡
            {
                m.color = C_indifference;
            }
            else if (AreaDate.Subjection == 0)//行省
            {
                m.color = C_province;
            }
            else if (AreaDate.Subjection == 1)//首都
            {
                m.color = Color.green;
            }
            else if (AreaDate.Subjection == 3)//中立
            {
                m.color = Color.gray;
            }
            if (AreaDate.IsTheFalledArea)
            {
                m.color = Color.black;
            }
            m.SetColor("_Line", Color.black);
        }
        else
        {
            //m.color = Color.yellow;
            m.SetColor("_Line", Color.white);
        }
    }
}
