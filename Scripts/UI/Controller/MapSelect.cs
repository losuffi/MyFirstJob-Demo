﻿using UnityEngine;
using System.Collections;

public class MapSelect : MonoBehaviour {
    private Camera m;
    private RaycastHit hits;
    public Collider cache;
    public static MapSelect Ins;
    [HideInInspector]
    public bool Islock;
    private void Awake()
    {
        Ins = this;
        m = gameObject.GetComponent<Camera>();
        Islock = false;
    }
    private void Update()
    {
        if (Islock)
            return;
        //Unit
        if (Physics.Raycast(m.ScreenPointToRay(Input.mousePosition), out hits, 100, 1 << 8))
        {
            if (hits.collider.tag.Equals("Unit"))
            {
                if (hits.collider.GetComponent<IUnit>().Name.Equals("Army"))
                {
                    Cursor.visible = false;
                    UIArmy.Ins.View(hits.collider.GetComponent<ArmyUnit>().army);
                }
            }
            else
            {
                Cursor.visible = true;
                UIArmy.Ins.gameObject.SetActive(false);
            }
        }
        else
        {
            Cursor.visible = true;
            UIArmy.Ins.gameObject.SetActive(false);
        }
        if (Physics.Raycast(m.ScreenPointToRay(Input.mousePosition),out hits, 100,1<<9))
        {
            //MapCube;
            if (hits.collider.tag.Equals("Cube"))
            {
                if (hits.collider == cache)
                    return;
                if (cache != null)
                    cache.GetComponent<Cube>().IsSelected = false;
                cache = hits.collider;
                cache.GetComponent<Cube>().IsSelected = true;
                if (!UIAreaMessage.Ins.gameObject.activeInHierarchy)
                    UIAreaMessage.Ins.gameObject.SetActive(true);
                UIAreaMessage.Ins.View(hits.collider.GetComponent<Cube>().AreaDate);
            }
            else
            {
                if (cache == null)
                    return;
                cache.GetComponent<Cube>().IsSelected = false;
                cache = null;
                UIAreaMessage.Ins.gameObject.SetActive(false);
            }
        }
    }
}
