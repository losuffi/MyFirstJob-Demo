﻿using UnityEngine;
using System.Collections;
using System;

public class UIS_Man : UIState
{
    public Transform LBperson;
    private bool IsSetSenior;
    protected override void OnInit(params object[] argss)
    {
        LBperson = m_UIDate.UIModel.transform.Find("ListBox");
        LBperson.GetComponent<ListBox>().DateID = GameStart.Ins.playerfamily.FamilyID;
        LBperson.GetComponent<ListBox>().CreateElement(GameStart.Ins.playerfamily.FStaff.Count);
        LBperson.GetComponent<ListBox>().SetClick(ChoseMember);
    }

    protected override void OnStart()
    {
        MapSelect.Ins.Islock = true;
        var t = GameStart.Ins.playerfamily.FStaff.Count - LBperson.GetComponent<ListBox>().Content.childCount;
        LBperson.GetComponent<ListBox>().CreateElement(t);
        LBperson.GetComponent<ListBox>().View();
        if (GameStart.Ins.playerfamily.seniorSetCount > 0)
        {
            IsSetSenior = true;
            m_UIDate.UIModel.transform.Find("NoteChioseSenior").gameObject.SetActive(true);
        }
        else
        {
            IsSetSenior = false;
            m_UIDate.UIModel.transform.Find("NoteChioseSenior").gameObject.SetActive(false);
        }
        if (IsSetSenior)
            UIManager.Ins.StateIsLock = true;
    }

    protected override void OnStop()
    {
        MapSelect.Ins.Islock = false;
    }
    void ChoseMember(int index)
    {
        if (!IsSetSenior)
            return;
        GameStart.Ins.playerfamily.FStaff.GetPerson(index).Job = Person.JobKind.Senior;
        GameStart.Ins.playerfamily.seniorSetCount--;
        UIManager.Ins.StateIsLock = false;
        UIManager.Ins.SetState("UIS_Empty");
    }
}
