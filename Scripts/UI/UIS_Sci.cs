﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System;
/// <summary>
/// 需要额外保存每个事件的AI提案。后根据AI提案，来显示。
/// </summary>
public class UIS_Sci : UIState
{
    public bool IsJoined;
    private Situation currentsituation;
    private Person currentPerson;
    private Motion currentmotion;
    private Argue currentArgue, otherArgue, EmptyArgue;
    private GameEventBackPro player, other;
    private int count, width1, width2;
    public Transform LB1, LB2, LB3, AB4, AB5, AB6, EB7, EB8, Consul;
    protected override void OnInit(params object[] argss)
    {
        IsJoined = false;
        LB1 = m_UIDate.UIModel.transform.Find("P1/ListBox");
        LB2 = m_UIDate.UIModel.transform.Find("P2/ListBox");
        LB3 = m_UIDate.UIModel.transform.Find("P3/ListBox");
        AB4 = m_UIDate.UIModel.transform.Find("P4/ArgueListBox1");
        AB5 = m_UIDate.UIModel.transform.Find("P4/ArgueListBox2");
        AB6 = m_UIDate.UIModel.transform.Find("P4/ArgueListBox3");
        EB7 = m_UIDate.UIModel.transform.Find("Exct3/ListBox");
        EB8 = m_UIDate.UIModel.transform.Find("Exct4/ListBox");
        Consul = m_UIDate.UIModel.transform.Find("Consul");
        EmptyArgue = new Argue();
        EmptyArgue.Cost = 0;
        EmptyArgue.Dec = "空";
        EmptyArgue.ID = 99;
        EmptyArgue.Kind = 4;
        EmptyArgue.ValueA = 0;
        EmptyArgue.ValueB = 0;
        var p = m_UIDate.UIModel.GetComponent<UI_ShowText>().child[3].GetComponent<UI_ShowText>();
        width1 = p.child[7].transform.Find("Sprite").GetComponent<UISprite>().width;
        width2 = p.child[8].transform.Find("Sprite").GetComponent<UISprite>().width;
        p.child[7].transform.Find("Sprite").GetComponent<UIRect>().SetAnchor(p.child[7]);
        p.child[8].transform.Find("Sprite").GetComponent<UIRect>().SetAnchor(p.child[8]);
        //LB1.GetComponent<ListBox>().CreateElement(GameEventBox.Ins.Count);
    }
    /// <summary>
    /// 选事件
    /// </summary>
    protected override void OnStart()
    {
        MapSelect.Ins.Islock = true;
        count = 1;
        Consul.gameObject.SetActive(true);
        Person t = AreaPool.Ins.Capital().Umpire;
        LB1.GetComponent<ListBox>().Clear();
        LB1.GetComponent<ListBox>().DateID = GameStart.Ins.playerfamily.FamilyID;
        LB1.GetComponent<ListBox>().CreateElement(GameEventBox.Ins.Count);
        LB1.GetComponent<ListBox>().SetClick(LB1Click);
        if (t != null)
        {
            Consul.GetComponent<UI_ShowText>().texts[0].text = t.Name + "-" + t.staff.fam.FamilyName;
            Consul.GetComponent<UI_ShowText>().texts[1].text = t.Force.ToString();
            Consul.GetComponent<UI_ShowText>().texts[2].text = t.Brain.ToString();
            Consul.GetComponent<UI_ShowText>().texts[3].text = t.Charm.ToString();
            Consul.GetComponent<UI_ShowText>().texts[4].text = GameEventBackPro.ConverNature(t.Nature);
            Consul.GetComponent<UI_ShowText>().texts[5].text = TimeControl.Ins.TimeConvert(t.TaskRegCount + t.TaskCost);
        }
        View(0);
    }

    protected override void OnStop()
    {
        MapSelect.Ins.Islock = false;
        View(-1);
    }
    /// <summary>
    /// 选人
    /// </summary>
    /// <param name="index"></param>
    void LB1Click(int index)
    {
        currentsituation = GameEventBox.Ins[index];
        if (!currentsituation.IsLaw)
            other = GameEventBox.Ins.Gebps[currentsituation];
        View(1);
        if (AreaPool.Ins.Capital().Umpire.staff.fam != GameStart.Ins.playerfamily || !AreaPool.Ins.Capital().Umpire.StayArea.Equals(AreaPool.Ins.Capital()))
        {
            LB2.GetComponent<ListBox>().Clear();
            LB2.GetComponent<ListBox>().DateID = GameStart.Ins.playerfamily.FamilyID;
            LB2.GetComponent<ListBox>().CreateElement(GameStart.Ins.playerfamily.FStaff.Seniors.Count);
            LB2.GetComponent<ListBox>().SetClick(LB2Click);
        }
        else
        {
            LB2Click(AreaPool.Ins.Capital().Umpire.staff.GetPersonIndex((AreaPool.Ins.Capital().Umpire)));
        }
    }
    /// <summary>
    /// 选提案
    /// </summary>
    /// <param name="index"></param>
    void LB2Click(int index)
    {
        currentPerson = GameStart.Ins.playerfamily.FStaff.Seniors[index];
        View(2);
        var text = m_UIDate.UIModel.GetComponent<UI_ShowText>().child[2].GetComponent<UI_ShowText>();
        Motion agree = MotionPool.Ins[19] as Motion;
        foreach (var c in text.child)
        {
            //AI提案项 是否显示，如果为法案型Situation，应该不显示
            c.SetActive(!currentsituation.IsLaw);
        }
        text.texts[0].text = agree.CreditY.ToString();
        text.texts[1].text = agree.CreditX.ToString();
        text.texts[2].text = agree.RelaADD.ToString();
        text.texts[3].text = agree.Score.ToString();
        string exct = "";
        if (!currentsituation.IsLaw)
        {
            if (other.excts.ContainsKey(0))
                exct = other.moti.Name + "-" + other.excts[0].ToString();
            else if (other.excts.ContainsKey(1))
                exct = other.moti.Name + "-" + (other.excts[1] as Person).Name + "-" + ((other.excts[1] as Person).staff == null ? " " : ((other.excts[1] as Person).staff.fam.FamilyName));
            else
                exct = other.moti.Name;
            text.texts[4].text = (other.person.Job == Person.JobKind.Consul ? "执政官提案:\n" : "元老院提案:\n") + exct;
            UIEventListener.Get(text.child[0]).onClick = new UIEventListener.VoidDelegate(ar =>
            {
                GameEventBox.Ins.PassGebp(other);
                UIManager.Ins.StateIsLock = false;
                UIManager.Ins.SetState("UIS_Empty");
            });
        }
        else
        {
            text.texts[4].text = exct;
            UIEventListener.Get(text.child[0]).onClick = null;
        }
        LB3.GetComponent<ListBox>().Clear();
        LB3.GetComponent<ListBox>().DateID = currentsituation.Sid;
        LB3.GetComponent<ListBox>().CreateElement(currentsituation.motions.Count, currentsituation);
        LB3.GetComponent<ListBox>().SetClick(LB3Click);
    }
    void LB3Click(int index)
    {
        currentmotion = currentsituation.motions[index];
        player = new GameEventBackPro(currentsituation, currentPerson, currentmotion);
        player.MotiWeight = player.moti.Weight;
        player.excts.Clear();
        if (currentmotion.Nature == 0 && !player.situ.IsLaw)
        {
            CenterSenate();
            P4MessageShow();
        }
        else
        {
            Exct3(index);
        }
#if AiMotionByPlayer
        currentmotion = currentsituation.motions[index];
        if (currentmotion.Nature ==0)
        {
            CenterSenate();
            P4MessageShow();
        } 
        else
        {
            Exct3(index);
            return;
        }
#endif
    }
    /// <summary>
    /// 加挂变量的界面
    /// </summary>
    /// <param name="index">LB3的index，传递保存</param>
    void Exct3(int index)
    {
        View(5);
        EB7.GetComponent<ListBox>().Clear();
        EB7.GetComponent<ListBox>().DateID = currentmotion.Nature;
        if (!currentsituation.IsLaw && currentmotion.Nature == 1)
        {
            EB7.GetComponent<ListBox>().CreateElement(FamilyPool.Ins.count);
            EB7.GetComponent<ListBox>().SetClick(EB7Click1);
        }
        else if (!currentsituation.IsLaw && currentmotion.Nature == 3)
        {
            EB7.GetComponent<ListBox>().CreateElement(3);
            EB7.GetComponent<ListBox>().SetClick(EB7Click3);
        }
        else if (!currentsituation.IsLaw && currentmotion.Nature == 4)
        {
            ares.Clear();
            GetProvinceArea(1);
            EB7.GetComponent<ListBox>().CreateElement(ares.Count, currentsituation.TriggerArea);
            EB7.GetComponent<ListBox>().SetClick(EB7Click4);
        }
        else if (!currentsituation.IsLaw && currentmotion.Nature == 5)
        {
            EB7.GetComponent<ListBox>().CreateElement(3);
            EB7.GetComponent<ListBox>().SetClick(EB7Click3); //数值无变化时，与小麦一致
        }
        else if (!currentsituation.IsLaw && currentmotion.Nature == 6)
        {
            EB7.GetComponent<ListBox>().CreateElement(2, currentsituation.TriggerArea);
            EB7.GetComponent<ListBox>().SetClick(EB7Click6);
        }
        else if (!currentsituation.IsLaw && currentmotion.Nature == 7)
        {
            ares.Clear();
            GetAlliedNationsArea();
            EB7.GetComponent<ListBox>().CreateElement(ares.Count);
            EB7.GetComponent<ListBox>().SetClick(EB7Click4);
        }
        else if (!currentsituation.IsLaw && currentmotion.Nature == 8)
        {
            ares.Clear();
            GetAlliedNationsArea();
            EB7.GetComponent<ListBox>().CreateElement(ares.Count);
            EB7.GetComponent<ListBox>().SetClick(EB7Click8);
        }
        else if (currentsituation.IsLaw && currentmotion.Nature == 0)
        {
            EB7.GetComponent<ListBox>().CreateElement(LawPool.Ins[currentmotion.ID].Sections.Count, currentsituation.Sid, currentmotion.ID);
            EB7.GetComponent<ListBox>().SetClick(EB7Click0);
        }
        else if (currentsituation.IsLaw && currentmotion.Nature == 1)
        {
            EB7.GetComponent<ListBox>().CreateElement(FamilyPool.Ins.count);
            EB7.GetComponent<ListBox>().SetClick(EB7Click0);
        }
        else if (currentsituation.IsLaw && currentmotion.Nature == 4)
        {
            ares.Clear();
            GetProvinceArea(1);
            EB7.GetComponent<ListBox>().CreateElement(ares.Count, AreaPool.Ins.Capital());
            EB7.GetComponent<ListBox>().SetClick(EB7Click0);
        }
        else if (currentsituation.IsLaw && currentmotion.Nature == 8)
        {
            ares.Clear();
            GetAlliedNationsArea();
            EB7.GetComponent<ListBox>().CreateElement(ares.Count, currentsituation.Sid, currentmotion.ID);
            EB7.GetComponent<ListBox>().SetClick(EB7Click0);
        }
        else if (currentsituation.IsLaw && currentmotion.Nature == 10)
        {
            GetProvinceArea(2);
            EB7.GetComponent<ListBox>().CreateElement(ares.Count);
            EB7.GetComponent<ListBox>().SetClick(EB7Click00);
        }
        else if (currentsituation.IsLaw && (currentmotion.Nature == 18|| currentmotion.Nature == 11|| currentmotion.Nature == 14))
        {
            GetOtherArea(1);
            EB7.GetComponent<ListBox>().CreateElement(ares.Count);
            EB7.GetComponent<ListBox>().SetClick(EB7Click00);
        }
    }
    /// <summary>
    /// 选定家族
    /// </summary>
    /// <param name="index"></param>
    /// 
    private List<Area> ares = new List<Area>();
    private Family CurrentFamily;
    void EB7Click01(int index)
    {
        player.excts.Add(1, CurrentFamily.FStaff.GetPerson(index));
        Exct3(index);
    }
    void EB7Click0(int index)
    {
        //保存加挂，跳过开会，直接站队
        // CenterSenate();开会
        currentmotion.Nature += 10;
        if (currentmotion.Nature == 10)
        {
            player.excts.Add(3, index);
            Exct3(index);
        }
        else if (currentmotion.Nature == 18)
        {
            GetAlliedNationsArea();
            player.excts.Add(4, ares[index]);
            Exct3(index);
        }
        else if (currentmotion.Nature == 11)
        {
            CurrentFamily = FamilyPool.Ins.ReadPool(index + 1) as Family;
            EB7.GetComponent<ListBox>().Clear();
            EB7.GetComponent<ListBox>().DateID = 2;
            EB7.GetComponent<ListBox>().CreateElement(CurrentFamily.FStaff.Count, index);
            EB7.GetComponent<ListBox>().SetClick(EB7Click01);
        }
        else if (currentmotion.Nature == 14)
        {
            GetProvinceArea(1);
            player.excts.Add(1, ares[index].Umpire);
            Exct3(index);
        }
    }
    void EB7Click00(int index)
    {
        currentmotion.Nature -= 10;
        if (currentmotion.Nature == 0)
        {
            GetProvinceArea(2);
            player.excts.Add(2, ares[index]);
        }
        else if(currentmotion.Nature==1|| currentmotion.Nature == 8||currentmotion.Nature==4)
        {
            GetOtherArea(1);
            player.excts.Add(2, ares[index]);
        }
        player.MotiWeight = player.moti.Weight;     //需重做，直接给性质，激进or保守
        var p5 = m_UIDate.UIModel.GetComponent<UI_ShowText>().child[4].GetComponent<UI_ShowText>();
        player.MotiNature = 1; //法案激进
        player.npo = SingleValuePool.Ins["LawNpoOfHandleValue"].IntValue;    //信服度
        player.person.staff.fam.FAsset.CreditX -= LawPool.Ins[player.moti.ID].CreditXCost;
        UITopMessage.ins.UpdateMessage();
        View(4);
        p5.texts[0].text = player.person.Name + ":" + player.moti.Name;
        p5.child[0].GetComponent<Particle2DManager>().pro = player;
        p5.child[0].GetComponent<Particle2DManager>().playerpro = player;
        p5.child[0].GetComponent<Particle2DManager>().StartWork();
    }
    void EB7Click8(int index)
    {
        //征伐
        GetAlliedNationsArea();
        CenterSenate();
        player.excts.Add(2, ares[index]);
        player.MotiWeight = player.moti.Weight + ((player.excts[2] as Area).AreaID * 0.01f);
        P4MessageShow();
    }
    void EB7Click6(int index)
    {
        CenterSenate();
        player.excts.Add(1, index == 0 ? (WarEventBackPro.WorldWarEvent.Find(ar=>ar.Leader== currentsituation.TriggerArea.MinisterA)!=null? currentsituation.TriggerArea.MinisterA:currentsituation.TriggerArea.Umpire) : currentsituation.TriggerArea.MinisterB);
        player.MotiWeight = player.moti.Weight + index * 0.3f;
        P4MessageShow();
    }
    void EB7Click4(int index)
    {
        CenterSenate();
        GetProvinceArea(1);
        player.excts.Add(1, ares[index].Umpire);
        player.MotiWeight = player.moti.Weight + ((player.excts[1] as Person).Nature + 2) * 0.3f;
        P4MessageShow();
    }
    /// <summary>
    /// 发小麦
    /// </summary>
    /// <param name="index">麦子数目系数 </param>
    void EB7Click3(int index)
    {
        CenterSenate();
        player.excts.Add(0,500*(index+1));  //数目栏目公式
        //player.MotiNature = (((int)player.excts[0] - 1000) / 500); //1、0、-1 转化公式（得到 倾向）
        player.MotiWeight = player.moti.Weight + ((int)player.excts[0] / 500) * 0.3f;
        P4MessageShow();
    }
    void EB7Click1(int index)
    {
        CurrentFamily = FamilyPool.Ins.ReadPool(index + 1) as Family;
        EB7.GetComponent<ListBox>().Clear();
        EB7.GetComponent<ListBox>().DateID = 2;
        EB7.GetComponent<ListBox>().CreateElement(CurrentFamily.FStaff.Count, index);
        EB7.GetComponent<ListBox>().SetClick(EB7Click2);
    }
    //void Exct4(int index)
    //{
    //    View(6);
    //    EB8.GetComponent<ListBox>().Clear();
    //    EB8.GetComponent<ListBox>().DateID = 2;
    //    EB8.GetComponent<ListBox>().CreateElement(CurrentFamily.FStaff.Count,index);
    //    EB8.GetComponent<ListBox>().SetClick(EB8Click);
    //}
    void EB7Click2(int index)
    {
        CenterSenate();
        player.excts.Add(1,CurrentFamily.FStaff.GetPerson(index));
        //player.MotiNature =(player.excts[1] as Person).Nature;
        player.MotiWeight = player.moti.Weight + ((player.excts[1] as Person).Nature + 2) * 0.3f;
       P4MessageShow();
    }
    void CenterSenate()
    {
        if(currentmotion==other.moti)
        IsJoined = true;
        Consul.gameObject.SetActive(false);
        UIManager.Ins.StateIsLock = true;
        View(3);
        AB4.GetComponent<ListBox>().Clear();
        AB5.GetComponent<ListBox>().Clear();
        AB6.GetComponent<ListBox>().Clear();
        AB4.GetComponent<ListBox>().DateID = 1;
        AB5.GetComponent<ListBox>().DateID = 2;
        AB6.GetComponent<ListBox>().DateID = 3;
        AB4.GetComponent<ListBox>().CreateElement(ArguePool.Ins.KindArgues(1).Count);
        AB5.GetComponent<ListBox>().CreateElement(ArguePool.Ins.KindArgues(2).Count);
        AB6.GetComponent<ListBox>().CreateElement(ArguePool.Ins.KindArgues(3).Count);
        AB4.GetComponent<ListBox>().SetClick(AB4Click);
        AB5.GetComponent<ListBox>().SetClick(AB5Click);
        AB6.GetComponent<ListBox>().SetClick(AB6Click);
        AB4.gameObject.SetActive(true);
        AB5.gameObject.SetActive(false);
        AB6.gameObject.SetActive(false);
        UIEventListener.Get(m_UIDate.UIModel.GetComponent<UI_ShowText>().child[3].GetComponent<UI_ShowText>().child[0]).onClick = new UIEventListener.VoidDelegate(
            ar =>
            {
                AB4.gameObject.SetActive(true);
                AB5.gameObject.SetActive(false);
                AB6.gameObject.SetActive(false);
            });
        UIEventListener.Get(m_UIDate.UIModel.GetComponent<UI_ShowText>().child[3].GetComponent<UI_ShowText>().child[1]).onClick = new UIEventListener.VoidDelegate(
    ar =>
    {
        AB4.gameObject.SetActive(false);
        AB5.gameObject.SetActive(true);
        AB6.gameObject.SetActive(false);
    });
        UIEventListener.Get(m_UIDate.UIModel.GetComponent<UI_ShowText>().child[3].GetComponent<UI_ShowText>().child[2]).onClick = new UIEventListener.VoidDelegate(
    ar =>
    {
        AB4.gameObject.SetActive(false);
        AB5.gameObject.SetActive(false);
        AB6.gameObject.SetActive(true);
    });
        UIEventListener.Get(m_UIDate.UIModel.GetComponent<UI_ShowText>().child[3].GetComponent<UI_ShowText>().child[3]).onClick = new UIEventListener.VoidDelegate(
    ar =>
    {
        Put();
    });
        UIEventListener.Get(m_UIDate.UIModel.GetComponent<UI_ShowText>().child[3].GetComponent<UI_ShowText>().child[9]).onClick = new UIEventListener.VoidDelegate(
    ar =>
    {
        currentArgue = EmptyArgue;
        Put();
    });
        m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[0].text = currentsituation.Name + "--" + currentsituation.TriggerArea.AreaName;
        //提案的相对激进属性，动态赋值
        if (player.MotiWeight > other.MotiWeight)
        {
            player.MotiNature = 1;
            other.MotiNature = -1;
        }
        else
        {
            player.MotiNature = -1;
            other.MotiNature = 1;
        }
        //other = AIGameEventPro.CreateBackPro(currentPerson.staff.fam, currentsituation, currentmotion);
        //othermotion = other.moti;
        //otherPerson = other.person;
        //if (player.MotiWeight > other.MotiWeight)
        //{
        //    player.MotiNature = 1;
        //    other.MotiNature = -1;
        //}
        //else
        //{
        //    player.MotiNature = -1;
        //    other.MotiNature = 1;
        //}
    }
    void P4MessageShow()
    {
        if (player.MotiWeight == other.MotiWeight)
        {
            LB2Click(player.person.staff.Seniors.IndexOf(player.person));
            return;
        }
        m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[1].text = "第"+count.ToString()+"回";
        var p = m_UIDate.UIModel.GetComponent<UI_ShowText>().child[3].GetComponent<UI_ShowText>();
        for (int j = 0; j < p.child[5].transform.childCount; j++)
        {
            p.child[5].transform.GetChild(j).gameObject.SetActive(false);
        }
        for (int j = 0; j < p.child[6].transform.childCount; j++)
        {
            p.child[6].transform.GetChild(j).gameObject.SetActive(false);
        }
        p.texts[0].text = player.moti.Name;
        p.texts[1].text = player.person.Name+"·"+ player.person.staff.fam.FamilyName;
        p.texts[2].text = player.value.ToString();
        p.texts[3].text = player.npo.ToString();
        p.texts[4].text = player.person.Brain.ToString();
        p.texts[5].text = player.person.Charm.ToString();
        if(player.npo==0)
            p.texts[6].text = " ";
        else
            p.texts[6].text = "+" + player.AddNpo;
        p.texts[7].text = other.moti.Name;
        p.texts[8].text = other.person.Name + "·" + other.person.staff.fam.FamilyName;
        p.texts[9].text = other.value.ToString();
        p.texts[10].text = other.npo.ToString();
        p.texts[11].text = other.person.Brain.ToString();
        p.texts[12].text = other.person.Charm.ToString();
        if (other.npo == 0)
            p.texts[13].text = " ";
        else
            p.texts[13].text = "+" + other.AddNpo;
        p.child[7].GetComponent<UIRect>().ChangeIntime(2, p.texts[6], "");
        p.child[7].GetComponent<UIRect>().ChangeIntime(2, p.texts[13], "");
        p.texts[14].text = player.Nature;
        p.texts[15].text = other.Nature;
        p.texts[16].text =player.value+"-"+player.AddVal.ToString();
        p.texts[17].text = other.value + "-" +other.AddVal.ToString();
        p.child[7].transform.Find("Sprite").GetComponent<UIRect>().SetAnchor(p.child[7], -1, -2, -(width1- width1 * player.value / player.moti.Value), -1);
        p.child[8].transform.Find("Sprite").GetComponent<UIRect>().SetAnchor(p.child[8], (width2 - width2 * other.value / other.moti.Value), -2, -1, -1);
        p.child[7].GetComponent<UIRect>().ChangeIntime(2, p.texts[16], player.value.ToString());
        p.child[7].GetComponent<UIRect>().ChangeIntime(2, p.texts[17], other.value.ToString());
        if (player.excts.ContainsKey(1))
        {
            p.texts[0].text = player.moti.Name + ":\n" + (player.excts[1] as Person).Name + "-" + ((player.excts[1] as Person).staff == null ? "" : ((player.excts[1] as Person).staff.fam.FamilyName));
        }
        else if (player.excts.ContainsKey(0))
        {
            p.texts[0].text = player.moti.Name + ":\n" + player.excts[0].ToString();
        }
        //---------------------//
        if (other.excts.ContainsKey(1))
        {
            p.texts[7].text = other.moti.Name + ":\n" + (other.excts[1] as Person).Name + "-" + ((other.excts[1] as Person).staff == null ? "" : ((other.excts[1] as Person).staff.fam.FamilyName));
        }
        else if (other.excts.ContainsKey(0))
        {
            p.texts[7].text = other.moti.Name+":\n" + other.excts[0].ToString();
        }
        for(int j = 0; j < player.cost; j++)
        {
            p.child[5].transform.GetChild(j).gameObject.SetActive(true);
        }
        for (int j = 0; j < other.cost; j++)
        {
            p.child[6].transform.GetChild(j).gameObject.SetActive(true);
        }
    }
    void AB4Click(int index)
    {
        //TODO:被选择的高光，其余低光 （遍历低光一次）
        var ab4 = AB4.Find("Content");
        ClearBoard();
        if (currentArgue == ArguePool.Ins.GetKind(1)[index])
        {
            ab4.GetChild(index).GetComponent<UI_ShowText>().child[0].SetActive(false);
            currentArgue = EmptyArgue;
            return;
        }
        ab4.GetChild(index).GetComponent<UI_ShowText>().child[0].SetActive(true);
        currentArgue = ArguePool.Ins.GetKind(1)[index];
    }
    void AB5Click(int index)
    {
        var ab5 = AB5.Find("Content");
        ClearBoard();
        if (currentArgue == ArguePool.Ins.GetKind(2)[index])
        {
            ab5.GetChild(index).GetComponent<UI_ShowText>().child[0].SetActive(false);
            currentArgue = EmptyArgue;
            return;
        }
        ab5.GetChild(index).GetComponent<UI_ShowText>().child[0].SetActive(true);
        currentArgue = ArguePool.Ins.GetKind(2)[index];
    }
    void ClearBoard()
    {
        m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[2].text = "";
        var ab5 = AB5.Find("Content");
        for (int i = 0; i < ab5.childCount; i++)
        {
            ab5.GetChild(i).GetComponent<UI_ShowText>().child[0].SetActive(false);
        }
        var ab4 = AB4.Find("Content");
        for (int i = 0; i < ab4.childCount; i++)
        {
            ab4.GetChild(i).GetComponent<UI_ShowText>().child[0].SetActive(false);
        }
        var ab6 = AB6.Find("Content");
        for (int i = 0; i < ab6.childCount; i++)
        {
            ab6.GetChild(i).GetComponent<UI_ShowText>().child[0].SetActive(false);
        }
    }
    void AB6Click(int index)
    {
        var ab6 = AB6.Find("Content");
        ClearBoard();
        if (currentArgue == ArguePool.Ins.GetKind(3)[index])
        {
            ab6.GetChild(index).GetComponent<UI_ShowText>().child[0].SetActive(false);
            currentArgue = EmptyArgue;
            return;
        }
        ab6.GetChild(index).GetComponent<UI_ShowText>().child[0].SetActive(true);
        currentArgue = ArguePool.Ins.GetKind(3)[index];
    }
    /// <summary>
    /// 出牌结算。
    /// </summary>
    void Put()
    {
        otherArgue = AIGameEventPro.CreateArgue(other);
        otherArgue = otherArgue == null ? EmptyArgue : otherArgue;
        if (player.value <= 0||player.cost<=0)
        {
            currentArgue = EmptyArgue;
        }
        if (other.value <= 0)
        {
            otherArgue = EmptyArgue;
        }
        if (!GameEventBackPro.Battle(currentArgue, player, otherArgue, other))
        {
            m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[2].text = SingleValuePool.Ins["NotePanelArgueCostNotEnough"].StringValue;
            return;
        }
        var p = m_UIDate.UIModel.GetComponent<UI_ShowText>().child[3].GetComponent<UI_ShowText>();
        P4MessageShow();
        count++;
        if (count > 3)
        {
            p.child[4].GetComponent<P4Anim>().click = P5Start;
            p.child[4].GetComponent<P4Anim>().Work(currentArgue, otherArgue);
        }
        else
        {
            p.child[4].GetComponent<P4Anim>().click = null;
            p.child[4].GetComponent<P4Anim>().Work(currentArgue, otherArgue);
        }
    }
    /// <summary>
    /// 结算界面
    /// </summary>
    void P5Start()
    {
        View(4);
        GameEventBackPro temp;
        if (player.value > other.value)
        {
            temp = player;
        }
        else
        {
            temp = other;
        }
        var p5 = m_UIDate.UIModel.GetComponent<UI_ShowText>().child[4].GetComponent<UI_ShowText>();
        p5.texts[0].text = temp.person.Name+":"+temp.moti.Name;
        p5.child[0].GetComponent<Particle2DManager>().pro = temp;
        p5.child[0].GetComponent<Particle2DManager>().playerpro = player;
        p5.child[0].GetComponent<Particle2DManager>().StartWork();
    }
    void View(int i)
    {
        for(int j=0;j< m_UIDate.UIModel.GetComponent<UI_ShowText>().child.Count; j++)
        {
            m_UIDate.UIModel.GetComponent<UI_ShowText>().child[j].SetActive(false);
        }
        if(i!=-1)
            m_UIDate.UIModel.GetComponent<UI_ShowText>().child[i].SetActive(true);
    }
    void GetAlliedNationsArea()
    {
        ares.Clear();
        for (int j = 0; j < AreaPool.Ins.Count; j++)
        {
            var a = AreaPool.Ins.ReadPool(j + 1) as Area;
            if (a.Subjection == 2)
            {
                if (a.Umpire != null)
                {
                    ares.Add(a);
                }
            }
        }
    }
    void GetProvinceArea(int i)
    {
        ares.Clear();
        for (int j = 0; j < AreaPool.Ins.Count; j++)
        {
            var a = AreaPool.Ins.ReadPool(j + 1) as Area;
            if (a.Subjection < i)
            {
                if (a.Umpire != null)
                {
                    ares.Add(a);
                }
            }
        }
    }
    void GetOtherArea(int i)
    {
        ares.Clear();
        for (int j = 0; j < AreaPool.Ins.Count; j++)
        {
            var a = AreaPool.Ins.ReadPool(j + 1) as Area;
            if (a.Subjection > i)
            {
                if (a.Umpire != null)
                {
                    ares.Add(a);
                }
            }
        }
    }
    void GetNeighboringArea()
    {
        ares.Clear();
        for (int j = 0; j < AreaPool.Ins.Count; j++)
        {
            var a = AreaPool.Ins.ReadPool(j + 1) as Area;
            if (Mathf.Abs(a.pos_X - currentsituation.TriggerArea.pos_X) + Mathf.Abs(a.pos_Y - currentsituation.TriggerArea.pos_Y) <= SingleValuePool.Ins["MaxNearDistance"].IntValue)
            {
                if (a.Subjection < 2)
                {
                    if (a.Umpire != null)
                    {
                        ares.Add(a);
                    }
                }

            }
        }
    }
}
