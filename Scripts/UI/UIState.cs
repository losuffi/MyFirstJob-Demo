﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class UIStateDate
{
    public int UIStateId;
    public string UIStateName;
    public GameObject UIModel;
}
public abstract class UIState{
    public UIStateDate m_UIDate;
    public void Start(params object[] args)
    {
        if (args.Length > 0)
            StartMoreAct(args);
        OnStart();
        if (m_UIDate.UIModel == null)
            return;
        m_UIDate.UIModel.SetActive(true);
    }
    public void Stop()
    {
        OnStop();
        if (m_UIDate.UIModel == null)
            return;
        m_UIDate.UIModel.SetActive(false);
    }
    public void Init(params object[] args)
    {
        OnInit(args);
    }
    protected abstract void OnStart();
    protected abstract void OnStop();
    protected abstract void OnInit(params object[] argss);
    protected virtual void StartMoreAct(params object[] args)
    {

    }
    //protected abstract void OnWork();
}
