﻿using UnityEngine;
using System.Collections;
using System;

public class UIS_Boardcast : UIState
{
    Transform Lbox;
    protected override void OnInit(params object[] argss)
    {
        Lbox = m_UIDate.UIModel.transform.Find("ListBox");
        Lbox.GetComponent<ListBox>().DateID = GameStart.Ins.playerfamily.FamilyID;
        Lbox.GetComponent<ListBox>().CreateElement(BoardcastDate.Ins.count);
    }

    protected override void OnStart()
    {
        MapSelect.Ins.Islock = true;
        Lbox.GetComponent<ListBox>().CreateElement(BoardcastDate.Ins.count-
            Lbox.Find("Content").childCount);
        Lbox.GetComponent<ListBox>().SetClick(Click);
        if (Lbox.Find("Content").childCount> Lbox.GetComponent<ListBox>().viewcount)
            Lbox.GetComponent<ListBox>().startindex = Lbox.Find("Content").childCount - Lbox.GetComponent<ListBox>().viewcount;
    }

    protected override void OnStop()
    {
        MapSelect.Ins.Islock = false;;
    }
    void Click(int i)
    {
        var temp = BoardcastDate.Ins.Get(i);
        switch (temp.mKind)
        {
            case BoardMessage.MessageKind.Election:
                UIManager.Ins.SetState("UIS_Tot");
                break;
            case BoardMessage.MessageKind.FamRelation:
                UIManager.Ins.SetState("UIS_Con");
                break;
            case BoardMessage.MessageKind.Job:
                UIManager.Ins.SetState("UIS_Man");
                break;
            case BoardMessage.MessageKind.Senate:
                UIManager.Ins.SetState("UIS_Sci");
                break;
            case BoardMessage.MessageKind.Trade:
                var t= UIManager.Ins.SetState("UIS_Eco") as UIS_Eco;
                t.Trade();
                break;
            case BoardMessage.MessageKind.Estate:
                var zt = UIManager.Ins.SetState("UIS_Eco") as UIS_Eco;
                zt.Estate();
                break;
            case BoardMessage.MessageKind.War:
                var z = temp.exct[0] as WarEventBackPro;
                Camera m = NGUITools.FindCameraForLayer(z.leadReal.layer);
                var y = m.transform.position.y;
                m.transform.position = new Vector3(z.leadReal.transform.position.x, y, z.leadReal.transform.position.z);
                UIManager.Ins.SetState("UIS_War", temp.exct[0] as WarEventBackPro, temp.exct[1] as WarEventBackPro,(bool)temp.exct[2]);
                break;
            default:
                break;
        }
    }
}
