﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
public class UIManager : MonoBehaviour {
    private void Awake()
    {
        Ins = this;
        StateIsLock = false;
    }
    public static UIManager Ins;
    public Dictionary<string, UIState> states = new Dictionary<string, UIState>();
    public UIState Currentstate = null;
    public List<UIStateDate> dates = new List<UIStateDate>();
    public bool StateIsLock { get; set; }
    public object SetState(string StateName,params object[] args)
    {
        if (StateIsLock)
            return null;
        if (!states.ContainsKey(StateName))
        {
            var date = dates.Find(ar =>
            {
                if (ar.UIStateName == StateName)
                    return true;
                else return false;
            });
            if (date == null)
            {
                return null;
            }
            var temp = Assembly.GetExecutingAssembly().CreateInstance(date.UIStateName) as UIState;
            temp.m_UIDate = date;
            temp.Init();
            states.Add(StateName, temp);
        }
        if(Currentstate!=null)
            Currentstate.Stop();
        states[StateName].Start(args);
        Currentstate = states[StateName];
        return Currentstate;
    }
}
