﻿using UnityEngine;
using System.Collections;
using System;

public class UIS_Eco : UIState
{
    GameObject LB1, LB2;
    protected override void OnInit(params object[] argss)
    {
        LB1 = m_UIDate.UIModel.GetComponent<UI_ShowText>().child[0];
        LB2 = m_UIDate.UIModel.GetComponent<UI_ShowText>().child[1];
        UIEventListener.Get(m_UIDate.UIModel.GetComponent<UI_ShowText>().child[2]).onClick = new UIEventListener.VoidDelegate(ar => { Trade(); });
        UIEventListener.Get(m_UIDate.UIModel.GetComponent<UI_ShowText>().child[3]).onClick = new UIEventListener.VoidDelegate(ar => { Estate(); });
    }

    protected override void OnStart()
    {
        MapSelect.Ins.Islock = true;
        m_UIDate.UIModel.GetComponent<UI_ShowText>().Work(0, GameStart.Ins.playerfamily.FAsset.Cash.ToString());
        LB1.SetActive(false);
        LB2.SetActive(false);
        m_UIDate.UIModel.GetComponent<UI_ShowText>().Work(1, " ");

    }

    protected override void OnStop()
    {
        MapSelect.Ins.Islock = false;
    }
    public void Trade()
    {
        LB1.GetComponent<ListBox>().Clear();
        LB1.GetComponent<ListBox>().DateID = GameStart.Ins.playerfamily.FamilyID;
        int b = 0;
        for (int j = 0; j < GameStart.Ins.playerfamily.FStaff.Count; j++)
        {
            var per = GameStart.Ins.playerfamily.FStaff.GetPerson(j);
            if (per.taskKind == Person.TaskKind.Trade)
            {
                b++;
            }
        }
        LB1.GetComponent<ListBox>().CreateElement(b);
        LB1.SetActive(true);
        LB2.SetActive(false);
    }
    public void Estate()
    {
        LB2.GetComponent<ListBox>().Clear();
        LB2.GetComponent<ListBox>().DateID = GameStart.Ins.playerfamily.FamilyID;
        LB2.GetComponent<ListBox>().CreateElement(GameStart.Ins.playerfamily.FAsset.estates.Count);
        if (GameStart.Ins.playerfamily.FAsset.Cash < 0)
        {
            LB2.GetComponent<ListBox>().SetClick(SaleEstate);
            m_UIDate.UIModel.GetComponent<UI_ShowText>().Work(1, SingleValuePool.Ins["StrPanelNoteEcoCrisis"].StringValue);
        }
        else
        {
            LB2.GetComponent<ListBox>().SetClick(null);
        }
        LB1.SetActive(false);
        LB2.SetActive(true);
    }
    void SaleEstate(int i)
    {
        GameStart.Ins.playerfamily.FAsset.Cash += GameStart.Ins.playerfamily.FAsset.estates[i].Value;
        GameStart.Ins.playerfamily.FAsset.estates.RemoveAt(i);
        UITopMessage.ins.UpdateMessage();
        (UIManager.Ins.SetState("UIS_Eco") as UIS_Eco).Estate();
    }
}
