﻿using UnityEngine;
using System.Collections;
using System;

public class UIS_War : UIState
{
    UI_ShowText text;
    WarEventBackPro Attack, Defense;
    Area tem1, tem2;
    bool IsOver;
    protected override void OnInit(params object[] argss)
    {
        text = m_UIDate.UIModel.GetComponent<UI_ShowText>();
    }

    protected override void OnStart()
    {
        MapSelect.Ins.Islock = true;
        for (int i = 0; i < text.texts.Count; i++)
        {
            text.texts[i].text = " ";
        }
        WarEventBackPro x, y;
        x = Attack;
        y = Defense;
        int count = 0;
        while (x.Adder != null)
        {
            count++;
            x = x.Adder;
        }
        while (y.Adder != null)
        {
            count--;
            y = y.Adder;
        }
        if (count > 0)
        {
            x = Attack;
            y = Defense;
        }
        else
        {
            x = Defense;
            y = Attack;
        }
        text.texts[1].text = x.Leader.Name;
        text.texts[2].text = x.Leader.Force.ToString();
        text.texts[7].text = y.Leader.Name;
        text.texts[8].text = y.Leader.Force.ToString();
        double a = 0;
        for (int i = 0; i < x.armys.Count; i++)
        {
            a += x.Leader.Force * UnityEngine.Random.Range((float)x.armys[i].ArmyAttiMin, (float)x.armys[i].ArmyAttiMax) * x.armys[i].ArmyValue;
        }
        double b = 0;
        for (int j = 0; j < y.armys.Count; j++)
        {
            b += y.Leader.Force * UnityEngine.Random.Range((float)y.armys[j].ArmyAttiMin, (float)y.armys[j].ArmyAttiMax) * y.armys[j].ArmyValue;
        }
        text.texts[12].text = a.ToString(".#");
        text.texts[16].text = b.ToString(".#");
        if (!IsOver)
        {
            text.texts[15].text = x.IsWinner ? "优势" : "劣势";
            text.texts[19].text = y.IsWinner ? "优势" : "劣势";
        }
        else
        {
            text.texts[15].text = x.IsWinner ? "胜利" : "败北";
            text.texts[19].text = y.IsWinner ? "胜利" : "败北";
        }
        text.texts[20].text = "第" + (TimeDate.TimeCount - Defense.TimeDefense) + "回合";
        while (x.Adder != null)
        {
            string str = x.Adder.Adder != null ? "+" : "";
            text.texts[0].text += x.Adder.Leader.OwnerArea.AreaName + str;
            text.texts[3].text += x.Adder.armys.Count.ToString()+str;
            text.texts[4].text += x.Adder.MaxCount.ToString()+str;
            text.texts[5].text += x.Adder.TotalCount.ToString() + str;
            text.texts[13].text += x.Adder.SubCount.ToString() + str;
            text.texts[14].text += x.Adder.SubArmyCount.ToString() + str;
            x = x.Adder;
        }
        while (y.Adder != null)
        {
            string str = y.Adder.Adder != null ? "+" : "";
            text.texts[6].text += y.Adder.Leader.OwnerArea.AreaName + str;
            text.texts[17].text += y.Adder.SubCount.ToString() + str;
            text.texts[18].text += y.Adder.SubArmyCount.ToString() + str;
            text.texts[9].text+= y.Adder.armys.Count.ToString() + str;
            text.texts[10].text += y.Adder.MaxCount.ToString() + str;
            text.texts[11].text += y.Adder.TotalCount.ToString() + str;
            y = y.Adder;
        }
    }

    protected override void OnStop()
    {
        MapSelect.Ins.Islock = false;
    }
    protected override void StartMoreAct(params object[] args)
    {
        base.StartMoreAct(args);
        Attack = args[0] as WarEventBackPro;
        Defense = args[1] as WarEventBackPro;
        if (tem1 == null && tem2 == null)
        {
            if (Attack.armys.Count > 0)
                tem1 = Attack.armys[0].OwnerArea;
            if (Defense.armys.Count > 0)
                tem2 = Defense.armys[0].OwnerArea;
        }
        IsOver = (bool)args[2];
    }
}
