﻿using UnityEngine;
using System.Collections;
using System;

public class UIS_Capital : UIState
{
    Transform BaseList;
    protected override void OnInit(params object[] argss)
    {
        BaseList = m_UIDate.UIModel.transform.Find("ListBox");
    }

    protected override void OnStart()
    {
        BaseList.GetComponent<ListBox>().CreateElement(LawPool.Ins.Topics.Count - BaseList.Find("Content").childCount);
        MapSelect.Ins.Islock = true;
    }

    protected override void OnStop()
    {
        MapSelect.Ins.Islock = false;
    }
}
