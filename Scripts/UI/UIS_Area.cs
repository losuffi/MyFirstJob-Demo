﻿using UnityEngine;
using System.Collections;
using System;

public class UIS_Area : UIState
{
    [HideInInspector]
    Transform LBperson,LBcrop;
    Area area;
    Specialty spe;
    protected override void OnInit(params object[] argss)
    {
        LBcrop= m_UIDate.UIModel.GetComponent<UI_ShowText>().child[5].transform;
        LBcrop.GetComponent<ListBox>().DateID = GameStart.Ins.playerfamily.FamilyID;
        LBcrop.GetComponent<ListBox>().CreateElement(CropPool.Ins.Count);
        LBcrop.GetComponent<ListBox>().SetClick(ChoseCrop);
        UIEventListener.Get(m_UIDate.UIModel.GetComponent<UI_ShowText>().child[3])
            .onClick = new UIEventListener.VoidDelegate(ar => { Trade(); });
        UIEventListener.Get(m_UIDate.UIModel.GetComponent<UI_ShowText>().child[4])
            .onClick = new UIEventListener.VoidDelegate(ar => { Estate(); });
    }

    protected override void OnStart()
    {
        MapSelect.Ins.Islock = true;
        var temp = MapSelect.Ins.cache.GetComponent<Cube>();
        area = AreaPool.Ins.ReadPool(temp.AreaId) as Area;
        spe = SpecialtyPool.Ins.ReadPool(area.Specialty) as Specialty;
        if (spe != null)
        {
            m_UIDate.UIModel.GetComponent<UI_ShowText>().View(0);
            m_UIDate.UIModel.GetComponent<UI_ShowText>().Work(0, spe.SpecialtyName);
            m_UIDate.UIModel.GetComponent<UI_ShowText>().Work(1, spe.SpecialtyValueReal.ToString());
        }
        else
        {
            m_UIDate.UIModel.GetComponent<UI_ShowText>().Hidden(0);
        }
        if (area.SuitableId > 0 && area.Subjection < 2)
        {
            m_UIDate.UIModel.GetComponent<UI_ShowText>().View(2);
            m_UIDate.UIModel.GetComponent<UI_ShowText>().Work(2, (CropPool.Ins.ReadPool(area.SuitableId) as Crop).CropName);
            m_UIDate.UIModel.GetComponent<UI_ShowText>().Work(3, area.AreaValue.ToString());
            m_UIDate.UIModel.GetComponent<UI_ShowText>().Work(4, area.AreaCropPercent.ToString());
        }
        else
        {
            m_UIDate.UIModel.GetComponent<UI_ShowText>().Hidden(2);
        }
        m_UIDate.UIModel.GetComponent<UI_ShowText>().Work(5, "");
        m_UIDate.UIModel.GetComponent<UI_ShowText>().Hidden(1);
        m_UIDate.UIModel.GetComponent<UI_ShowText>().Hidden(5);
    }

    protected override void OnStop()
    {
        MapSelect.Ins.Islock = false;
    }
    public void Trade()
    {
        m_UIDate.UIModel.GetComponent<UI_ShowText>().Hidden(1);
        m_UIDate.UIModel.GetComponent<UI_ShowText>().Hidden(5);
        m_UIDate.UIModel.GetComponent<UI_ShowText>().View(1);
        LBperson = m_UIDate.UIModel.GetComponent<UI_ShowText>().child[1].transform;
        LBperson.GetComponent<ListBox>().Clear();
        LBperson.GetComponent<ListBox>().DateID = GameStart.Ins.playerfamily.FamilyID;
        LBperson.GetComponent<ListBox>().CreateElement(GameStart.Ins.playerfamily.FStaff.Civics.Count);
        LBperson.GetComponent<ListBox>().SetClick(ChosePerson);
    }
    /// <summary>
    /// 向Backgroun 计算类中进行注册 贸易事件。并把
    /// 所选择的人状态转换好
    /// </summary>
    /// <param name="i">选定家族中，人的ID </param>
    void ChosePerson(int i)
    {
        if (!GameStart.Ins.playerfamily.FStaff.Civics[i].IsLiving
            || !GameStart.Ins.playerfamily.FStaff.Civics[i].Idel)
        {
            return;
        }
        Trade temp = new global::Trade(spe, GameStart.Ins.playerfamily.FStaff.Civics[i], area);
        BackGroundManager.Ins.RegBGEvent(temp);
        UIManager.Ins.SetState("UIS_Empty");
    }
    void Estate()
    {
        m_UIDate.UIModel.GetComponent<UI_ShowText>().Hidden(1);
        m_UIDate.UIModel.GetComponent<UI_ShowText>().Hidden(5);
        m_UIDate.UIModel.GetComponent<UI_ShowText>().View(5);
    }
    void ChoseCrop(int i)
    {
        if (GameStart.Ins.playerfamily.FAsset.Cash - area.AreaValue >= 0)
        {
            GameStart.Ins.playerfamily.FAsset.Cash -= area.AreaValue;
            UITopMessage.ins.UpdateMessage();
            GameStart.Ins.playerfamily.FAsset.estates.Add(new global::Estate(GameStart.Ins.playerfamily, CropPool.Ins.ReadPool(i) as Crop, area));
            UIManager.Ins.SetState("UIS_Empty");
        }
        else
        {
            m_UIDate.UIModel.GetComponent<UI_ShowText>().Work(5, "现金不够，无法购买");
        }

    }
}
