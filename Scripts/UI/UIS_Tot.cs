﻿using UnityEngine;
using System.Collections;
using System;

public class UIS_Tot : UIState
{
    Person senior, umpire, Consul;
    Election temp;
    int MinTimeCount = SingleValuePool.Ins["ElectionStartMonth"].IntValue;
    int MaxTimeCount = SingleValuePool.Ins["ElectionEndMonth"].IntValue;
    bool t; //是否是竞选时间
    protected override void OnInit(params object[] argss)
    {
        TimeControl.Ins.ep += ClearDiplomacyMessage;
        TimeControl.Ins.ep += ClearTot;
    }

    protected override void OnStart()
    {
        MapSelect.Ins.Islock = true;
        var T = TimeDate.TimeCount * TimeControl.Ins.TimeSpeed; //时间系数 与speed 相关
        t = ((1 + (T % 360) / 30) >= MinTimeCount && ((1 + (T % 360) / 30) < MaxTimeCount));
        var Tbox = m_UIDate.UIModel.GetComponent<UI_ShowText>().child[0];
        var fam = GameStart.Ins.playerfamily;
        m_UIDate.UIModel.GetComponent<UI_ShowText>().child[1].SetActive(t);
        m_UIDate.UIModel.GetComponent<UI_ShowText>().child[2].SetActive(!t);
        if (t)
        {
            if (temp == null)
            {
                temp = new Election(MaxTimeCount - (2 + (T % 360) / 30));
                BackGroundManager.Ins.RegBGEvent(temp);
            }
            if (senior == null && umpire == null)
            {
                Tbox.SetActive(t);
                Tbox.GetComponent<ListBox>().Clear();
                Tbox.GetComponent<ListBox>().DateID = fam.FamilyID;
                Tbox.GetComponent<ListBox>().CreateElement(fam.FStaff.IdelPerson.Count, 1);
                Tbox.GetComponent<ListBox>().SetClick(ChosePerson);
                m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[0].text = "选择一名成员，参与竞选";
            }
            else
            {
                m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[0].text = " ";
            }
            if (senior != null)
            {
                m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[0].text += senior.Name + "  ";
            }
            if (umpire != null)
            {
                m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[0].text += umpire.Name + "  ";
            }
            if (Consul != null)
            {
                m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[0].text +=  Consul.Name + "  ";
            }
            if (senior != null && umpire != null)
            {
                if(Consul!=null||GameStart.Ins.playerfamily.FStaff.UmpiredSenior.Count<1)
                    Tbox.SetActive(false);
            }
        }
        else
        {
            m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[0].text = "";
            Tbox.SetActive(false);
        }
        UIEventListener.Get(m_UIDate.UIModel.GetComponent<UI_ShowText>().child[3]).onClick = new UIEventListener.VoidDelegate(ar =>
        {
            BuyCreditY();
        });
        UIEventListener.Get(m_UIDate.UIModel.GetComponent<UI_ShowText>().child[4]).onClick = new UIEventListener.VoidDelegate(ar =>
        {
            PickCreditY();
        });
    }

    protected override void OnStop()
    {
        MapSelect.Ins.Islock = false;
    }
    void ChosePerson(int index)
    {
        var p  = GameStart.Ins.playerfamily.FStaff.IdelPerson[index];
        if (p.Job!= Person.JobKind.None)
        {
            if(p.Job==  Person.JobKind.ConsuledSenior||p.Job== Person.JobKind.UmpiredSenior)
            {
                if (Consul == null)
                {
                    Consul = p;
                    temp.consual.per = Consul;
                    temp.consual.t = Election.ElectionKind.Consual;
                    temp.consual.CostValue= SingleValuePool.Ins["ElectionConsualCreditYCost"].IntValue;
                }
                else if(Consul!=p)
                {
                    if (p.Job != Person.JobKind.ConsuledSenior)
                    {
                        if (umpire == null)
                        {
                            umpire = p;
                            temp.umpire.per = umpire;
                            temp.umpire.t = Election.ElectionKind.Umpire;
                            temp.umpire.CostValue = SingleValuePool.Ins["ElectionUmpireCreditYCost"].IntValue;
                        }
                    }
                }
            }
            else
            {
                if (p.Job != Person.JobKind.ConsuledSenior)
                {
                    if (umpire == null)
                    {
                        umpire = p;
                        temp.umpire.per = umpire;
                        temp.umpire.t = Election.ElectionKind.Umpire;
                        temp.umpire.CostValue = SingleValuePool.Ins["ElectionUmpireCreditYCost"].IntValue;
                    }
                }
            }
        }
        else
        {
            if (senior == null)
            {
                senior = p;
                temp.senior.per = senior;
                temp.senior.t = Election.ElectionKind.Senior;
                temp.senior.CostValue= SingleValuePool.Ins["ElectionSeniorCreditYCost"].IntValue;
            }
        }
        UIManager.Ins.SetState("UIS_Tot");
    }
    /// <summary>
    /// 买声望 200块一次
    /// </summary>
    void BuyCreditY()
    {
        if (!t)
            return;
        if (GameStart.Ins.playerfamily.FAsset.Cash - 200 < 0)
        {
            BoardcastDate.Ins.RecOther("Diplomacy", "player_CreditYBuy", SingleValuePool.Ins["MessageSmallBuyCreditYNoMoney"].StringValue + "   \n");
            m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[1].text += BoardcastDate.Ins.GetOtherLists("Diplomacy").Find(ar =>
            {
                if (ar.Title == "player_CreditYBuy"&& ar.Time == BoardcastDate.Ins.GetOtherLists("Diplomacy").Count - 1)
                {
                    return true;
                }
                return false;
            }).Content;
            return;
        }
        GameStart.Ins.playerfamily.FAsset.Cash -= 200;
        CreditYChange temp = new CreditYChange(0, 20, GameStart.Ins.playerfamily);
        BoardcastDate.Ins.RecOther("Diplomacy", "player_CreditYBuy", SingleValuePool.Ins["MessageSmallBuyCreditY200"].StringValue + "   \n");
        BackGroundManager.Ins.RegBGEvent(temp);
        m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[1].text += BoardcastDate.Ins.GetOtherLists("Diplomacy").Find(ar =>
        {
            if (ar.Title == "player_CreditYBuy" && ar.Time == BoardcastDate.Ins.GetOtherLists("Diplomacy").Count - 1)
            {
                return true;
            }
            return false;
        }).Content;
        UITopMessage.ins.UpdateMessage();
    }
    /// <summary>
    /// 免费操作一回合只能作用一次。所以需要记录 时间数
    /// </summary>
    private int PickYcount = -1;
    /// <summary>
    /// 消息清楚，在UIS_Con中代劳，因为都是使用的Diplomacy 名称的容器。
    /// </summary>
    void PickCreditY()
    {
        if (PickYcount == TimeDate.TimeCount)
            return;
        CreditYChange temp = new CreditYChange(0, 2, GameStart.Ins.playerfamily);
        BoardcastDate.Ins.RecOther("Diplomacy", "player_CreditYPick", SingleValuePool.Ins["MessageSmallPickCreditY"].StringValue +"   \n");
        BackGroundManager.Ins.RegBGEvent(temp);
        m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[1].text += BoardcastDate.Ins.GetOtherLists("Diplomacy").Find(ar =>
        {
            if (ar.Title == "player_CreditYPick" && ar.Time == BoardcastDate.Ins.GetOtherLists("Diplomacy").Count - 1)
            {
                return true;
            }
            return false;
        }).Content;
        UITopMessage.ins.UpdateMessage();
        PickYcount = TimeDate.TimeCount;
    }
    void ClearDiplomacyMessage()
    {
        BoardcastDate.Ins.GetOtherLists("Diplomacy").Clear();
        m_UIDate.UIModel.GetComponent<UI_ShowText>().texts[1].text = " ";
    }
    void ClearTot()
    {
        var T = TimeDate.TimeCount * TimeControl.Ins.TimeSpeed;
        if (((1 + (T % 360) / 30) >= MinTimeCount && ((1 + (T % 360) / 30) < MaxTimeCount)))
        {
            return;
        }
        else
        {
            temp = null;
            senior = null;
            umpire = null;
            Consul = null;
        }
    }
}
