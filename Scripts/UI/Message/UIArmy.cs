﻿using UnityEngine;
using System.Collections;

public class UIArmy : MonoBehaviour {

    public static UIArmy Ins;
    UI_ShowText text;
    private Camera nc;
    private void Awake()
    {
        Ins = this;
        text = GetComponent<UI_ShowText>();
        nc = NGUITools.FindCameraForLayer(gameObject.layer);
    }
    private void Start()
    {
        gameObject.SetActive(false);
    }
    public void View(Army date)
    {
        transform.position = nc.ScreenToWorldPoint(Input.mousePosition);
        gameObject.SetActive(true);
        text.texts[0].text = date.ArmyName;
        text.texts[1].text = date.ArmyMember.ToString();
        text.texts[2].text = date.CurrentArea.AreaName;
        text.texts[3].text = TimeControl.Ins.TimeConvert(date.WorkTime);
        text.texts[4].text = date.ArmyAge.ToString() + "回合";
        string vs;
        switch (date.State)
        {
            case Army.states.Battle:
                vs = "战斗中";
                break;
            case Army.states.Run:
                vs = "行军中";
                break;
            default:
                vs = "驻扎休息";
                break;
        }
        text.texts[5].text = vs;
    }
}
