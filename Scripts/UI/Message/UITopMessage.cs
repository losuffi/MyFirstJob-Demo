﻿using UnityEngine;
using System.Collections;
using Tools;
public class UITopMessage : MonoBehaviour {
    private bool flag;
    public static UITopMessage ins;
    UI_ShowText uis;
    private void Awake()
    {
        flag = false;
        ins = this;
        uis = GetComponent<UI_ShowText>();
    }
    private void Update()
    {
        if (flag|| GameStart.Ins.playerfamily.FStaff == null)
            return;
        UpdateMessage();
        flag = true;
        TimeControl.Ins.ep += UpdateMessage;
    }
    public void UpdateMessage()
    {
        uis.texts[0].text = GameStart.Ins.playerfamily.FamilyName;
        uis.texts[1].text = GameStart.Ins.playerfamily.FAsset.Cash.ToString();
        uis.texts[2].text = GameStart.Ins.playerfamily.FStaff.IdelCount.ToString() + "/" +
            GameStart.Ins.playerfamily.FStaff.Count.ToString();
        uis.texts[3].text = GameStart.Ins.playerfamily.FAsset.CreditX.ToString();
        uis.texts[4].text = GameStart.Ins.playerfamily.FAsset.CreditY.ToString();
    }
}
