﻿using UnityEngine;
using System.Collections;

public class UIAreaMessage : MonoBehaviour {
    public static UIAreaMessage Ins;
    UI_ShowText text;
    private void Awake()
    {
        Ins = this;
        text = GetComponent<UI_ShowText>();
        gameObject.SetActive(false);
    }
    public void View(Area date)
    {
        if (date == null)
            return;
        gameObject.SetActive(true);
        text.texts[0].text = date.AreaName;
        text.texts[1].text = date.Tax.ToString();
        text.texts[2].text = date.Distance.ToString();
        var specialty = SpecialtyPool.Ins.ReadPool(date.Specialty) as Specialty;
        var crop = CropPool.Ins.ReadPool(date.SuitableId) as Crop;
        if (specialty != null)
        {
            text.texts[3].text = specialty.SpecialtyName;
        }
        else
        {
            text.texts[3].text = "None";
        }
        if (crop != null)
        {
            text.texts[4].text = crop.CropName;
        }
        else
        {
            text.texts[4].text = "None";
        }
        text.texts[9].text = date.CD.ToString();
        switch (date.Subjection)
        {
            case 1:
                text.texts[6].text = " 首都";
                text.texts[5].text = "罗马";
                text.texts[8].text = "执政官:";
                text.texts[10].text = "国库信息：\n财富：" + date.country.Cash.ToString() + "\n粮食：" + date.country.Rice + "\n无业游民：" + date.country.PersonCount;
                text.texts[12].text = date.Laws[0].Topic + ":";
                text.texts[13].text = date.Laws[0].Name + ":" + date.Laws[0].Value.ToString();
                text.texts[14].text = date.Laws[1].Name + ":" + date.Laws[1].Value.ToString();
                text.texts[15].text = date.Laws[2].Name + ":" + date.Laws[2].Value.ToString();
                text.texts[16].text = date.Laws[3].Topic + ":";
                text.texts[17].text = date.Laws[3].Name + ":" + date.Laws[3].Value.ToString();
                break;
            case 2:
                text.texts[6].text = " 同盟国";
                text.texts[5].text = date.AreaName;
                text.texts[8].text = "国王:";
                text.texts[10].text = "国库信息：\n财富：" + date.country.Cash.ToString();
                break;
            case 3:
                text.texts[6].text = " 中立国";
                text.texts[5].text = date.AreaName;
                text.texts[8].text = "国王:";
                text.texts[10].text = "国库信息：\n财富：" + date.country.Cash.ToString();
                break;
            case 4:
                text.texts[6].text = " 冷淡国";
                text.texts[5].text = date.AreaName;
                text.texts[8].text = "国王:";
                text.texts[10].text = "国库信息：\n财富：" + date.country.Cash.ToString();
                break;
            default:
                text.texts[6].text = " 行省";
                text.texts[8].text = "裁判官:";
                text.texts[5].text = "罗马";
                text.texts[10].text = "";
                text.texts[12].text = date.Laws[0].Topic + ":";
                text.texts[13].text = date.Laws[0].Name + ":" + date.Laws[0].Value.ToString();
                text.texts[14].text = date.Laws[1].Name + ":" + date.Laws[1].Value.ToString();
                text.texts[15].text = date.Laws[2].Name + ":" + date.Laws[2].Value.ToString();
                text.texts[16].text = date.Laws[3].Topic + ":";
                text.texts[17].text = date.Laws[3].Name + ":" + date.Laws[3].Value.ToString();
                break;
        }
        if (date.Umpire != null)
            text.texts[7].text = date.Umpire.Name + (date.Umpire.staff != null ? "-" + date.Umpire.staff.fam.FamilyName : "");
        else
            text.texts[7].text = "None";
        if(date.armys.Count>0)
            text.texts[11].text = (date.armys[0].ArmyName).Replace('0',' ') + ",数量：" + date.armys.Count;
        else
            text.texts[11].text = "无";
    }
}
