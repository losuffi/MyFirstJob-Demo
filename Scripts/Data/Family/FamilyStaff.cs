﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class FamilyStaff{
    private List<Person> staffs = new List<Person>();
    public int IdelCount { get; set; }
    public int Count { get; private set; }
    public Family fam { get; set; }
    public int GetPersonIndex(Person t)
    {
        return staffs.IndexOf(t);
    }
    public FamilyStaff(Family fami)
    {
        fam = fami;
    }
    public void Init(string a,string b)
    {
        string[] strs = a.Split('|');
        int count = Random.Range(int.Parse(strs[0]), int.Parse(strs[1]) + 1);
        for (; count>0; count--)
        {
            staffs.Add(new Person(true, this, AreaPool.Ins.Capital()));
        }
        Count = IdelCount = staffs.Count;
        InitNature(b);
    }
    public void UpdateCount()
    {
        IdelCount = 0;
        for (int j = 0; j < Count; j++)
        {
            if (GetPerson(j).Idel)
                IdelCount++;
        }
    }
    public Person GetPerson(int index)
    {
        return staffs[index];
    }
    public int SeniorCount
    {
        get
        {
           return Seniors.Count;
        }
    }
    public List<Person> Seniors
    {
        get
        {
            List<Person> temp = new List<Person>();
            foreach(var c in staffs)
            {
                if (c.Job == Person.JobKind.ConsuledSenior||c.Job== Person.JobKind.Senior|| c.Job== Person.JobKind.UmpiredSenior)
                    temp.Add(c);
            }
            return temp;
        }
    }
    public List<Person> IdelPerson
    {
        get
        {
            List<Person> temp = new List<Person>();
            foreach (var c in staffs)
            {
                if (c.Idel)
                    temp.Add(c);
            }
            return temp;
        }

    }
    public List<Person> UmpiredSenior
    {
        get
        {
            List<Person> temp = new List<Person>();
            foreach (var c in staffs)
            {
                if (c.Job == Person.JobKind.UmpiredSenior||c.Job== Person.JobKind.ConsuledSenior)
                    temp.Add(c);
            }
            return temp;
        }
    }
    public List<Person> Civics
    {
        get
        {
            List<Person> temp = new List<Person>();
            foreach (var c in staffs)
            {
                if (c.Job == Person.JobKind.None)
                    temp.Add(c);
            }
            return temp;
        }
    }
    void InitNature(string str)
    {
        string[] strs = str.Split('|');
        int a = int.Parse(strs[0]);
        int b = int.Parse(strs[1]);
        int c = int.Parse(strs[2]);
        int d = int.Parse(strs[3]);
        a = (Count * a) / (a + b + c + d);
        b = (Count * b) / (a + b + c + d);
        c = (Count * c) / (a + b + c + d);
        d = Count - a - b - c;
        for(int i = 0; i < Count; i++)
        {
            if (a > 0)
            {
                staffs[i].Nature = -2;
                a--;
            }
            else if (b > 0)
            {
                staffs[i].Nature = -1;
                b--;
            }
            else if (c > 0)
            {
                staffs[i].Nature = 1;
                c--;
            }
            else
            {
                staffs[d].Nature = 2;
                d--;
            }
        }
    }
}
