﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Tools;
/// <summary>
/// Family类的组合部分，构成家族的财产、资源部分数据。
/// 提供构造函数，对字段进行初始化
/// </summary>
public class FamilyAsset{
    public int Cash { get; set; }
    public int Slave { get; set; }
    public int CreditX { get; set; }
    public int CreditY { get; set; }
    public Dictionary<int, int> Relationship = new Dictionary<int, int>();
    public List<Estate> estates = new List<Estate>();
    public FamilyAsset(MySqlDataReader t)
    {
        MySqlDataReader mdr = t;
        Cash = (int)mdr[2];
        Slave = (int)mdr[3];
        CreditX = (int)mdr[4];
        CreditY = (int)mdr[5];
    }
    public FamilyAsset()
    {

    }
}
