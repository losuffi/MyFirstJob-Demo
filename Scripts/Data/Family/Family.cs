﻿using UnityEngine;
using System.Collections;
using Tools;
/// <summary>
/// 家族的数据保存类，所有关于家族的数据均在该类中进行保存
/// 提供4个组合分支，讲数据以树形结构进行分类处理
/// </summary>
public class Family {
    public int FamilyID { get; set; }
    public string FamilyName { get; set; }
    public int FamilyScore { get; set; }
    public FamilyAsset FAsset;
    public FamilyDiplomacy FDiplomacy;
    public FamilyStaff FStaff;
    public FamilyTechnology FTechnology;
    public int seniorSetCount { get; set; }
    public Family()
    {
        seniorSetCount = 1;
    }
    /// <summary>
    /// 从数据池中获取家族数据
    /// </summary>
    /// <param name="ID">家族ID号</param>
}
