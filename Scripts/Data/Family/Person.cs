﻿using UnityEngine;
using System.Collections;
using LitJson;
public class Person {
    public int Force { get; set; }
    public int Brain { get; set; }
    public int Charm { get; set; }
    public string Name { get; set; }
    public int Age { get; set; }
    public bool Idel { get; set; }
    public bool Sex { get; set; }
    public bool IsLiving { get; set; }
    public Area OwnerArea { get; set; }
   // public bool IsSenior { get; set; }
    public enum JobKind
    {
        Senior,
        Umpire,
        UmpiredSenior,
        Consul,
        ConsuledSenior,
        ConsulUmpire,
        None,
    }
    public enum TaskKind
    {
        None,
        Trade,
        Official,
    }
    public int Nature { get; set; }
    public FamilyStaff staff { get; set; }
    public JobKind Job { get; set; }
    public TaskKind taskKind { get; set; }
    public Area StayArea { get; set; }
    public Area TargetArea { get; set; }
    /// <summary>
    /// 以下为功能变量
    /// </summary>
    private int BirthDay;
    private int TaskDay;
    public Person(bool isinit,FamilyStaff sta,Area stay)
    {
        SetName();
        SetValue();
        Idel = true;
        IsLiving = true;
        //IsSenior = false;
        Job = JobKind.None;
        if (Random.Range(1, 11) > 5)
            Sex = true;
        else
            Sex = false;
        if (isinit)
            Age = 18 + Random.Range(0, 30);
        else
            Age = 1;
        BirthDay = TimeDate.TimeCount % 360;
        StayArea = stay;
        OwnerArea = stay;
        RndNature();
        staff = sta;
    }
    void RndNature()
    {
        Nature = Random.Range(-2, 3);
        if (Nature == 0)
            RndNature();
    }
    void SetName()
    {
        int rnd = Random.Range(3, 6);
        int rndC = Random.Range(1, 26);
        char first = (char)((int)'A' + rndC);
        Name += first;
        for (int i = 0; i < rnd; i++)
        {
            int rC = Random.Range(1, 26);
            char temp = (char)((int)'a' + rC);
            Name += temp;
        }
    }
    void SetValue()
    {
        int a = Random.Range(-5, 14);
        int b = Random.Range(1 - a, 20 - a);
        if (b >= -5 && b < 14)
        {
            Force = (a + b) / 2;
            Brain = (15 - a) / 2;
            Charm = (15 - b) / 2;
            if (Force + Brain + Charm < 15)
            {
                Force += 15 - (Force + Brain + Charm);
            }
        }
        else
        {
            SetValue();
        }
    }
    public int TaskCost { get; private set; }
    public int TaskRegCount { get;private set; }
    public void UpdateAge()
    {
        if (TimeDate.TimeCount % 360 == BirthDay)
            Age++;
    }
    /// <summary>
    /// 外部调用人去执行Task的接口方法。
    /// </summary>
    /// <param name="TimeCost">时间损耗</param>
    /// <param name="TaskID">执行的Task信息ID</param>
    public void TaskGO(int TimeCost,TaskKind tk,Area target)
    {
        TargetArea = target;
        taskKind = tk;
        TaskDay = TimeDate.TimeCount + TimeCost;
        TaskRegCount = TimeDate.TimeCount;
        TaskCost = TimeCost;
        Idel = false;
        staff.UpdateCount();
        //TODO:得到执行的Task信息，如跑商、做省长等。
    }
    /// <summary>
    /// 外部调用人去执行Task的接口方法。
    /// </summary>
    /// <param name="TimeCost">时间损耗</param>
    /// <param name="TaskID">执行的Task信息ID</param>
    public void TaskGO(TaskKind tk, Area target)
    {
        TargetArea = target;
        taskKind = tk;
        Idel = false;
        TaskDay = TimeDate.TimeCount + 1 << 10;
        staff.UpdateCount();
        //TODO:得到执行的Task信息，如跑商、做省长等。
    }
    public void UpdateTask()
    {
        if (Idel)
            return;
        if (TimeDate.TimeCount > TaskDay)
        {
            taskKind = TaskKind.None;
            Idel = true;
        }
        staff.UpdateCount();
    }
    public void Relax()
    {
        if (Idel)
            return;
        else
        {
            taskKind = TaskKind.None;
            Idel = true;
        }
        staff.UpdateCount();
    }
    public void Dead()
    {
        IsLiving = false;
    }
}
