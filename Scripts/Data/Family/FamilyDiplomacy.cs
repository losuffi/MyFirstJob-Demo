﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class FamilyDiplomacy {
    private Family Fm;
    private Dictionary<Family,Relation> relations = new Dictionary<Family, Relation>();
    public FamilyDiplomacy(Family fm)
    {
        Fm = fm;
    }
    public Relation GetRelation(Family sfm)
    {
        if (!relations.ContainsKey(sfm))
        {
            relations.Add(sfm, RelationPool.Ins.ReadValue(Fm.FamilyID, sfm.FamilyID));
        }
        return relations[sfm];
    }
}
