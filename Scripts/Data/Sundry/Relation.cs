﻿using UnityEngine;
using System.Collections;

public class Relation{
    public int Id { get; set; }
    public int FirstFamilyId { get; set; }
    public int SecondFamilyId { get; set; }
    public double Coefficient { get; set; }
    private float _value;
    public Relation()
    {
        _value = 0;
    }
    public float Value
    {
        get
        {
            return _value;
        }
    }
    public void ChageValue(float  x)
    {
        _value = x;
    }
}
