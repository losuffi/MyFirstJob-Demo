﻿using UnityEngine;
using System.Collections;

public class SingleValue {
    public int ID { get; private set; }
    public string Name { get; private set; }
    public string StringValue { get; private set; }
    public int IntValue { get; private set; }
    public SingleValue(params object[] args)
    {
        ID = (int)args[0];
        Name = args[1].ToString();
        StringValue = args[2].ToString();
        IntValue = (int)args[3];
    }
}
