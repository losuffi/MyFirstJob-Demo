﻿using UnityEngine;
using System.Collections;

public class Estate {
    public Family fam { get; set; }
    public Crop cro { get; set; }
    public int CostTime { get; set; }
    public Area area { get; set; }
    public bool IsWork { get; set; }
    public int Value { get; set; }
    public int StartTIme { get; set; }
    public Estate(Family f,Crop c,Area a)
    {
        fam = f;
        cro = c;
        CostTime = cro.Cd;
        area = a;
        IsWork = false;
        StartTIme = TimeDate.TimeCount;
        Value = area.AreaValue;
        BackGroundManager.Ins.RegBGEvent(new Assart(this));
    }
}
