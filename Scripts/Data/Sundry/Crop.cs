﻿using UnityEngine;
using System.Collections;

public class Crop{
    public int CropId { get; set; }
    public string CropName { get; set; }
    public int Cd { get; set; }
    public int Consume { get; set; }
    public int Earning { get; set; }
    public int Month { get; set; }
}
