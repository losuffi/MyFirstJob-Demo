﻿using UnityEngine;
using System.Collections;

public class Army{
    public int ArmyID { get; set; }
    public string ArmyName { get; set; }
    public double ArmyPassPercent { get; set; }
    public double ArmyAttiMax { get; set; }
    public double ArmyAttiMin { get; set; }
    //public Country Owneer{get;set;}  //国家，暂无该数据模型
    public int ArmyCost { get; set; }
    public int ArmyCost2 { get; set; }
    public int ArmyCostMonth { get; set; }
    public Person ArmyLeader { get; set; }
    public int ArmyMember { get; set; }
    public int ArmyValue { get; set; }
    public int ArmyAge { get; set; }
    public int ArmyWeight { get; set; }
    public Army()
    {
        ArmyAge = 0;
        IsOnArmy = false;
        TimeControl.Ins.ep += AgeChange;
    }
    void AgeChange()
    {
        ArmyAge++;
    }
    public Area OwnerArea { get; set; }
    public Area CurrentArea { get; set; }
    public int WorkTime { get; set; }
    public bool IsOnArmy { get; set; }
    public states State { get; set; }
    public enum states
    {
        Run,
        Battle,
        Stay,
    }
}
