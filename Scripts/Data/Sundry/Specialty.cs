﻿using UnityEngine;
using System.Collections;

public class Specialty{
    public int SpecialtyId { get; set; }
    public string SpecialtyName { get; set; }
    public int SpecialtyArea { get; set; }
    public int SpecialtyValueMin { get; set; }
    public int SpecialtyValueMax { get; set; }
    public int SpecialtyRnd { get; set; }
    public int SpecialtyValueReal { get; set; }
}
