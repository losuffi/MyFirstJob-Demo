﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Country
{
    public string CountryName { get; set; }
    public int Cash { get; set; }
    public int Rice { get; set; }
    public int PersonCount { get; set; }
    public int CountryValue { get; set; }
}
public class Area{
    public int AreaID { get; set; }
    public string AreaName { get; set; }
    public int Tax { get; set; }
    public int TaxTotal { get; set; }
    public int Distance { get; set; }
    public int SuitableId { get; set; }
    public int Subjection { get; set; }
    public int Specialty { get; set; }
    public Person Umpire { get; set; }
    public int AreaValue { get; set; }
    public double AreaTaxPercent { get; set; }
    public double AreaCropPercent { get; set; }
    public int pos_X { get; set; }
    public int pos_Y { get; set; }
    public List<Army> armys;
    public Country country { get; set; }
    public int CD { get; set; }
    public bool IsBattleArea { get; set; }
    public bool IsTheFalledArea { get; set; }
    public Person MinisterA, MinisterB;
    public List<ALaw> Laws { get; set; }
    public static void Falled(Area target)
    {
        target.IsTheFalledArea = true;
        EstateRemove(target);
    }
    public static void EstateRemove(Area target)
    {
        for (int i = 0; i < GameStart.Ins.playerfamily.FAsset.estates.Count; i++)
        {
            if (GameStart.Ins.playerfamily.FAsset.estates[i].area.Equals(target))
                GameStart.Ins.playerfamily.FAsset.estates.RemoveAt(i);
        }
    }
}
