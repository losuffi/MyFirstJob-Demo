﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ALaw{
    public int ID { get; set; }
    public string Name { get; set; }
    public int Value { get; set; }
    public List<int> Sections { get; set; }
    public int CreditXCost { get; set; }
    public string Topic { get; set; }
    public int Command { get; set; }
}
