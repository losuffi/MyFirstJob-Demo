﻿using UnityEngine;
using System.Collections;

public class Argue{
    public int ID { get; set; }
    public int Kind { get; set; }
    public int ValueA { get; set; }
    public int ValueB { get; set; }
    public int Cost { get; set; }
    public string Dec { get; set; }
}
