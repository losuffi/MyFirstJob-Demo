﻿using UnityEngine;
using System.Collections;
using System;

public class Effect10 : IEffect
{
    public override void ChangeValue(params object[] args)
    {
    }

    public override void OnChange()
    {
        if (situ.effects.Find(ar => ar.ID == 1) != null)
            situ.effects.Find(ar => ar.ID == 1).end += Complete;
    }

    public override void Work(params object[] args)
    {
        situ = args[0] as Situation;
        IBG = new DisasterBGWork(situ.TriggerArea);
        if(situ.effects.Find(ar => ar.ID == 1)!=null)
            situ.effects.Find(ar => ar.ID == 1).end += Complete;
    }
    void Complete()
    {
        (IBG as DisasterBGWork).IBG_TimeOfDuration = 0;
        End(1);
    }
}
