﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Effect6 : IEffect
{
    public WarEventBackPro AIWarUnit;
    public override void ChangeValue(params object[] args)
    {
    }

    public override void OnChange()
    {
    }

    public override void Work(params object[] args)
    {
        if (situ == null)
        {
            situ = args[0] as Situation;
            Set(1);
            int ArmyCount = UnityEngine.Random.Range(SingleValuePool.Ins["SlaveOfBattleArmyCountMin"].IntValue, SingleValuePool.Ins["SlaveOfBattleArmyCountMax"].IntValue + 1);
            List<Army> armys = new List<Army>();
            while (ArmyCount > 0)
            {
                armys.Add(ArmyPool.Ins.CreatArmy(4));
                ArmyCount--;
            }
            AIWarUnit = new WarEventBackPro(armys, new Person(true, null, situ.TriggerArea), true);
            var RomeWarUnit = new WarEventBackPro(situ.TriggerArea.armys, situ.TriggerArea.Umpire, false);
            RomeWarUnit.IsRes = true;
            IBG = new War(AIWarUnit, RomeWarUnit, true);
            IBG.ed += End;
        }
        else if (situ.TriggerArea.armys.Count>0&&situ.TriggerArea.Umpire!=null) {
            var RomeWarUnit = new WarEventBackPro(situ.TriggerArea.armys, situ.TriggerArea.Umpire, false);
            RomeWarUnit.IsRes = true;
            IBG = new War(AIWarUnit, RomeWarUnit, true);
            IBG.ed += End;
        }
        else
        {
            End(3);
        }
    }
    protected override void End(int res)
    {
        base.End(res);
        if (res != 5)
        {
            if (res > 2)
            {
                AIWarUnit.armyReal.Clear();
                for (int i = 0; i < AIWarUnit.armys.Count; i++)
                {
                    var ar = UnitPool.Ins.Pop("Army");
                    ar.GetComponent<ArmyUnit>().army = AIWarUnit.armys[i];
                    AIWarUnit.armyReal.Add(ar);
                    ar.GetComponent<ArmyUnit>().StayOn(AIWarUnit.Leader.StayArea);
                }
                var man = UnitPool.Ins.Pop("Man");
                man.GetComponent<ManUnit>().p = AIWarUnit.Leader;
                man.transform.position = AIWarUnit.leadReal.transform.position;
                man.SetActive(true);
                AIWarUnit.leadReal = man;
                AIWarUnit.MoveArmy();
                Area.Falled(situ.TriggerArea);
                if (situ.TriggerArea.Equals(AreaPool.Ins.Capital()))
                {
                    Debug.Log("GG");
                    return;
                }
                AIWarUnit.Move(situ.TriggerArea, AreaPool.Ins.Capital(), 1, delegate {
                    AIWarUnit.leadReal.GetComponent<ManUnit>().Stop();
                    situ.TriggerArea = AIWarUnit.Leader.StayArea;
                    situ.effects.Add(this);
                    GameEventBox.Ins.AddSitus(situ);
                });
                AIWarUnit.leadReal.GetComponent<ManUnit>().mp += delegate {
                    if (AIWarUnit.Leader.StayArea != situ.TriggerArea && AIWarUnit.Leader.StayArea.Subjection < 2)
                    {
                        AIWarUnit.leadReal.GetComponent<ManUnit>().Stop();
                        situ.TriggerArea = AIWarUnit.Leader.StayArea;
                        situ.effects.Add(this);
                        GameEventBox.Ins.AddSitus(situ);
                    }
                };
            }
        }
    }
}
