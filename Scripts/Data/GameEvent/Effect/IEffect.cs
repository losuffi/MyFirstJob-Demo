﻿using UnityEngine;
using System;
using System.Collections;
/// <summary>
/// 效果链实现方式：每次父决策对象替换时，将元父对象决策的效果组，加到现对象决策效果组，以致当前的决策中包含了该连锁事件中所有的效果。
/// TODO：如何检索相应的效果。名称？
///  定义一个ID，主效果ID唯一，且在效果组中主效果有且只有一个。新设定一个主效果，将把之前的主效果取消。
/// </summary>
public abstract class IEffect {
    public int ID { get; set; }
    public int Res { get; set; }
    public abstract void Work(params object[] args);
    public abstract void OnChange();
    public abstract void ChangeValue(params object[] args);
    public IBackGround IBG;
    public Situation situ;
    protected virtual void Set(int id)
    {
        var targets = situ.effects.FindAll(ar => ar.ID == id);
        if (targets.Count < 1)
        {
            ID = 1;
            return;
        }
        foreach (var target in targets)
        {
            target.ID = 0;
        }
        ID = 1;
    }
    public event Action end;
    protected virtual void End(int res)
    {
        Res = res;
        if (situ != null)
            situ.effects.Remove(this);
        GameEventBox.Ins.effcts.Remove(this);
        if (end != null)
            end();
    }
    public virtual void Pause(bool flag)
    {

    }
    public virtual void BreakOff()
    {

    }
}
