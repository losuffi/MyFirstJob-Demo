﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
public class Effect1 : IEffect
{
    Area target;
    WarEventBackPro A, B;
    public bool IsOne;
    public Situation sit;
    public override void Work(params object[] args)
    { 
        situ = (args[0] as Situation);
        Set(1);
        GetThePatternOfEffectWork();
    }
    public override void OnChange()
    {
        if (situ.gebp.moti != null&&situ.gebp.moti.ID!=1)
            return;
        if (IsOne)
        {
            if (A.TotalCount < A.MaxCount *0.01f*SingleValuePool.Ins["FoundMediationPercent"].IntValue || B.TotalCount < B.MaxCount * 0.01f * SingleValuePool.Ins["FoundMediationPercent"].IntValue)
            {
                var sit = SituationPool.Ins[12] as Situation;
                sit.effects.AddRange(situ.effects);
                sit.TriggerArea = target;
                IsOne = false;
                GameEventBox.Ins.AddSitus(sit);
            }
        }
    }
    protected override void End(int res)
    {
        base.End(res);
        Res = res;
        if (res != 5)
        {
            if (A.Root.IsRes)
            {
                if (res < 3)
                {
                    target.Umpire = target.MinisterA;
                }
                else
                {
                    target.Umpire = target.MinisterB;
                }
            }
            else
            {
                if (res < 3)
                {
                    target.Umpire = target.MinisterB;
                }
                else
                {
                    target.Umpire = target.MinisterA;
                }
            }
            BoardcastDate.Ins.Rec(SingleValuePool.Ins["SystemNoteMessageTitle"].StringValue, TimeDate.TimeCount, target.Umpire.Name + SingleValuePool.Ins["StrMessageBoxOtherCountryChangeUmpire"].StringValue, BoardMessage.MessageKind.None);
            target.MinisterA = new Person(true, null, target);
            target.MinisterB = new Person(true, null, target);
            UnitPool.Ins.Push(A.leadReal);
            UnitPool.Ins.Push(B.leadReal);
        }
        else
        {
            UnitPool.Ins.Push(A.leadReal);
            UnitPool.Ins.Push(B.leadReal);
            BoardcastDate.Ins.Rec(SingleValuePool.Ins["SystemNoteMessageTitle"].StringValue, TimeDate.TimeCount, target.Umpire.Name + SingleValuePool.Ins["StrMessageBoxOtherCountryStayUmpire"].StringValue, BoardMessage.MessageKind.None);
        }
        int sum = 0;
        for (int i = 0; i < A.armys.Count; i++)
        {
            sum += A.armys[i].ArmyMember;
            UnitPool.Ins.Push(A.armyReal[i]);
        }
        for (int i = 0; i < B.armys.Count; i++)
        {
            sum += B.armys[i].ArmyMember;
            UnitPool.Ins.Push(B.armyReal[i]);
        }
        target.country.Cash += (sum / target.armys[0].ArmyMember) * target.armys[0].ArmyCost;
    }
    public override void BreakOff()
    {
        base.BreakOff();
        IBG.IsEnd = true;
    }
    public override void Pause(bool flag)
    {
        base.Pause(flag);
        if (flag)
        {
            IBG.IsPause = true;
        }
        else
        {
            IBG.IsPause = false;
        }
    }
    public override void ChangeValue(params object[] args)
    {
    }
    void GetThePatternOfEffectWork()
    {
        target = situ.TriggerArea;
        var money = (target.country.Cash * 4) / 10;
        target.country.Cash -= money * 2;
        List<Army> aArmy = new List<Army>();
        List<Army> bArmy = new List<Army>();
        IsOne = true;
        for (; money > 0; money -= target.armys[0].ArmyCost)
        {
            var army = ArmyPool.Ins.CreatArmy(target.armys[0].ArmyID);
            army.OwnerArea = target;
            army.CurrentArea = target;
            army.ArmyName += target.MinisterA.Name;
            var army1 = ArmyPool.Ins.CreatArmy(target.armys[0].ArmyID);
            army1.OwnerArea = target;
            army1.CurrentArea = target;
            army1.ArmyName += target.MinisterB.Name;
            aArmy.Add(army);
            bArmy.Add(army1);
        }
        if (bArmy.Count > 0 && aArmy.Count > 0)
        {
            A = new WarEventBackPro(aArmy, target.MinisterA, true);
            B = new WarEventBackPro(bArmy, target.MinisterB, false);
            A.Res += 1;
            IBG = new War(A, B, true);
            IBG.ed += End;
        }
    }
}
