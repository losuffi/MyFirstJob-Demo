﻿using UnityEngine;
using System.Collections;
using System;

public class Effect9 : IEffect
{
    int TimeOfDurration;
    bool IsFirstTriggerInfringement;
    int TriggerInfringementProb;
    bool pause;
    public override void ChangeValue(params object[] args)
    {
    }

    public override void OnChange()
    {
        TimeOfDurration--;
        if (IsFirstTriggerInfringement)
        {
            if (!pause&&UnityEngine.Random.Range(0, 100) < TriggerInfringementProb)
            {
                IsFirstTriggerInfringement = false;
                var sit = SituationPool.Ins[4] as Situation;
                sit.effects.AddRange(situ.effects);
                sit.TriggerArea = situ.TriggerArea;
                GameEventBox.Ins.AddSitus(sit);
            }
        }
        if (TimeOfDurration <= 0)
            End(1);
    }

    public override void Work(params object[] args)
    {
        situ = args[0] as Situation;
        TriggerInfringementProb = SingleValuePool.Ins["TriggerInfringementProb"].IntValue;
        Set(1);
        Initialization();
    }
    public override void BreakOff()
    {
        base.BreakOff();
        End(1);
    }
    public override void Pause(bool flag)
    {
        base.Pause(flag);
        pause = flag;
    }
    void Initialization()
    {
        TimeOfDurration = 12;
        IsFirstTriggerInfringement = true;
        pause = false;
        if (situ.TriggerArea.Subjection > 1 && situ.TriggerArea.Subjection < 4)
        {
            situ.TriggerArea.Subjection++;
        }
    }
}
