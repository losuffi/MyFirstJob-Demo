﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
public class Effect8 : IEffect
{
    Area target;
    WarEventBackPro AIWarUnit;
    public override void ChangeValue(params object[] args)
    {
    }

    public override void OnChange()
    {
    }

    public override void Work(params object[] args)
    {
        if (situ == null)
        {
            situ = args[0] as Situation;
            Initialization();
        }
        else
        {
            Implementation();
        }
    }
    void Initialization()
    {
        Set(1);
        target = situ.TriggerArea;
        List<Army> TotalArmy = new List<Army>();
        TotalArmy.AddRange(target.armys);
        TotalArmy.AddRange(CreateArmysOnTime());
        AIWarUnit = new WarEventBackPro(TotalArmy, target.Umpire, true);
        AIWarUnit.Move(target, AreaPool.Ins.Capital(), 1, null);
        AIWarUnit.leadReal.GetComponent<ManUnit>().mp += delegate
        {
            if (AIWarUnit.Leader.StayArea != situ.TriggerArea && AIWarUnit.Leader.StayArea.Subjection < 2)
            {
                AIWarUnit.leadReal.GetComponent<ManUnit>().Stop();
                situ.TriggerArea = AIWarUnit.Leader.StayArea;
                if (situ.Kind == -3)
                {
                    var sit = SituationPool.Ins[10] as Situation;
                    sit.TriggerArea = situ.TriggerArea;
                    sit.effects.AddRange(situ.effects);
                    base.End(1);
                    GameEventBox.Ins.AddSitus(sit);
                }
                else
                {
                    GameEventBox.Ins.AddSitus(situ);
                }
            }
        };
    }
    void Implementation()
    {
        var RomeWarUnit = new WarEventBackPro(situ.TriggerArea.armys, situ.TriggerArea.Umpire, false);
        RomeWarUnit.IsRes = true;
        IBG = new War(AIWarUnit, RomeWarUnit, true);
        IBG.ed += End;
    }
    List<Army> CreateArmysOnTime()
    {
        List<Army> temp = new List<Army>();
        var money = (target.country.Cash * 5) / 10;
        var count = money / (target.armys[0].ArmyCost);
        for (int i = 0; i < count; i++)
        {
            var tem = ArmyPool.Ins.CreatArmy(target.armys[0].ArmyID);
            tem.IsOnArmy = true;
            temp.Add(tem);
        }
        target.country.Cash -= money;
        return temp;
    }

    protected override void End(int res)
    {
        base.End(res);
        if (res != 5)
        {
            if (res > 2)
            {
                AIWarUnit.armyReal.Clear();
                for(int i = 0; i < AIWarUnit.armys.Count; i++)
                {
                    var ar = UnitPool.Ins.Pop("Army");
                    ar.GetComponent<ArmyUnit>().army = AIWarUnit.armys[i];
                    AIWarUnit.armyReal.Add(ar);
                    ar.GetComponent<ArmyUnit>().StayOn(AIWarUnit.Leader.StayArea);
                }
                var man = UnitPool.Ins.Pop("Man");
                man.GetComponent<ManUnit>().p = AIWarUnit.Leader;
                man.transform.position = AIWarUnit.leadReal.transform.position;
                man.SetActive(true);
                AIWarUnit.leadReal = man;
                AIWarUnit.MoveArmy();
                Area.Falled(situ.TriggerArea);
                if (situ.TriggerArea.Equals(AreaPool.Ins.Capital()))
                {
                    Debug.Log("GG");
                    return;
                }
                AIWarUnit.Move(situ.TriggerArea, AreaPool.Ins.Capital(), 1,delegate {
                    if (AIWarUnit.Leader.StayArea != situ.TriggerArea && AIWarUnit.Leader.StayArea.Subjection < 2)
                    {
                        var sit = SituationPool.Ins[10] as Situation;
                        sit.TriggerArea = AIWarUnit.Leader.StayArea;
                        sit.effects.Add(this);
                        AIWarUnit.leadReal.GetComponent<ManUnit>().Stop();
                        situ = sit;
                        GameEventBox.Ins.AddSitus(situ);
                    }
                });
                AIWarUnit.leadReal.GetComponent<ManUnit>().mp += delegate
                {
                    if (AIWarUnit.Leader.StayArea != situ.TriggerArea && AIWarUnit.Leader.StayArea.Subjection < 2)
                    {
                        var sit = SituationPool.Ins[10] as Situation;
                        sit.TriggerArea = AIWarUnit.Leader.StayArea;
                        sit.effects.Add(this);
                        AIWarUnit.leadReal.GetComponent<ManUnit>().Stop();
                        situ = sit;
                        GameEventBox.Ins.AddSitus(situ);
                    }
                };
            }
        }
    }
}