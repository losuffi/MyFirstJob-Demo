﻿using UnityEngine;
using System.Collections;
using System;

public class Effect3 : IEffect
{
    public override void ChangeValue(params object[] args)
    {
    }

    public override void OnChange()
    {
    }

    public override void Work(params object[] args)
    {
        situ = args[0] as Situation;
        Area t = situ.TriggerArea;
        if (t != null)
        {
            if (t.Subjection < 4)
                t.Subjection += 1;
        }
        End(1);
    }
}
