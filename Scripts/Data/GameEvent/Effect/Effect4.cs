﻿using UnityEngine;
using System.Collections;
using System;

public class Effect4 : IEffect
{
    int TimeOfDuration;     //持续回合
    bool IsFirstTriggerSlaveOfBattle;
    int TriggerSlaveOfBattleProb;
    public override void OnChange()
    {
        TimeOfDuration--;
        if (IsFirstTriggerSlaveOfBattle)
        {
            if (UnityEngine.Random.Range(0, 100) < TriggerSlaveOfBattleProb)
            {
                IsFirstTriggerSlaveOfBattle = false;
                var sit = SituationPool.Ins[9] as Situation;
                sit.effects.AddRange(situ.effects);
                sit.TriggerArea = situ.TriggerArea;
                GameEventBox.Ins.AddSitus(sit);
            }
        }
        if (TimeOfDuration <= 0)
        {
            End(1);
        }
    }

    public override void Work(params object[] args)
    {
        situ = args[0] as Situation;
        TriggerSlaveOfBattleProb = SingleValuePool.Ins["TriggerSlaveOfBattleProb"].IntValue;
        TimeOfDuration = 6;
        Set(1);
        IsFirstTriggerSlaveOfBattle = true;
    }
    public void ChangeDuration(int x)
    {
        TimeOfDuration += x;
    }
    public override void ChangeValue(params object[] args)
    {
        TimeOfDuration += (int)args[0];
    }
}
