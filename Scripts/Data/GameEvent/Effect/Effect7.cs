﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Effect7 : IEffect
{
    Area DefenderArea,OffenseArea;
    WarEventBackPro Defender, Offense;
    bool IsFirstTriggerFunc;
    public override void ChangeValue(params object[] args)
    {
    }

    public override void OnChange()
    {
        if (situ.gebp.moti != null && situ.gebp.moti.ID != 1)
            return;
        if (IsFirstTriggerFunc)
        {
            if (IsSendMessageToRome())
            {
                IsFirstTriggerFunc = false;
                if (DefenderArea.Subjection == 2)   //同盟国
                {
                    //请求罗马来援军
                    TriggerRequestReinforcements();
                }
                else
                {
                    //请求调停
                    TriggerRequestMediation();
                }
            }
        }
    }

    public override void Work(params object[] args)
    {
        situ = args[0] as Situation;
        Set(1);
        GetThePatternOfEffectWork();
    }
    void GetThePatternOfEffectWork()
    {
        DefenderArea = situ.TriggerArea;
        var AvailableOffenseAreas = GetAvailableOffenseAreas();
        OffenseArea = AvailableOffenseAreas[UnityEngine.Random.Range(0, AvailableOffenseAreas.Count)];
        Defender = new WarEventBackPro(DefenderArea.armys, DefenderArea.Umpire, false);
        Offense = new WarEventBackPro(OffenseArea.armys, OffenseArea.Umpire, true);
        Offense.Leader.StayArea = DefenderArea;
        IBG=new War(Offense, Defender, true);
        IBG.ed += End;
        IsFirstTriggerFunc = true;
    }
    List<Area> GetAvailableOffenseAreas()
    {
        List<Area> AvailableOffenseAreas = new List<Area>();
        for (int i = 1; i < AreaPool.Ins.Count + 1; i++)
        {
            var areatemp = AreaPool.Ins.ReadPool(i) as Area;
            if (areatemp.Subjection < 2 || areatemp.Equals(DefenderArea))
                continue;
            if (AreaRoad.Ins.GetRoadDistance(DefenderArea.AreaID, areatemp.AreaID) <= SingleValuePool.Ins["MaxNearDistance"].IntValue)
            {
                AvailableOffenseAreas.Add(areatemp);
            }
        }
        return AvailableOffenseAreas;
    }
    bool IsSendMessageToRome()
    {
        return (Defender.TotalCount < Defender.MaxCount * 0.01f * SingleValuePool.Ins["FoundMediationPercent"].IntValue);
    }
    void TriggerRequestMediation()
    {
        var sit = SituationPool.Ins[12] as Situation;
        sit.effects.AddRange(situ.effects);
        sit.TriggerArea = DefenderArea;
        GameEventBox.Ins.AddSitus(sit);
    }
    void TriggerRequestReinforcements()
    {
        var sit = SituationPool.Ins[18] as Situation;
        sit.effects.AddRange(situ.effects);
        sit.TriggerArea = DefenderArea;
        GameEventBox.Ins.AddSitus(sit);
    }
}
