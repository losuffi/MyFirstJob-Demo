﻿using UnityEngine;
using System.Collections;
using System;

public class Effect13 : IEffect
{
    bool flag;
    int Count;
    public override void ChangeValue(params object[] args)
    {
        flag = false;
        Count = situ.TriggerArea.CD;
    }

    public override void OnChange()
    {
        if (flag)
        {
            situ.TriggerArea.IsTheFalledArea = false;
            Count--;
            if (Count <= 0)
            {
                End(1);
            }
        }
        else
        {
            Count--;
            situ.TriggerArea.IsTheFalledArea = true;
            if (Count <= 0)
            {
                situ.TriggerArea.country = AreaPool.Ins.Capital().country;
                situ.TriggerArea.Subjection = 0;
                LawPool.Ins.BindLaw(situ.TriggerArea);
                situ.TriggerArea.IsTheFalledArea = false;
                End(1);
            }
        }
               
    }

    public override void Work(params object[] args)
    {
        situ = args[0] as Situation;
        Set(1);
        flag = true;
        situ.TriggerArea.IsTheFalledArea = true;
        Count = 2;
    }
}
