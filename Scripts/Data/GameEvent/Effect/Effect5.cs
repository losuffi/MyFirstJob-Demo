﻿using UnityEngine;
using System.Collections;
using System;

public class Effect5 : IEffect
{
    int AreaGoods;
    bool Flag;
    public override void ChangeValue(params object[] args)
    {
        return;
    }

    public override void OnChange()
    {
        if (!Flag)
        {
            var effmain = situ.effects.Find(ar => { return ar.ID == 1; });
            Debug.Log(situ.effects[0].GetType());
            if (effmain != null)
            {
                Flag = true;
                effmain.end += Complete;
            }
        }
        return;
    }

    public override void Work(params object[] args)
    {
        situ = args[0] as Situation;
        var effmain = situ.effects.Find(ar => { return ar.ID == 1; });
        if (effmain != null)
        {
            effmain.end += Complete;
        }
        Flag = false;
        AreaGoods = situ.TriggerArea.Specialty;
        situ.TriggerArea.Specialty = 0;
    }
    void Complete()
    {
        End(1);
    }
    protected override void End(int res)
    {
        base.End(res);
        situ.TriggerArea.Specialty = AreaGoods;
    }
}
