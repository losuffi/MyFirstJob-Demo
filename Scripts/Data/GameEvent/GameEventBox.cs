﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;
using Tools;
public class GameEventBox{
    private GameEventBox() {
        TimeControl.Ins.ep += WorkQurp;
    }
    private static GameEventBox ins;
    public static GameEventBox Ins
    {
        get
        {
            if (ins == null)
                ins = new GameEventBox();
            return ins;
        }
    }
    public List<Situation> Situs = new List<Situation>();                      //对应容器A
    public Dictionary<Situation,GameEventBackPro> Gebps = new Dictionary<Situation, GameEventBackPro>();         //对应容器A，中的情景生成的AI决策。
    public List<GameEventBackPro> ResGebps = new List<GameEventBackPro>();    //对应容器B
    public List<GameEventBackPro> CacGebps = new List<GameEventBackPro>();      //对应容器C
    public Dictionary<IWork, GameEventBackPro> works = new Dictionary<IWork, GameEventBackPro>();
    public List<IEffect> effcts = new List<IEffect>();
    List<IWork> Garbage = new List<IWork>();
    public void AddSitus(Situation t)
    {
        for(int c = 0; c < t.effects.Count; c++)
        {
            if (!effcts.Contains(t.effects[c]))
            {
                effcts.Add(t.effects[c]);
                t.effects[c].Work(t);
            }
        }
        if (t.Kind >= 0)
        {
            Situs.Add(t);
            GameEventBackPro AIgebp = AIGameEventPro.CreateBackPro(t);
            Gebps.Add(t, AIgebp);
            BoardcastDate.Ins.Rec(t.Name, TimeDate.TimeCount,
            (t.TriggerArea != null ? t.TriggerArea.AreaName : "") + "发生了——" + t.Name + "请尽快去处理", BoardMessage.MessageKind.Senate);
        }
        else if(t.Kind==-1)
        {
            BoardcastDate.Ins.Rec(t.Name, TimeDate.TimeCount,
            (t.TriggerArea != null ? t.TriggerArea.AreaName : "") + "发生了——" + t.Name, BoardMessage.MessageKind.None);
        }
        else if (t.Kind == -2)
        {
            Situs.Add(t);
        }
    }
    public int Count { get
        {
            return Situs.Count;
        } }
    public void Clear()
    {
        Situs.Clear();
    }
    public Situation this[int index]
    {
        get
        {
            return Situs[index];
        }
    }
    public void RemovSit(GameEventBackPro t)
    {
        if (Situs.Contains(t.situ))
            Situs.Remove(t.situ);
    }
    public void PassGebp(GameEventBackPro t)
    {
        if (t.situ.IsLaw)
        {
            HandleLaw(t);
            //RemovSit(t);
            return;
        }
        int situid = t.situ.Sid;
        t.situ.gebp = t;
        int mid = t.moti == null ? 0 : t.moti.ID;
        BoardcastDate.Ins.Rec(SingleValuePool.Ins["StrMessageBoxOtherSenNoteTitle"].StringValue, TimeDate.TimeCount, t.situ.TriggerArea.AreaName + ":" + t.situ.Name + "\n" + (t.moti == null ? "无通过提案" : t.moti.Name), BoardMessage.MessageKind.None);
        DateBase.ConnectDate();
        DateBase.Query(string.Format("select * from gameeventbackpro where GameEventBackProStart={0} and GameEventBackProMot={1}", situid, mid));
        DateBase.mdr.Read();
        int l = 0;
        if (DateBase.mdr.HasRows)
            l = (int)DateBase.mdr[3];
        DateBase.CloseConnect();
        var pa = CacGebps.Find(ar => { return ar.Nextsitu.Equals(t.situ); });
        if (pa != null)
        {
            t.Parent = pa;
            t.situ.effects.AddRange(pa.situ.effects);
        }
        if (l > 0)
        {
            t.Nextsitu = SituationPool.Ins[l] as Situation;
            t.Nextsitu.TriggerArea = t.situ.TriggerArea;
            ResGebps.Add(t);
        }
        else if (l < 0)
        {
            var work = Assembly.GetExecutingAssembly().CreateInstance("Work" + Mathf.Abs(l)) as IWork;
            work.Start(t);
            works.Add(work, t);
            ResGebps.Add(t);
        }
        else
        {
            //todo:事件完结，调用完结函数计算。
            //应该是等待效果结束，才算事件结束。
        }
        RemovSit(t);
    }
    //连接父级 应该放在通过时，而不再替换父级中。
    //每个回合预先调用 该方法。放置有清空事件功能。
    public void GetResSitu()
    {
        for(int i = 0; i < Situs.Count; i++)
        {
            if (!Situs[i].IsLaw)
                PassGebp(Gebps[Situs[i]]);
        }
        Gebps.Clear();
        for(int i = 0; i < ResGebps.Count; i++)
        {
            if (ResGebps[i].Nextsitu != null)
            {
                AddSitus(ResGebps[i].Nextsitu);
                var pa= CacGebps.Find(ar => { return ar.Nextsitu.Equals(ResGebps[i].situ); });
                if (pa != null)
                {
                    CacGebps.Remove(pa);
                }
                else
                {
                    ResGebps[i].Parent = null;
                }
                CacGebps.Add(ResGebps[i]);
                ResGebps.RemoveAt(i);
            }
        }
        for(int j = 0; j < effcts.Count; j++)
        {
            effcts[j].OnChange();
        }
    }
    void WorkQurp()
    {
        Garbage.Clear();
        foreach(var work in works)
        {
            int res = work.Key.Result();
            if (res > 0)
            {
                DateBase.ConnectDate();
                DateBase.Query(string.Format("select * from gameeventbackpro where GameEventBackProStart={0} and GameEventBackProMot={1}", work.Value.situ.Sid, work.Value.moti == null ? 0 : work.Value.moti.ID));
                DateBase.mdr.Read();
                string resstr = DateBase.mdr[5].ToString().Split('|')[res-1];
                res = int.Parse(resstr);
                if (res == 0)
                {
                    //todo:事件完结，调用完结函数计算。
                    ResGebps.Remove(work.Value);
                }
                else if(res>0)
                {
                    work.Value.Nextsitu = SituationPool.Ins[res] as Situation;
                    work.Value.Nextsitu.TriggerArea = work.Value.situ.TriggerArea;
                }
                work.Key.OnEnd();
                Garbage.Add(work.Key);
                DateBase.CloseConnect();
            }
            else if (res < 0)
                Garbage.Add(work.Key);
            else
            {
                work.Key.OnWork();
            }
        }
        for(int i = 0; i < Garbage.Count; i++)
        {
            works.Remove(Garbage[i]);
        }
    }
    void HandleLaw(GameEventBackPro p)
    {
        if (p.moti == null)
            return;
        int LawID = p.moti.ID;
        var area = p.excts[2] as Area;
        var Law = LawPool.Ins[LawID];
        if (Law.Command == 8)
        {
            //TODO:征伐   侵犯公民——征伐
            var sit = SituationPool.Ins[4] as Situation;
            sit.TriggerArea = area;
            var mot = MotionPool.Ins[23] as Motion;
            var gebp = new GameEventBackPro(sit, p.person, mot);
            gebp.excts[2] = p.excts[4];
            PassGebp(gebp);
        }
        else if (Law.Command == 1)
        {
            var sit = SituationPool.Ins[19] as Situation;
            sit.TriggerArea = area;
            var mot = MotionPool.Ins[2] as Motion;
            var gebp = new GameEventBackPro(sit, p.person, mot);
            gebp.excts[1] = p.excts[1];
            PassGebp(gebp);
        }
        else if (Law.Command == 4)
        {
            var sit = SituationPool.Ins[19] as Situation;
            sit.TriggerArea = area;
            var mot = MotionPool.Ins[4] as Motion;
            var gebp = new GameEventBackPro(sit, p.person, mot);
            gebp.excts[1] = p.excts[1];
            PassGebp(gebp);
        }
        else if (Law.Command == 0)
        {
            Law = area.Laws.Find(ar => ar.ID == LawID);
            int ChangeId = (int)p.excts[3];
            Law.Value = Law.Sections[ChangeId];
        }
    }
}
