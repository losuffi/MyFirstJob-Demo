﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
//修改战争机制，减少数据端，进行逻辑处理
public class WarEventBackPro  {
    public bool IsRoot { get; set; } //是否为根
    public bool IsRes { get; set; } //结果判定标志
    public int Res { get; set; }    //标志权重
    public War waring { get; set; }
    public Person Leader { get; set; }
    public List<Army> armys { get; set; }
    public List<GameObject> armyReal { get; set; }
    public GameObject leadReal { get; set; }
    public bool WarNature { get; set; }
    public WarEventBackPro Target { get; set; }
    public int TotalCount { get
        {
            var t = 0;
            for(int i = 0; i < armys.Count; i++)
            {
                t += armys[i].ArmyMember;
            }
            return t;
        } }
    public int MaxCount { get; set; }
    public int MaxArmyCount { get; set; }
    public int TimeDefense { get; set; }
    public WarEventBackPro Adder { get; set; }
    public WarEventBackPro Root { get; set; }
    public int WarEventBackProWeight { get; set; }
    public static List<WarEventBackPro> WorldWarEvent;
    static WarEventBackPro()
    {
        WorldWarEvent = new List<WarEventBackPro>();
    }
    public WarEventBackPro(List<Army> a,Person l,bool nature)
    {
        armys = new List<Army>();
        armys.AddRange(a);
        Leader = l;
        Leader.Idel = false;
        WarNature = nature;         //真为 主动进攻。
        armyReal = new List<GameObject>();
        MaxCount = TotalCount;
        MaxArmyCount = armys.Count;
        WarEventBackProWeight = 0;
        for (int c = 0; c < armys.Count; c++)
            WarEventBackProWeight += armys[c].ArmyWeight;
        if (nature)
        {
            armyReal.Clear();
            for (int i = 0; i < armys.Count; i++)
            {
                var obj = UnitPool.Ins.Pop("Army");
                armyReal.Add(obj);
                armys[i].WorkTime = TimeDate.TimeCount;
                armys[i].State = Army.states.Run;
                obj.GetComponent<ArmyUnit>().army = armys[i];
                obj.GetComponent<ArmyUnit>().StayOn(Leader.StayArea);
            }
            leadReal = UnitPool.Ins.Pop("Man");
            leadReal.GetComponent<ManUnit>().p = Leader;
            leadReal.GetComponent<ManUnit>().StayOn(Leader.StayArea);
            //leadReal.GetComponent<ManUnit>().MoveOn(Leader.StayArea, target.Leader.StayArea, 1);
            //leadReal.GetComponent<ManUnit>().cp += StartWar;
            //leadReal.GetComponent<ManUnit>().mp += MoveArmy;
            MoveArmy();
        }
        Adder = null;
        IsRoot = false;
        IsRes = false;
        Res = 0;
        if(l.Job== Person.JobKind.Consul ||l.Job==Person.JobKind.Umpire) 
        {
            startarea = l.StayArea;
            BackGroundJobs.Ins.cw += delegate
            {
                var p = Leader;
                startarea.Umpire.StayArea = p.StayArea;
                Leader = startarea.Umpire;
                leadReal.GetComponent<ManUnit>().p = Leader;
                if (Root != null&& Root.Leader.Equals(p))
                {
                    Root.Leader = Leader;
                }
            };
        }
        WorldWarEvent.Add(this);
    }
    Area startarea;
    public WarEventBackPro()
    {
        armys = new List<Army>();
        armyReal = new List<GameObject>();
        Leader = null;
        leadReal = null;
        WarEventBackProWeight = 0;
        Adder = null;
        MaxCount = TotalCount;
        MaxArmyCount = armys.Count;
        IsRoot = false;
        IsRes = false;
        Res = 0;
        WorldWarEvent.Add(this);
    }
    public void ChangeStatus(Army.states t)
    {
        for(int i = 0; i < armys.Count; i++)
        {
            armys[i].State = t;
        }
    }
    //public void Attack(WarEventBackPro target)
    //{
    //    if (!WarNature)
    //        return;
    //    Target = target;
    //    armyReal.Clear();
    //    for(int i = 0; i < armys.Count; i++)
    //    {
    //        var obj = UnitPool.Ins.Pop("Army");
    //        armyReal.Add(obj);
    //        armys[i].WorkTime = TimeDate.TimeCount;
    //        armys[i].State = Army.states.Run;
    //        obj.GetComponent<ArmyUnit>().army = armys[i];
    //        obj.GetComponent<ArmyUnit>().StayOn(Leader.StayArea);
    //    }
    //    leadReal = UnitPool.Ins.Pop("Man");
    //    leadReal.GetComponent<ManUnit>().p = Leader;
    //    leadReal.GetComponent<ManUnit>().MoveOn(Leader.StayArea, target.Leader.StayArea, 1);
    //    leadReal.GetComponent<ManUnit>().cp += StartWar;
    //    leadReal.GetComponent<ManUnit>().mp += MoveArmy;
    //    MoveArmy();
    //}
    //public void AddArmy(WarEventBackPro t)
    //{
    //    for (int i = 0; i < armys.Count; i++)
    //    {
    //        var obj = UnitPool.Ins.Pop("Army");
    //        armyReal.Add(obj);
    //        armys[i].WorkTime = TimeDate.TimeCount;
    //        armys[i].State = Army.states.Run;
    //        obj.GetComponent<ArmyUnit>().army = armys[i];
    //        obj.GetComponent<ArmyUnit>().StayOn(Leader.StayArea);
    //    }
    //    leadReal = UnitPool.Ins.Pop("Man");
    //    leadReal.GetComponent<ManUnit>().p = Leader;
    //    leadReal.GetComponent<ManUnit>().MoveOn(Leader.StayArea, t.Leader.StayArea, 1);
    //    leadReal.GetComponent<ManUnit>().cp += delegate
    //    {

    //    };
    //    leadReal.GetComponent<ManUnit>().mp += MoveArmy;
    //    MoveArmy();
    //}
    public void Defense()
    {
        if (WarNature)
            return;
        for(int i = 0; i < armys.Count; i++)
        {
            var obj = UnitPool.Ins.Pop("Army");
            armyReal.Add(obj);
            armys[i].WorkTime = TimeDate.TimeCount;
            armys[i].State = Army.states.Battle;
            obj.GetComponent<ArmyUnit>().army = armys[i];
            obj.GetComponent<ArmyUnit>().StayOn(Leader.StayArea);
        }
        TimeDefense = TimeDate.TimeCount;
        leadReal = UnitPool.Ins.Pop("Man");
        leadReal.GetComponent<ManUnit>().p = Leader;
        leadReal.GetComponent<ManUnit>().StayOn(Leader.StayArea);
    }
    /// <summary>
    /// 数据以准备好！
    /// </summary>
    void StartWar()
    {
        TimeControl.Ins.ep -= MoveArmy;
        Target.Defense();
        new War(this, Target);
    }
    public void MoveArmy()
    {
        int RowNum = armys.Count / 5 + 1;
        int RowedCount = 0;
        int CurrentI = 0;
        int zindex = 0;
        float N = 2; //军队排列间距。War 对应修改构造函数 的zz
        while (RowNum > 0)
        {
            if ((armys.Count - RowedCount * 5) >= 5)
            {
                zindex = 5;
            }
            else
            {
                zindex = (armys.Count - RowedCount * 5) % 5;
            }
            zindex = zindex == 0 ? 5 : zindex;
            for(int i = 0; i < zindex; i++)
            {
                var _x = (-N + (5 - zindex) * N / 4) + i * N / 2;
                if (CurrentI >= armyReal.Count)
                    break;
                armyReal[CurrentI].transform.position = leadReal.transform.position;
                armyReal[CurrentI].transform.rotation = leadReal.transform.rotation;
                armyReal[CurrentI].transform.position += N / 2*RowNum * armyReal[CurrentI].transform.forward;
                armyReal[CurrentI].transform.Rotate(Vector3.up * 90);
                armyReal[CurrentI].transform.position += _x * armyReal[CurrentI].transform.forward;
                armyReal[CurrentI].transform.Rotate(Vector3.up * -90);
                armys[CurrentI].CurrentArea = Leader.StayArea;
                CurrentI++;
            }
            RowedCount++;
            RowNum--;
        }
    }
    public int SubCount, SubArmyCount;
    public void Move(Area start, Area target, int count,Action method)
    {
        leadReal.GetComponent<ManUnit>().MoveOn(start, target, 1);
        leadReal.GetComponent<ManUnit>().mp += MoveArmy;
        leadReal.GetComponent<ManUnit>().cp += delegate
        {
            for(int i = 0; i < armyReal.Count; i++)
            {
                armyReal[i].GetComponent<ArmyUnit>().army.State = Army.states.Stay;
            }
            if(method!=null)
                method();
        };
    }
    public void Move(Area start, Area target, int count, Action method,Action method2)
    {
        leadReal.GetComponent<ManUnit>().MoveOn(start, target, 1);
        leadReal.GetComponent<ManUnit>().mp += MoveArmy;
        leadReal.GetComponent<ManUnit>().cp += delegate
        {
            MoveArmy();
            for (int i = 0; i < armyReal.Count; i++)
            {
                armyReal[i].GetComponent<ArmyUnit>().army.State = Army.states.Stay;
            }
            if (method != null)
                method();
        };
        leadReal.GetComponent<ManUnit>().mp += method2;
    }
    public bool IsWinner;
}
