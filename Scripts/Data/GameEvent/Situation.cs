﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Situation {

    public int Sid { get; set; }
    public string Name { get; set; }
    public List<Area> areas = new List<Area>();
    public List<Motion> motions = new List<Motion>();
    public Area TriggerArea = null;
    public int Kind { get; set; }
    public sbyte erupt { get; set; }
    public int Weight { get; set; }
    public bool IsLaw { get; set; }
    public List<IEffect> effects { get; set; }
    public GameEventBackPro gebp { get; set; }
}
