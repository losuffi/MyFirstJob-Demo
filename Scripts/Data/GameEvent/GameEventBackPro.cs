﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class GameEventBackPro{
    public bool isValid { get; set; }
    public int value { get; set; }
    public int npo { get; set; }
    public int cost { get; set; }
    public int totalcount { get; set; }
    public Person person { get; set; }
    public Situation situ { get; set; }
    public Motion moti { get; set; }
    public GameEventBackPro Parent { get; set; }
    public Situation Nextsitu { get; set; }
    public int MotiNature { get; set; }
    public int AddNpo { get; set; }
    public int AddVal { get; set; }
    public GameEventBackPro(Situation sit,Person p,Motion mot)
    {
        value = mot.Value;
        cost = 10;
        npo = 0;
        totalcount = 3;
        person = p;
        situ = sit;
        moti = mot;
        isValid = false;
        excts = new Dictionary<int, object>();
        AddNpo = 0;
    }
    public GameEventBackPro(Situation sit)
    {
        isValid = false;
        excts = new Dictionary<int, object>();
        situ = sit;
        moti = null;
        person = null;
    }
    public static bool Battle(Argue aA,GameEventBackPro a,Argue bB,GameEventBackPro b)
    {
        //数值结算。
        if (a.cost < aA.Cost)
            return false;
        int tempa = a.value;
        int tempb = b.value;
        a.totalcount++;
        a.cost -= aA.Cost;
        b.totalcount++;
        b.cost -= bB.Cost;
        var valueAdd = (a.person.Brain * 0.1f) * (aA.ValueA * PaiXiBattle(aA, bB));
        var valueBdd = (b.person.Brain * 0.1f) * (bB.ValueA * PaiXiBattle(bB, aA));
        if (valueAdd - valueBdd > 0)
        {
            b.value -= (int)(valueAdd - valueBdd);
        }
        else
        {
            a.value -= (int)(valueBdd - valueAdd);
        }
        a.value = a.value < 0 ? 0 : a.value;
        b.value = b.value < 0 ? 0 : b.value;
        a.AddNpo= (int)((a.person.Charm * 0.1f) * aA.ValueB * PaiXiBattle(aA, bB));
        b.AddNpo= (int)((b.person.Charm * 0.1f) * bB.ValueB * PaiXiBattle(bB, aA));
        a.npo += a.AddNpo;
        b.npo += b.AddNpo;
        a.AddVal = tempa - a.value;
        b.AddVal = tempb - b.value;
        return true;
    }
    static float PaiXiBattle(Argue a,Argue b)
    {
        float res;
        switch(a.Kind)
        {
            case 1:
                res = b.Kind == 3 ? 1.5f : 1;
                break;
            case 2:
                res = b.Kind == 1 ? 1.5f : 1;
                break;
            default:
                res = b.Kind == 2 ? 1.5f : 1;
                break;
        }
        return res;
    }
    public string Nature
    {
        //get
        //{
        //    switch (MotiNature)
        //    {
        //        case 1:
        //            return "激进";
        //        case -1:
        //            return "保守";
        //        default:
        //            return "中立";
        //    }
        //}
        get
        {
            if (MotiNature == 1)
            {
                return "激进";
            }
            else
            {
                return "保守";
            }
        }
    }
    public float MotiWeight { get; set; }
    public Dictionary<int, object> excts;
    public static string ConverNature(int i)
    {
            switch (i)
            {
                case 1:
                    return "激进";
                case -1:
                    return "保守";
                default:
                    return "中立";
            }
    }
}
