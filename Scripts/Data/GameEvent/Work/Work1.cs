﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// 打仗
/// </summary>
public class Work1 : IWork
{
    War Listen;
    WarEventBackPro TheRomeWarUnit;
    Area start;
    int flag;
    public override int Result()
    {
        if (flag != 0)
            return flag;
        return Listen != null ? Listen.Res : 0;
    }

    public override void Start(params object[] args)
    {
        flag = 0;
        gebp = args[0] as GameEventBackPro;
        start = AreaPool.Ins.Capital();
        CertainTheWarUnit();
        start = TheRomeWarUnit.Leader.StayArea;
        TheRomeWarUnit.Res += 2;
        InplementationByOneWay();
    }
    List<Army> CreateArmysOnTime(int ID)
    {
        List<Army> temp = new List<Army>();
        var count = LawPool.Ins[3].Value;
        for (int i = 0; i < count; i++)
        {
            if (start.country.Cash - (ArmyPool.Ins.ReadPool(ID) as Army).ArmyCost < 0)
                break;
            var tem = ArmyPool.Ins.CreatArmy(ID);
            tem.IsOnArmy = true;
            start.country.Cash -= tem.ArmyCost;
            temp.Add(tem);
        }
        return temp;
    }
    int ArmyConvertCash(List<Army> temp)
    {
        int sum = 0;
        for (int i = 0; i < temp.Count; i++)
        {
            if (temp[i].IsOnArmy) {
                sum += (int)(temp[i].ArmyCost * (float)(temp[i].ArmyMember / (ArmyPool.Ins.ReadPool(temp[i].ArmyID) as Army).ArmyMember));
                temp.RemoveAt(i);
             }
        }
        return sum;
    }
    public override void OnWork()
    {
        Listen = TheRomeWarUnit.waring;
    }
    public override void OnEnd()
    {
        for(int i = 0; i < TheRomeWarUnit.armyReal.Count; i++)
        {
            UnitPool.Ins.Push(TheRomeWarUnit.armyReal[i]);
        }
        UnitPool.Ins.Push(TheRomeWarUnit.leadReal);
        TheRomeWarUnit.armyReal.Clear();
        for (int i = 0; i < TheRomeWarUnit.armys.Count; i++)
        {
            if (TheRomeWarUnit.armys[i].ArmyMember == 0)
            {
                TheRomeWarUnit.armys.RemoveAt(i);
            }
        }
        for (int i = 0; i < TheRomeWarUnit.armys.Count; i++)
        {
            var ar = UnitPool.Ins.Pop("Army");
            ar.GetComponent<ArmyUnit>().army = TheRomeWarUnit.armys[i];
            TheRomeWarUnit.armyReal.Add(ar);
            ar.GetComponent<ArmyUnit>().StayOn(TheRomeWarUnit.Leader.StayArea);
        }
        var man = UnitPool.Ins.Pop("Man");
        man.GetComponent<ManUnit>().p = TheRomeWarUnit.Leader;
        man.transform.position = TheRomeWarUnit.leadReal.transform.position;
        man.SetActive(true);
        TheRomeWarUnit.leadReal = man;
        TheRomeWarUnit.MoveArmy();
        TheRomeWarUnit.Move(TheRomeWarUnit.Leader.StayArea, start, 1, delegate{
            TheRomeWarUnit.Leader.OwnerArea.country.Cash += ArmyConvertCash(TheRomeWarUnit.armys);  //军队资金转化为领导的所属国
            TheRomeWarUnit.Leader.Idel = true;
            UnitPool.Ins.Push(TheRomeWarUnit.leadReal);
            for(int i = 0; i < TheRomeWarUnit.armyReal.Count; i++)
            {
                UnitPool.Ins.Push(TheRomeWarUnit.armyReal[i]);
            }
        } );
    }
    void CertainTheWarUnit()
    {
        if (!gebp.excts.ContainsKey(1))
        {
            if (gebp.excts.ContainsKey(2))
            {
                List<Army> temparmys = new List<Army>();
                temparmys.AddRange(AreaPool.Ins.Capital().Umpire.StayArea.armys);
                temparmys.AddRange(CreateArmysOnTime((gebp.excts[2] as Area).armys[0].ArmyID));
                TheRomeWarUnit = new WarEventBackPro(temparmys, AreaPool.Ins.Capital().Umpire, true);
            }
            else
            {
                TheRomeWarUnit = new WarEventBackPro(AreaPool.Ins.Capital().Umpire.StayArea.armys, AreaPool.Ins.Capital().Umpire, true);
            }
        }
        else
        {
            if ((gebp.excts[1] as Person).Job == Person.JobKind.Umpire)
                TheRomeWarUnit = new WarEventBackPro((gebp.excts[1] as Person).StayArea.armys, (gebp.excts[1] as Person), true);
            else
                TheRomeWarUnit = new WarEventBackPro(CreateArmysOnTime(1), (gebp.excts[1] as Person), true);
        }
    }
    void InplementationByOneWay()
    {
        if (gebp.Parent!=null&&gebp.Parent.moti.Nature == 6)
        {
            Person otherper = gebp.Parent.excts[1] as Person;
            WarEventBackPro target = WarEventBackPro.WorldWarEvent.Find(ar => { return (ar.Leader == otherper && ar.IsRoot); });
            new AddWar(TheRomeWarUnit, target, TheRomeWarUnit.Leader,AddWarIsBreakOff);
        }
        else if (gebp.situ.TriggerArea.Subjection < 3)
        {
            WarEventBackPro target = WarEventBackPro.WorldWarEvent.Find(ar => ar.Leader == gebp.situ.TriggerArea.Umpire && ar.IsRoot);
            if (target != null)
            {
                new AddWar(TheRomeWarUnit, target, TheRomeWarUnit.Leader, AddWarIsBreakOff);
            }
            else
            {
                AddWarIsBreakOff();
            }
        }
        else
        {
            AddWarIsBreakOff();
        }
    }
    void AddWarIsBreakOff()
    {
        flag = 5;
    }
}
