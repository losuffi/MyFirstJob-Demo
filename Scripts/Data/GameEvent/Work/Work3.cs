﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Work3 : IWork
{
    IEffect MainEffect;
    public override void OnEnd()
    {
        return;
    }

    public override void OnWork()
    {
        return;
    }

    public override int Result()
    {
        return GetResult((int)gebp.excts[0]);
    }

    public override void Start(params object[] args)
    {
        gebp = args[0] as GameEventBackPro;
        MainEffect = gebp.situ.effects.Find(ar => { return ar.ID == 1; });
        if (MainEffect == null)
            return;
    }
    int GetResult(int Source)
    {
        MainEffect.ChangeValue(-(Source / 500));
        return Source / 500;
    }
}
