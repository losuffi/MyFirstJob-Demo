﻿using UnityEngine;
using System.Collections;
public abstract class IWork {
    protected GameEventBackPro gebp;
    public abstract void Start(params object[] args);
    public abstract int Result();
    public abstract void OnWork();
    public abstract void OnEnd();
}
