﻿using UnityEngine;
using System.Collections;
using System;
/// <summary>
/// 对地区的叛乱双方的调停
/// </summary>
public class Work2 : IWork
{
    Mediation listen;
    IEffect t;
    public override int Result()
    {
        return listen.Res;
    }

    public override void Start(params object[] args)
    {
        gebp = args[0] as GameEventBackPro;
        listen = new Mediation(gebp.excts[1] as Person, gebp.situ.TriggerArea, gebp.situ.Weight);
        if (gebp.situ.effects.Find(ar => { return ar.ID == 1; }) == null)
            return;
        t = gebp.situ.effects.Find(ar => { return ar.ID == 1; });
        t.Pause(true);
        listen.ed += delegate (int i) { t.Pause(false); };
    }
    public override void OnWork()
    {

    }
    public override void OnEnd()
    {
        if (Result() == 1)
        {
            t.BreakOff();
        }
    }
}
