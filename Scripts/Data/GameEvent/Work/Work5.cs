﻿using UnityEngine;
using System.Collections;
using System;

public class Work5 : IWork
{
    public override void OnEnd()
    {
    }

    public override void OnWork()
    {
    }

    public override int Result()
    {
        return 1;
    }

    public override void Start(params object[] args)
    {
        gebp = args[0] as GameEventBackPro;
        var EffectMain = gebp.situ.effects.Find(ar => ar.ID == 1);
        EffectMain.ChangeValue();
    }
}
