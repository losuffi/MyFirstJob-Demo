﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Motion{
    public int ID { get; set; }
    public string Name { get; set; }
    public int RelaADD { get; set; }
    public int RelaSub { get; set; }
    public int CreditX { get; set; }
    public int CreditY { get; set; }
    public int Score { get; set; }
    public int Value { get; set; }
    public int Nature { get; set; }
    public int Weight { get; set; }
    public List<int> callbacks = new List<int>();
    public List<Situation> Callbacks = new List<Situation>();
}
