﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Tools;
public class RelationPool : IPool
{
    private static RelationPool ins;
    private RelationPool() { }
    public static RelationPool Ins
    {
        get
        {
            if (ins == null)
                ins = new RelationPool();
            return ins;
        }
    }
    public override void InPool()
    {
        DateBase.ConnectDate();
        DateBase.Query("select * from relation");
        while (DateBase.mdr.Read())
        {
            var rela = new Relation();
            rela.Id = (int)DateBase.mdr[0];
            rela.FirstFamilyId = (int)DateBase.mdr[1];
            rela.SecondFamilyId = (int)DateBase.mdr[2];
            rela.ChageValue((int)DateBase.mdr[3]);
            rela.Coefficient = (Double)DateBase.mdr[4];
            map.Add(rela.Id, rela);
        }
        DateBase.CloseConnect();
    }

    public override object ReadPool(int ID)
    {
        return map[ID];
    }
    public List<Relation> ReadByFirstID(int fid)
    {
        List<Relation> temp = new List<Relation>();
        for(int i = 0; i < map.Count; i++)
        {
            var t = map[i+1] as Relation;
            if (t.FirstFamilyId == fid)
                temp.Add(t);
            else if (t.SecondFamilyId == fid)
                temp.Add(t);
        }
        return temp;
    }
    public Relation ReadValue(int fid,int sid)
    {
        for(int i = 1; i < map.Count+1; i++)
        {
            var x = map[i] as Relation;
            if (x.FirstFamilyId == fid && x.SecondFamilyId == sid)
            {
                return x;
            }
            else if (x.FirstFamilyId == sid && x.SecondFamilyId == fid)
            {
                return x;
            }
            
        }
        var temp = new Relation();
        temp.Id = map.Count+1;
        temp.FirstFamilyId = fid;
        temp.SecondFamilyId = sid;
        temp.Coefficient = 0;
        if (fid == GameStart.Ins.playerfamily.FamilyID)
            temp.ChageValue(Initlization.Ins.DiplomacyValue);
        map.Add(temp.Id, temp);
        return temp;
    }
}
