﻿using UnityEngine;
using System.Collections;
using System;
using Tools;
public class SingleValuePool : IPool {
    private SingleValuePool() { }
    private static SingleValuePool ins;
    public static SingleValuePool Ins
    {
        get
        {
            if (ins == null)
                ins = new SingleValuePool();
            return ins;
        }
    }

    public override void InPool()
    {
        DateBase.ConnectDate();
        DateBase.Query("select * from singlevalue");
        while (DateBase.mdr.Read())
        {
            map.Add(DateBase.mdr[1], new SingleValue(DateBase.mdr[0],DateBase.mdr[1],DateBase.mdr[2],DateBase.mdr[3]));
        }
        DateBase.CloseConnect();
    }

    public override object ReadPool(int ID)
    {
        return null;
    }
    public SingleValue this[string str]
    {
        get
        {
            return map[str] as SingleValue;
        }
    }
    public bool Countain(string str)
    {
        return map.Contains(str);
    }
}
