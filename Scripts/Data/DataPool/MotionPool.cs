﻿using UnityEngine;
using System.Collections;
using System;
using Tools;
public class MotionPool : IPool
{
    private MotionPool() { }
    private static MotionPool ins;
    public static MotionPool Ins
    {
        get
        {
            if (ins == null)
                ins = new MotionPool();
            return ins;
        }
    }
    public override void InPool()
    {
        DateBase.ConnectDate();
        DateBase.Query("select * from motion");
        while (DateBase.mdr.Read())
        {
            var temp = new Motion();
            temp.ID = (int)DateBase.mdr[0];
            temp.Name = DateBase.mdr[1].ToString();
            temp.Nature = (int)DateBase.mdr[2];
            temp.Value = (int)DateBase.mdr[3];
            string[] strs = DateBase.mdr[4].ToString().Split(',');
            temp.RelaADD = int.Parse(strs[0]);
            temp.RelaSub = int.Parse(strs[1]);
            temp.CreditX=(int)DateBase.mdr[5];
            temp.CreditY = (int)DateBase.mdr[6];
            temp.Score = (int)DateBase.mdr[7];
            temp.Weight = (int)DateBase.mdr[8];
            strs = DateBase.mdr[9].ToString().Split('|');
            for(int i = 0; i < strs.Length; i++)
            {
                temp.callbacks.Add(int.Parse(strs[i]));
            }
            map.Add(temp.ID, temp);
        }
        DateBase.CloseConnect();
    }
    public void UpdatePool()
    {
        for(int i = 1; i < map.Count; i++)
        {
            var m = map[i] as Motion;
            for (int j = 0; j < m.callbacks.Count; j++)
            {
                m.Callbacks.Add(SituationPool.Ins[m.callbacks[j]] as Situation);
            }
        }
    }
    public override object ReadPool(int ID)
    {
        return map[ID];
    }
    public object this[int ID]
    {
        get
        {
            return map[ID];
        }
    }
}
