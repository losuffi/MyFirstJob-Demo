﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public abstract class IPool{
    protected Hashtable map = new Hashtable();
    public abstract void InPool();
    public abstract object ReadPool(int ID);
}
