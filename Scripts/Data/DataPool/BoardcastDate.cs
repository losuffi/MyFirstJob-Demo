﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class BoardMessage
{
    public string Title;
    public int Time;
    public string Content;
    public enum MessageKind
    {
        None,
        Asset,
        Election,
        Senate,
        Trade,
        Job,
        FamRelation,
        Estate,
        War,
    }
    public  MessageKind mKind;
    public object[] exct;
    public BoardMessage(string a,int b,string c,MessageKind t)
    {
        Title = a;
        Time = b;
        Content = c;
        mKind = t;
    }
}
/// <summary>
/// 通知消息的存储池
/// </summary>
public class BoardcastDate {
    private BoardcastDate() { }
    private static BoardcastDate ins;
    public static BoardcastDate Ins
    {
        get
        {
            if (ins == null)
                ins = new BoardcastDate();
            return ins;
        }
    }
    private List<BoardMessage> map = new List<BoardMessage>();
    private Dictionary<string, List<BoardMessage>> others = new Dictionary<string, List<BoardMessage>>();
    public void Rec(string tit,int tim,string con,BoardMessage.MessageKind t,params object[] arges)
    {
        //TODO:接受系统发送过来的信息
        var zt = new BoardMessage(tit, tim, con, t);
        zt.exct = arges;
        map.Add(zt);
    }
    public BoardMessage Get(int i)
    {
        //TODO:显示
        return map[i];
    }
    public int count { get
        {
            return map.Count;
        } }
    public void RecOther(string kind,string title,string c)
    {
        if (!others.ContainsKey(kind))
        {
            others.Add(kind, new List<BoardMessage>());
        }
        others[kind].Add(new BoardMessage(title, others[kind].Count, c, BoardMessage.MessageKind.None));
    }
    public List<BoardMessage> GetOtherLists(string kind)
    {
        if (!others.ContainsKey(kind))
        {
            others.Add(kind, new List<BoardMessage>());
        }
        return others[kind];
    }
}
