﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Tools;
public class FamilyPool : IPool
{
    private static FamilyPool ins;
    private FamilyPool() { }
    public static FamilyPool Ins
    {
        get
        {
            if (ins == null)
                ins = new FamilyPool();
            return ins;
        }
    }
    public override void InPool()
    {
        DateBase.ConnectDate();
        DateBase.Query("select * from family");
        while (DateBase.mdr.Read())
        {
            var family = new Family();
            family.FamilyID = (int)DateBase.mdr[0];
            family.FamilyName = DateBase.mdr[1].ToString();
            family.FamilyScore = (int)DateBase.mdr[2];
            var temp = new FamilyStaff(family);
            temp.Init(DateBase.mdr[3].ToString(), DateBase.mdr[4].ToString());
            family.FStaff = temp;
            family.FDiplomacy = new FamilyDiplomacy(family);
            map.Add(family.FamilyID, family);
        }
        DateBase.ResetConnect();
        DateBase.Query("select * from familyasset");
        while (DateBase.mdr.Read())
        {
            var familyasset = new FamilyAsset(DateBase.mdr);
            var id = (int)DateBase.mdr[1];
            Family temp = map[id] as Family;
            temp.FAsset = familyasset;
        }
        DateBase.CloseConnect();
    }

    public override object ReadPool(int ID)
    {
        var x = map[ID] as Family;
        if (x!=null&&x.FAsset == null)
        {
            x.FAsset = new FamilyAsset();
            x.FAsset.Cash = 800;
            x.FAsset.CreditX = 20;
            x.FAsset.CreditY = 0;
            x.FAsset.Slave = 0;
        }
        return map[ID];
    }
    public int count
    {
        get
        {
            return map.Count;
        }
    }
}
