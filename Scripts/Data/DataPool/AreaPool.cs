﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using MySql.Data.MySqlClient;
using Tools;
public class AreaPool : IPool
{
    private static AreaPool ins;
    private AreaPool() { }
    public static AreaPool Ins
    {
        get
        {
            if (ins == null)
                ins = new AreaPool();
            return ins;
        }
    }
    /// <summary>
    /// 每一种数据池，都有其对应不同的入池方法，当一种数据的模型改变
    /// ，相应的池类方法也需要改变——适配器模式。
    /// </summary>
    public override void InPool()
    {
        DateBase.ConnectDate();
        DateBase.Query("select * from area");
        while (DateBase.mdr.Read())
        {
            var temp = new Area();
            temp.AreaID = (int)DateBase.mdr[0];
            temp.AreaName = DateBase.mdr[1].ToString();
            temp.Specialty = (int)DateBase.mdr[2];
            temp.SuitableId = (int)DateBase.mdr[3];
            temp.Tax = (int)DateBase.mdr[4];
            temp.TaxTotal = 0;
            temp.Subjection = (int)DateBase.mdr[5];
            temp.Distance = (int)DateBase.mdr[6];
            temp.AreaValue = (int)DateBase.mdr[7];
            temp.AreaTaxPercent = (double)DateBase.mdr[8];
            temp.AreaCropPercent = (double)DateBase.mdr[9];
            temp.pos_X = (int)DateBase.mdr[10];
            temp.pos_Y = (int)DateBase.mdr[11];
            string[] strs = DateBase.mdr[13].ToString().Split('|');
            int ArmyKind, ArmyCount;
            ArmyKind = int.Parse(strs[0]);
            ArmyCount = int.Parse(strs[1]);
            temp.armys = new List<Army>();
            for (int i = 0; i < ArmyCount; i++)
            {
                var army = ArmyPool.Ins.CreatArmy(ArmyKind);
                army.OwnerArea = temp;
                army.CurrentArea = temp;
                army.ArmyName += i;
                temp.armys.Add(army);
            }
            if (temp.Subjection < 1)
            {
                temp.country = null;
            }
            else if (temp.Subjection == 1)
            {
                temp.country = new Country();
                temp.country.Cash = (int)DateBase.mdr[12];
                //temp.CD = (int)DateBase.mdr[14];
                temp.country.Rice = (int)DateBase.mdr[15];
                temp.country.CountryName = temp.AreaName;
                temp.country.PersonCount = (int)DateBase.mdr[16];
                temp.country.CountryValue = (int)DateBase.mdr[17];

            }
            else
            {
                temp.country = new Country();
                temp.country.Cash = (int)DateBase.mdr[12];
                temp.country.CountryName = temp.AreaName;
                temp.Umpire = new Person(true, null, temp);
                temp.MinisterA = new Person(true, null, temp);
                temp.MinisterB = new Person(true, null, temp);
                temp.country.CountryValue = (int)DateBase.mdr[17];
            }
            temp.CD = (int)DateBase.mdr[14];
            temp.IsBattleArea = false;
            temp.IsTheFalledArea = false;
            temp.Laws = new List<ALaw>();
            map.Add(temp.AreaID, temp);
        }
    }
    public int Count { get
        {
            return map.Count;
        } }
    public override object ReadPool(int id)
    {
        return map[id];
    }
    public Area Capital()
    {
        return map[26] as Area;
    }
}
