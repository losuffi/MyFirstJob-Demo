﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Tools;
public class ArguePool : IPool
{
    private List<Argue> a, b, c;
    private static ArguePool ins;
    private ArguePool() { }
    public static ArguePool Ins { get { if (ins == null) ins = new ArguePool(); return ins; } }
    public override void InPool()
    {
        DateBase.ConnectDate();
        DateBase.Query("select * from argue");
        while (DateBase.mdr.Read())
        {
            var temp = new Argue();
            temp.ID = (int)DateBase.mdr[0];
            temp.Kind = (int)DateBase.mdr[1];
            temp.ValueA = (int)DateBase.mdr[2];
            temp.ValueB = (int)DateBase.mdr[3];
            temp.Dec = DateBase.mdr[4].ToString();
            temp.Cost = (int)DateBase.mdr[5];
            map.Add(temp.ID, temp);
        }
        DateBase.CloseConnect();
    }

    public override object ReadPool(int ID)
    {
        return map[ID];
    }
    public object this[int ID]
    {
        get
        {
            return map[ID];
        }
    }
    public List<Argue> KindArgues(int kind)
    {
        List<Argue> temp = new List<Argue>();
        //foreach(var c in map)
        //{
        //    Debug.Log((c as Argue).Kind);
        //    Argue t = c as Argue;
        //    if (t.Kind == kind)
        //    {
        //        temp.Add(t);
        //    }
        //}
        for(int i = 1; i < map.Count + 1; i++)
        {
            if((map[i] as Argue).Kind == kind)
            {
                temp.Add(map[i] as Argue);
            }
        }
        return temp;
    }
    public int Count { get { return map.Count; } }
    public List<Argue> GetKind(int i)
    {
        switch (i)
        {
            case 1:
                if (a == null)
                    a = KindArgues(i);
                return a;
            case 2:
                if (b == null)
                    b = KindArgues(i);
                return b;
            case 3:
                if (c == null)
                    c = KindArgues(i);
                return c;
            default:
                return null;
        }
    }
}
