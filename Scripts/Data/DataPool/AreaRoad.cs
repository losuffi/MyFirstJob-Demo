﻿using UnityEngine;
using System.Collections;
using Tools;
using System;
using System.Collections.Generic;
public class Point
{
    public int AreaID;
    public int Parent;
    public int Weight;
    public Point(int i,int a,int w)
    {
        AreaID = i;
        Parent = a;
        Weight = w;
    }
}
public class AreaRoad : IPool
{
    private AreaRoad()
    {
        arr = new int[30, 30];
        road = new List<Point>();
        closeset = new HashSet<int>();
        weights = new int[30];
        weights.Initialize();
    }
    private static AreaRoad ins;
    public static AreaRoad Ins { get
        {
            if (ins == null)
                ins = new AreaRoad();
            return ins;
        } }
    private int[,] arr;
    public override void InPool()
    {
        for (int i = 0; i < arr.GetLength(0); i++)
        {
            for (int j = 0; j < arr.GetLength(1); j++)
            {
                arr[i, j] = 1 << 30;
            }
        }
        DateBase.ConnectDate();
        DateBase.Query("select * from pointroad");
        while (DateBase.mdr.Read())
        {
            int f = (int)DateBase.mdr[1];
            int s = (int)DateBase.mdr[2];
            arr[f, s] = (int)DateBase.mdr[3]; //weight
        }
        DateBase.CloseConnect();
    }

    public override object ReadPool(int ID)
    {
        return arr[ID,0];
    }
    HashSet<int> closeset;
    List<Point> road;
    int[] weights;
    public List<Point> GetRoad(int a, int b)
    {
        if (a == b)
            return null;
        closeset.Clear();
        road.Clear();
        road.Add(new Point(a, -1,0));
        for (int i = 0; i < arr.GetLength(1); i++)
        {
            weights[i] = 1 << 30;
        }
        //closeset.Add(a);
        weights[a] = 0;
        FindOpenSet(a);
        Point tem = road.Find(ar => ar.AreaID == b);
        List<Point> temp = new List<Point>();
        temp.Add(tem);
        while (tem!=null&&tem.Parent != a)
        { 
            if (road.Find(ar => ar.AreaID == tem.Parent) != null)
            {
                tem = road.Find(ar => ar.AreaID == tem.Parent);
                temp.Add(tem);
            }
            else
            {
                temp.Add(new Point(tem.Parent, a, arr[a, tem.Parent]));
                break;
            }
        }
        temp.Reverse();
        return temp;
    }
    public int GetRoadDistance(int a,int b)
    {
        int Dis = 0;
        closeset.Clear();
        for (int i = 0; i < arr.GetLength(1); i++)
        {
            weights[i] = arr[a, i];
        }
        closeset.Add(a);
        weights[a] = 0;
        int start = a;
        int min = 1 << 30;
        for (int i = 1; i < arr.GetLength(1); i++)
        {
            min = 1 << 30;
            for (int j = 0; j < arr.GetLength(1); j++)
            {
                if (!closeset.Contains(j) && weights[j] < min)
                {
                    min = weights[j];
                    start = j;
                }
            }
            closeset.Add(start);
            for (int j = 0; j < arr.GetLength(1); j++)
            {
                int temp = arr[start, j] == 1 << 30 ? 1 << 30 : min + arr[start, j];
                if (!closeset.Contains(j) && temp < weights[j])
                {
                    weights[j] = temp;
                }
            }
        }
        Dis = weights[b];
        return Dis;
    }
    void FindOpenSet(int start)
    {
        int k = start;
        int min=1<<30;
        for (int i = 1; i < arr.GetLength(1); i++)
        {
            min = 1 << 30;
            for (int j=0;j<arr.GetLength(1); j++)
            {
                if (!closeset.Contains(j) && weights[j] < min)
                {
                    min = weights[j];
                    k = j;
                }
            }
            closeset.Add(k);
            for (int j = 0; j < arr.GetLength(1); j++)
            {
                int temp = arr[k, j] == 1 << 30 ? 1 << 30 : min + arr[k, j];
                if (!closeset.Contains(j) && temp < weights[j])
                {
                    weights[j] = temp;
                    var tem = road.Find(ar => ar.AreaID == j);
                    if (tem != null)
                    {
                        tem.Parent = k;
                        tem.Weight = arr[k, j];
                    }
                    else
                    {
                        road.Add(new Point(j, k, arr[k, j]));
                    }
                }
            }
        }
    }
}
