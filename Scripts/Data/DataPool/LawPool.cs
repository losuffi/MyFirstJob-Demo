﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using Tools;
public class LawPool : IPool
{
    private LawPool() { }
    private static LawPool ins;
    private List<Area> ares;
    private List<ALaw> laws;
    public static LawPool Ins
    {
        get
        {
            if (ins == null)
                ins = new LawPool();
            return ins;
        }
    }
    public override void InPool()
    {
        DateBase.ConnectDate();
        ares = new List<Area>();
        laws = new List<ALaw>();
        GetProvinceArea();
        GetLaws();
        for(int z = 0; z < ares.Count; z++) {
            var area = ares[z];
            BindLaw(area);
        }
    }

    public override object ReadPool(int ID)
    {
        return map[ID];
    }
    public ALaw this[int index]
    {
        get
        {
            return map[index] as ALaw;
        }
    }
    public int Count {
        get
        {
            return map.Count;
        }
    }
    void GetProvinceArea()
    {
        ares.Clear();
        for (int j = 0; j < AreaPool.Ins.Count; j++)
        {
            var a = AreaPool.Ins.ReadPool(j + 1) as Area;
            if (a.Subjection < 2)
            {
                ares.Add(a);
            }
        }
    }
    /// <summary>
    /// TODO：增加消耗属性
    /// </summary>
    void GetLaws()
    {
        DateBase.ConnectDate();
        DateBase.Query("select * from Law");
        while (DateBase.mdr.Read())
        {
            var law = new ALaw();
            law.ID = (int)DateBase.mdr[0];
            law.Name = DateBase.mdr[1].ToString();
            law.Value = (int)DateBase.mdr[2];
            string[] strs = DateBase.mdr[3].ToString().Split('|');
            law.Sections = new List<int>();
            for (int i = 0; i < strs.Length; i++)
            {
                law.Sections.Add(int.Parse(strs[i]));
            }
            law.CreditXCost = (int)DateBase.mdr[4];
            law.Topic = DateBase.mdr[5].ToString();
            law.Command = (int)DateBase.mdr[6];
            map.Add(law.ID, law);
        }
        DateBase.CloseConnect();
    }
    public List<string> Topics
    {
        get
        {
            List<string> temp = new List<string>();
            for (int i = 0; i < map.Count; i++)
            {
                if (!temp.Contains(this[i + 1].Topic))
                {
                    temp.Add(this[i + 1].Topic);
                }
            }
            return temp;
        }
    }
    public List<ALaw> GetLawsByTopic(string topic)
    {
        List<ALaw> temp = new List<ALaw>();
        for(int i = 0; i < map.Count; i++)
        {
            if (this[i + 1].Topic.Equals(topic))
            {
                temp.Add(this[i + 1]);
            }
        }
        return temp;
    }
    public void BindLaw(Area a)
    {
        laws.Clear();
        for (int i = 0; i < map.Count; i++)
        {
            var lawtem = this[i + 1];
            var law = new ALaw();
            law.Sections = new List<int>();
            law.ID = lawtem.ID;
            law.Name = lawtem.Name;
            law.Value = lawtem.Value;
            law.CreditXCost = lawtem.CreditXCost;
            law.Topic = lawtem.Topic;
            lawtem.Sections.ForEach(t => law.Sections.Add(t));
            law.Command = lawtem.Command;
            laws.Add(law);
        }
        a.Laws.AddRange(laws);
    }
}
