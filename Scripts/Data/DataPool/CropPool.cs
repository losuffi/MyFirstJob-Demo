﻿using UnityEngine;
using System.Collections;
using System;
using Tools;
public class CropPool : IPool
{
    private static CropPool ins;
    private CropPool() { }
    public static CropPool Ins
    {
        get
        {
            if (ins == null)
                ins = new CropPool();
            return ins;
        }
    }
    public override void InPool()
    {
        DateBase.ConnectDate();
        DateBase.Query("select * from crop");
        while (DateBase.mdr.Read())
        {
            var crop = new Crop();
            crop.CropId = (int)DateBase.mdr[0];
            crop.CropName = DateBase.mdr[1].ToString();
            crop.Earning = (int)DateBase.mdr[2];
            crop.Cd = (int)DateBase.mdr[3];
            crop.Month = (int)DateBase.mdr[4];
            crop.Consume = (int)DateBase.mdr[5];
            map.Add(crop.CropId, crop);
        }
        DateBase.CloseConnect();
    }

    public override object ReadPool(int ID)
    {
        return map[ID];
    }
    public int Count { get
        {
            return map.Count;
        } }
}
