﻿using UnityEngine;
using System.Collections;
using System;
using Tools;
using System.Collections.Generic;
using System.Reflection;
public class SituationPool : IPool
{
    private static SituationPool ins;
    private SituationPool() { }
    public static SituationPool Ins { get
        {
            if (ins == null)
                ins = new SituationPool();
            return ins;
        } }
    public override void InPool()
    {
        DateBase.ConnectDate();
        DateBase.Query("select * from situation");
        while (DateBase.mdr.Read())
        {
            var temp = new Situation();
            temp.Sid = (int)DateBase.mdr[0];
            temp.Name = DateBase.mdr[1].ToString();
            temp.motions = ConverMotionByString(DateBase.mdr[3].ToString());
            temp.Kind = (int)DateBase.mdr[4];
            temp.erupt = (sbyte)DateBase.mdr[5];
            temp.Weight = (int)DateBase.mdr[7];
            if(temp.Kind>0||temp.Kind==-3)
                temp.areas = ConverAreaByString(DateBase.mdr[2].ToString());
            string[] strs = DateBase.mdr[6].ToString().Split('|');
            temp.effects = new List<IEffect>();
            for(int i = 0; i < strs.Length; i++)
            {
                if(int.Parse(strs[i])!=0)
                    temp.effects.Add(Assembly.GetExecutingAssembly().CreateInstance("Effect" + int.Parse(strs[i])) as IEffect);
            }
            map.Add(temp.Sid, temp);
        }
        MotionPool.Ins.UpdatePool();
        DateBase.CloseConnect();
    }
    public List<Situation> Autos
    {
        get
        {
            var temp = new List<Situation>();
            for(int i = 1; i < map.Count+1; i++)
            {
                var v = map[i] as Situation;
                if (v.Kind == 1)
                    temp.Add(v);
            }
            return temp;
        }
    }
    public override object ReadPool(int ID)
    {
        Situation temp = new Situation();
        Situation tem = map[ID] as Situation;
        temp.Sid = tem.Sid;
        temp.Name = tem.Name;
        temp.motions = tem.motions;
        temp.Kind = tem.Kind;
        temp.erupt = tem.erupt;
        temp.areas = tem.areas;
        return temp;
    }
    List<Area> ConverAreaByString(string t)
    {
        var temp = new List<Area>();
        string[] strs = t.Split(',');
        for (int i = 0; i < strs.Length; i++)
        {
            int index = int.Parse(strs[i]);
            var area = AreaPool.Ins.ReadPool(index) as Area;
            temp.Add(area);
        }
        return temp;
    }
    List<Motion> ConverMotionByString(string t)
    {
        var temp = new List<Motion>();
        if ((int)DateBase.mdr[4] < 0)
            return temp;
        string[] strs = t.Split(',');
        for(int i = 0; i < strs.Length; i++)
        {
            int index = int.Parse(strs[i]);
            var motion = MotionPool.Ins[index] as Motion;
            temp.Add(motion);
        }
        return temp;
    }
    public object this[int ID]
    {
        get
        {
            Situation temp = new Situation();
            Situation tem = map[ID] as Situation;
            temp.Sid = tem.Sid;
            temp.Name = tem.Name;
            temp.motions = tem.motions;
            temp.Kind = tem.Kind;
            temp.erupt = tem.erupt;
            temp.areas = tem.areas;
            temp.effects = tem.effects;
            temp.Weight = tem.Weight;
            return temp;
        }
    }
    public int Count
    {
        get
        {
            return map.Count;
        }
    }
}
 