﻿using UnityEngine;
using System.Collections;
using System;
using Tools;
/// <summary>
/// 军队数据池
/// </summary>
public class ArmyPool : IPool
{
    private static ArmyPool ins;
    private ArmyPool() { }
    public static ArmyPool Ins
    {
        get
        {
            if (ins == null)
                ins = new ArmyPool();
            return ins;
        }
    }
    public override void InPool()
    {
        DateBase.ConnectDate();
        DateBase.Query("select * from army");
        while (DateBase.mdr.Read())
        {
            Army temp = new Army();
            temp.ArmyID = (int)DateBase.mdr[0];
            temp.ArmyName = DateBase.mdr[1].ToString();
            temp.ArmyMember = (int)DateBase.mdr[2];
            temp.ArmyPassPercent = (double)DateBase.mdr[3];
            temp.ArmyValue = (int)DateBase.mdr[4];
            string[] strs = DateBase.mdr[5].ToString().Split('|');
            temp.ArmyAttiMax = double.Parse(strs[0]);
            temp.ArmyAttiMin = double.Parse(strs[1]);
            temp.ArmyCost = (int)DateBase.mdr[6];
            strs = DateBase.mdr[7].ToString().Split('|');
            temp.ArmyCost2 = int.Parse(strs[0]);
            temp.ArmyCostMonth = int.Parse(strs[1]);
            temp.ArmyWeight = (int)DateBase.mdr[8];
            map.Add(temp.ArmyID, temp);
        }
        DateBase.CloseConnect();
    }

    public override object ReadPool(int ID)
    {
        return map[ID];
    }
    public Army CreatArmy(int ID)
    {
        var templete = map[ID] as Army;
        Army c = new Army();
        c.ArmyID = templete.ArmyID;
        c.ArmyName = templete.ArmyName;
        c.ArmyMember = templete.ArmyMember;
        c.ArmyPassPercent = templete.ArmyPassPercent;
        c.ArmyValue = templete.ArmyValue;
        c.ArmyAttiMax = templete.ArmyAttiMax;
        c.ArmyAttiMin = templete.ArmyAttiMin;
        c.ArmyCost = templete.ArmyCost;
        c.ArmyCost2 = templete.ArmyCost2;
        c.ArmyCostMonth = templete.ArmyCostMonth;
        c.State =  Army.states.Stay;
        c.ArmyWeight = templete.ArmyWeight;
        c.WorkTime = 0;
        return c;
    }
}
