﻿using UnityEngine;
using System.Collections;
using System;
using Tools;
public class SpecialtyPool : IPool {

    private static SpecialtyPool ins;
    private SpecialtyPool() { }
    public static SpecialtyPool Ins
    {
        get
        {
            if (ins == null)
                ins = new SpecialtyPool();
            return ins;
        }
    }

    public override void InPool()
    {
        DateBase.ConnectDate();
        DateBase.Query("select * from specialty");
        while (DateBase.mdr.Read())
        {
            var temp = new Specialty();
            temp.SpecialtyId = (int)DateBase.mdr[0];
            temp.SpecialtyName = DateBase.mdr[1].ToString();
            temp.SpecialtyArea = (int)DateBase.mdr[2];
            string[] strs = DateBase.mdr[3].ToString().Split('|');
            temp.SpecialtyValueMax = int.Parse(strs[1]);
            temp.SpecialtyValueMin = int.Parse(strs[0]);
            temp.SpecialtyValueReal = UnityEngine.Random.Range(temp.SpecialtyValueMin, temp.SpecialtyValueMax + 1);
            temp.SpecialtyRnd = (int)DateBase.mdr[4];
            map.Add(temp.SpecialtyId, temp);
        }
    }

    public override object ReadPool(int ID)
    {
        return map[ID];
    }
    public int Count { get
        {
            return map.Count;
        } }
}
