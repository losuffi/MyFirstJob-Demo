﻿using UnityEngine;
using System.Collections;
using MySql.Data;
using System.Data;
using MySql.Data.MySqlClient;
using LitJson;
using System.IO;
using System.Collections.Generic;
namespace Tools{
    public class DateBase
    {
        private static MySqlConnection conn;
        private static DateBase _ins;
        private DateBase()
        {
        }
        public static MySqlCommand cmd { get; set; }
        public static MySqlDataReader mdr { get; set; }
        public static DateBase Ins
        {
            get
            {
                if (_ins == null)
                    _ins = new DateBase();
                return _ins;
            }
        }
        public static void ConnectDate()
        {
            string connstr= string.Format("server={0};user={1};database={2};port={3};password={4}", "localhost", "root", "test1", 3306, 1234);
            conn = new MySqlConnection(connstr);
            conn.Open();
        }
        public static string ConnStr
        {
            get
            {
                return string.Format("server={0};user={1};database={2};port={3};password={4}", "localhost", "root", "test1", 3306, 1234);
            }
        }
        public static void Query(string sql)
        {
            cmd = new MySqlCommand(sql, conn);
            mdr = cmd.ExecuteReader();
        }
        public static void CloseConnect()
        {
            conn.Close();
        }
        public static void ResetConnect()
        {
            conn.Close();
            ConnectDate();
        }
    }
    public class DateJson<T>
    {
        public static T ReadDate(string path)
        {
            T res = JsonMapper.ToObject<T>(File.ReadAllText(path));
            return res;
        }
        //List<Family> AS = JsonMapper.ToObject<List<Family>>(File.ReadAllText(Application.dataPath));
    }
}
