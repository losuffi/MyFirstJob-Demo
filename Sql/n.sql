-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: test1
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `AreaId` int(11) NOT NULL AUTO_INCREMENT,
  `AreaName` varchar(45) DEFAULT NULL,
  `GoodsId` int(11) DEFAULT NULL,
  `AreaFarmId` int(11) DEFAULT NULL,
  `Revenue` int(11) DEFAULT NULL,
  `AreaKind` int(11) DEFAULT NULL,
  `AreaDistance` int(11) DEFAULT NULL,
  `AreaValue` int(11) DEFAULT NULL,
  `AreaRevenuePercent` double DEFAULT NULL,
  `AreaCropPercent` double DEFAULT NULL,
  `AreaPosX` int(11) DEFAULT NULL,
  `AreaPosY` int(11) DEFAULT NULL,
  `AreaCountryCash` int(11) DEFAULT '0',
  `AreaArmy` varchar(45) DEFAULT '0',
  `AreaCD` int(11) DEFAULT '0',
  `AreaCountryRice` int(11) DEFAULT '0',
  `AreaCountryPersonCount` int(11) DEFAULT '0',
  `AreaCountryValue` int(11) DEFAULT '5',
  PRIMARY KEY (`AreaId`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,'不列颠尼亚',0,0,200,4,0,200,0,1.2,0,0,15000,'3|2',12,0,0,4),(2,'撒丁尼亚',0,4,200,0,1,200,0.2,1.2,1,0,0,'1|1',12,0,0,0),(3,'西西里',0,4,200,0,1,200,0.2,1.2,0,1,0,'1|1',12,0,0,0),(4,'西班牙',1,2,350,0,1,200,0.2,1.2,-1,0,0,'1|1',12,0,0,0),(5,'毛里塔尼亚',0,1,200,2,1,200,0,1.2,0,-1,28000,'2|1',12,0,0,4),(6,'阿非利加',5,1,600,0,2,200,0.2,1.2,2,0,0,'1|1',12,0,0,0),(7,'昔兰尼',0,3,200,2,2,200,0,1.2,-2,0,35000,'2|1',12,0,0,4),(8,'埃及',3,3,1000,2,3,200,0,1.2,3,0,90000,'2|4',12,0,0,9),(9,'犹太',0,1,200,3,3,200,0,1.2,1,2,64000,'2|2',12,0,0,6),(10,'叙利亚',2,2,800,2,3,200,0,1.2,2,1,75000,'2|3',12,0,0,7),(11,'帕提亚',6,0,1000,4,6,200,0,1.2,3,3,100000,'2|5',12,0,0,10),(12,'亚美尼亚',0,3,600,4,6,200,0,1.2,0,3,60000,'2|3',12,0,0,8),(13,'奇里乞亚',0,3,200,4,6,400,0,1.2,0,3,10000,'4|2',12,0,0,3),(14,'本都',0,3,800,3,6,400,0,1.2,0,3,64000,'2|4',12,0,0,8),(15,'加拉太',0,3,800,3,6,400,0,1.2,0,3,60000,'3|2',12,0,0,6),(16,'帕加马',4,3,1000,2,6,400,0,1.2,0,3,64000,'2|3',12,0,0,6),(17,'希腊',0,3,1000,0,6,400,0.2,1.2,0,3,64000,'1|1',12,0,0,3),(18,'马其顿',0,3,800,0,6,40,0.2,1.2,0,3,0,'1|1',12,0,0,0),(19,'色雷斯',0,3,200,3,6,40,0,1.2,0,3,16000,'3|2',12,0,0,5),(20,'达契亚',0,3,400,3,6,40,0,1.2,0,3,32000,'3|3',12,0,0,7),(21,'雷诺潘',0,3,300,4,6,40,0,1.2,0,3,36000,'3|3',12,0,0,3),(22,'日耳曼',0,3,400,4,6,40,0,1.2,0,3,15000,'3|2',12,0,0,2),(23,'高卢',0,3,400,3,6,40,0,1.2,0,3,30000,'3|3',12,0,0,2),(24,'纳尔旁',0,3,400,0,6,40,0.2,1.2,0,3,0,'1|1',12,0,0,0),(25,'山南高卢',0,3,400,0,6,40,0.2,1.2,0,3,0,'1|1',12,0,0,2),(26,'罗马',0,3,0,1,6,40,0,1.2,0,3,80000,'1|4',0,40000,10000,0),(27,'伊利里亚',0,3,200,3,6,40,0,1.2,0,3,12000,'4|2',12,0,0,1),(28,'梅西亚',0,3,400,3,6,40,0,1.2,0,3,0,'3|2',12,0,0,3);
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `argue`
--

DROP TABLE IF EXISTS `argue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `argue` (
  `ArgueID` int(11) NOT NULL AUTO_INCREMENT,
  `ArgueKind` int(11) DEFAULT NULL,
  `ArgueValueA` int(11) DEFAULT NULL,
  `ArgueValueB` int(11) DEFAULT NULL,
  `ArgueDec` varchar(45) DEFAULT NULL,
  `ArgueCost` int(11) DEFAULT NULL,
  PRIMARY KEY (`ArgueID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `argue`
--

LOCK TABLES `argue` WRITE;
/*!40000 ALTER TABLE `argue` DISABLE KEYS */;
INSERT INTO `argue` VALUES (1,1,5,16,'颇有气势的定论',2),(2,2,14,16,'诙谐的对比',2),(3,3,18,3,'细致的分析',2),(4,1,10,33,'郑重的承诺',3),(5,2,28,25,'微妙的讽刺',3),(6,3,36,12,'厉害的阐述',3),(7,1,25,47,'引发共鸣的赞叹',4),(8,2,42,45,'充分的法律诠释',5),(9,3,42,20,'令人深思的实例',4),(10,1,1,66,'深度洗脑',5),(11,3,52,5,'锱铢必较',5);
/*!40000 ALTER TABLE `argue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `army`
--

DROP TABLE IF EXISTS `army`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `army` (
  `ArmyId` int(11) NOT NULL AUTO_INCREMENT,
  `ArmyName` varchar(45) DEFAULT NULL,
  `ArmyMember` int(11) NOT NULL,
  `ArmyPercent` double DEFAULT NULL,
  `ArmyValue` int(11) NOT NULL,
  `ArmyAttiPercent` varchar(45) NOT NULL,
  `ArmyCost` int(11) DEFAULT NULL,
  `ArmyMonthCost` varchar(45) DEFAULT NULL,
  `ArmyWeight` int(11) DEFAULT NULL,
  PRIMARY KEY (`ArmyId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `army`
--

LOCK TABLES `army` WRITE;
/*!40000 ALTER TABLE `army` DISABLE KEYS */;
INSERT INTO `army` VALUES (1,'罗马军团',6000,0.9,70,'0.85|1.2',0,'6000|12',2),(2,'希腊式军队',10000,0.7,55,'0.6|1',4000,'0|0',5),(3,'蛮族军队',10000,0.6,40,'0.4|0.95',3400,'0|0',7),(4,'奴隶海盗',3000,0.35,26,'0.2|0.7',1200,'0|0',9);
/*!40000 ALTER TABLE `army` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crop`
--

DROP TABLE IF EXISTS `crop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crop` (
  `CropId` int(11) NOT NULL AUTO_INCREMENT,
  `CropName` varchar(45) NOT NULL,
  `CropEarning` int(11) DEFAULT NULL,
  `CropCD` int(11) DEFAULT NULL,
  `CropMonth` int(11) DEFAULT NULL,
  `CropConsume` int(11) DEFAULT NULL,
  PRIMARY KEY (`CropId`),
  UNIQUE KEY `CropName_UNIQUE` (`CropName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crop`
--

LOCK TABLES `crop` WRITE;
/*!40000 ALTER TABLE `crop` DISABLE KEYS */;
INSERT INTO `crop` VALUES (1,'小麦',45,4,7,25),(2,'牧场',70,6,7,40),(3,'橄榄园',200,12,7,80),(4,'葡萄园',180,8,7,60);
/*!40000 ALTER TABLE `crop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `effection`
--

DROP TABLE IF EXISTS `effection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `effection` (
  `idEffection` int(11) NOT NULL AUTO_INCREMENT,
  `EffectionName` varchar(45) DEFAULT NULL,
  `EffectionDec` text,
  PRIMARY KEY (`idEffection`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `effection`
--

LOCK TABLES `effection` WRITE;
/*!40000 ALTER TABLE `effection` DISABLE KEYS */;
INSERT INTO `effection` VALUES (1,'皇嗣纷争','B、C各使用国库40%的资金组建军队，在本国领土上开始战争。国家常备军队不动。战争中率先损失到达总兵力30%的一方，下一回合向罗马求援，即出现事件【请求调停】'),(2,'无法贸易','本地区无法贸易，显示：点击此地域，贸易信息显示文本：动乱中，无法进行贸易活动。正在前往此地的跑商过途径此地的成员返回罗马'),(3,'路障','事件发生时，其他跑商、军队无法作为路径之一通过（军队可成为终点到达）'),(4,'叛乱','A使用国库50%的资金组建军队+常备军队，反叛者使用国库30%的资金组建军队。在本国领土上开始战争。          战争中率先损失到达总兵力30%的一方，下一回合向罗马求援，即出现事件【请求调停】');
/*!40000 ALTER TABLE `effection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `family`
--

DROP TABLE IF EXISTS `family`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `family` (
  `FamilyId` int(11) NOT NULL AUTO_INCREMENT,
  `FamilyName` varchar(45) DEFAULT NULL,
  `FamilyScore` int(11) NOT NULL,
  `FamilyMember` varchar(45) DEFAULT NULL,
  `FamilyNature` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`FamilyId`),
  UNIQUE KEY `FamilyName_UNIQUE` (`FamilyName`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='家族树总数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `family`
--

LOCK TABLES `family` WRITE;
/*!40000 ALTER TABLE `family` DISABLE KEYS */;
INSERT INTO `family` VALUES (1,'Gamer',0,'5|5','3|1|2|4'),(2,'埃米利乌斯',0,'8|15','4|3|2|1'),(3,'费边',0,'8|30','2|1|4|3'),(4,'瓦莱里乌斯',0,'5|20','1|3|2|4'),(5,'克劳狄',0,'8|12','2|3|4|1'),(6,'曼利',0,'9|15','3|3|3|1'),(7,'李锡尼',0,'6|10','3|3|2|2'),(8,'尤里乌斯',0,'4|8','2|2|2|4'),(9,'科尔涅利乌斯',0,'12|25','5|3|1|1');
/*!40000 ALTER TABLE `family` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familyasset`
--

DROP TABLE IF EXISTS `familyasset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familyasset` (
  `FamilyAssetID` int(11) NOT NULL AUTO_INCREMENT,
  `FamilyID` int(11) DEFAULT NULL,
  `FamilyCash` int(11) DEFAULT NULL,
  `FamilySlave` int(11) DEFAULT NULL,
  `FamilyCreditX` int(11) DEFAULT NULL,
  `FamilyCreditY` int(11) DEFAULT NULL,
  PRIMARY KEY (`FamilyAssetID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familyasset`
--

LOCK TABLES `familyasset` WRITE;
/*!40000 ALTER TABLE `familyasset` DISABLE KEYS */;
INSERT INTO `familyasset` VALUES (1,1,8000,0,20,0),(2,2,800,0,30,0),(3,3,800,0,40,0),(4,4,800,0,10,0),(5,5,800,0,25,0),(6,6,800,0,30,0),(7,7,800,0,10,0),(8,8,800,0,20,0),(9,9,800,0,40,0);
/*!40000 ALTER TABLE `familyasset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familydiplomacy`
--

DROP TABLE IF EXISTS `familydiplomacy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familydiplomacy` (
  `DiplomacyID` int(11) NOT NULL AUTO_INCREMENT,
  `FamilyID` int(11) NOT NULL,
  `Value` int(11) NOT NULL,
  PRIMARY KEY (`DiplomacyID`),
  UNIQUE KEY `FamilyID_UNIQUE` (`FamilyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familydiplomacy`
--

LOCK TABLES `familydiplomacy` WRITE;
/*!40000 ALTER TABLE `familydiplomacy` DISABLE KEYS */;
/*!40000 ALTER TABLE `familydiplomacy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familystaff`
--

DROP TABLE IF EXISTS `familystaff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familystaff` (
  `StaffID` int(11) NOT NULL AUTO_INCREMENT,
  `FamilyID` int(11) NOT NULL,
  PRIMARY KEY (`StaffID`),
  UNIQUE KEY `FamilyID_UNIQUE` (`FamilyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familystaff`
--

LOCK TABLES `familystaff` WRITE;
/*!40000 ALTER TABLE `familystaff` DISABLE KEYS */;
/*!40000 ALTER TABLE `familystaff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familytechnology`
--

DROP TABLE IF EXISTS `familytechnology`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familytechnology` (
  `TechnologyID` int(11) NOT NULL AUTO_INCREMENT,
  `FamilyID` int(11) NOT NULL,
  PRIMARY KEY (`TechnologyID`),
  UNIQUE KEY `FamilyID_UNIQUE` (`FamilyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familytechnology`
--

LOCK TABLES `familytechnology` WRITE;
/*!40000 ALTER TABLE `familytechnology` DISABLE KEYS */;
/*!40000 ALTER TABLE `familytechnology` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gameeventbackpro`
--

DROP TABLE IF EXISTS `gameeventbackpro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gameeventbackpro` (
  `GameEventBackProID` int(11) NOT NULL AUTO_INCREMENT,
  `GameEventBackProStart` int(11) DEFAULT NULL,
  `GameEventBackProMot` int(11) DEFAULT NULL,
  `GameEventBackProRes` int(11) DEFAULT NULL,
  `GameEventBackProValue` double DEFAULT NULL,
  `GameEventBackProDyRes` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`GameEventBackProID`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COMMENT='决策转向表。起因情景ID| 提案ID | 导向情景ID（为-1为动态导向，为0为事件群终结） |  导向概率 | 动态导向情景ID组 |';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gameeventbackpro`
--

LOCK TABLES `gameeventbackpro` WRITE;
/*!40000 ALTER TABLE `gameeventbackpro` DISABLE KEYS */;
INSERT INTO `gameeventbackpro` VALUES (1,6,0,0,1,'0'),(2,6,1,0,1,'0'),(3,6,20,13,1,'0'),(4,13,0,17,1,'0'),(5,13,3,-2,1,'16|0|0'),(6,13,4,-1,1,'16|16|17|17|0'),(7,13,2,-1,1,'16|16|17|17|0'),(8,14,0,17,1,'0'),(9,14,2,-1,1,'16|16|17|17|0'),(10,14,21,17,1,'0'),(11,15,0,17,1,'0'),(12,15,21,17,1,'0'),(13,15,2,-1,1,'16|16|17|17|0'),(14,12,0,17,1,'0'),(15,12,11,-2,1,'16|0|0'),(16,12,10,17,1,'0'),(17,1,1,0,1,'0'),(18,1,0,0,1,'0'),(19,1,16,-3,0,'0|0|0'),(20,9,0,0,1,'0'),(21,9,1,0,1,'0'),(22,9,4,-1,1,'0|0|0|0|0'),(23,9,2,-1,1,'0|0|0|0|0'),(24,11,0,0,1,'0'),(25,11,1,0,1,'0'),(26,11,4,-1,1,'0|0|0|0|0'),(27,11,12,-1,1,'0|0|0|0|0'),(28,11,23,-1,1,'0|0|0|0|0'),(29,18,0,0,1,'0'),(30,18,11,-1,1,'0|0|0|0|0'),(31,18,10,0,1,'0'),(32,2,0,0,1,'0'),(33,2,1,0,1,'0'),(34,2,20,13,1,'0'),(35,3,0,0,1,'0'),(36,3,1,0,1,'0'),(37,3,3,-2,1,'16|0|0'),(38,3,24,-2,1,'16|19|19'),(39,19,0,0,1,'0'),(40,19,1,0,1,'0'),(41,19,2,-4,1,'17|17|20|20|0'),(42,19,4,-4,1,'17|17|20|20|0'),(43,4,0,0,1,'0'),(44,4,3,-2,1,'16|0|0'),(45,4,23,-4,1,'17|17|20|20|0'),(46,20,7,-5,1,'0'),(47,20,9,16,1,'0'),(48,10,0,0,1,'0'),(49,10,1,0,1,'0'),(50,10,4,-1,1,'0|0|0|0|0'),(51,10,12,-1,1,'0|0|0|0|0'),(52,10,23,-1,1,'0|0|0|0|0'),(53,20,0,20,1,'0');
/*!40000 ALTER TABLE `gameeventbackpro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `law`
--

DROP TABLE IF EXISTS `law`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `law` (
  `idLaw` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  `Value` int(11) DEFAULT NULL,
  `Sections` varchar(60) DEFAULT NULL,
  `CreditXCost` int(11) DEFAULT NULL,
  `Topic` varchar(45) DEFAULT NULL,
  `Command` int(11) DEFAULT '0',
  PRIMARY KEY (`idLaw`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `law`
--

LOCK TABLES `law` WRITE;
/*!40000 ALTER TABLE `law` DISABLE KEYS */;
INSERT INTO `law` VALUES (1,'行省常驻军团数',1,'1|2|3|4',10,'安全法',0),(2,'军团军饷',3000,'2000|3000|5000',10,'安全法',0),(3,'罗马安置军团数',4,'4|5|6',10,'安全法',0),(4,'行省税收率',10,'10|20|30',10,'国土法',0),(5,'征伐',0,'0',10,'主动宣战',8),(6,'出兵',0,'0',10,'主动宣战',1),(7,'行省驻军',0,'0',10,'主动宣战',4);
/*!40000 ALTER TABLE `law` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `motion`
--

DROP TABLE IF EXISTS `motion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `motion` (
  `MotionID` int(11) NOT NULL AUTO_INCREMENT,
  `motionName` varchar(45) NOT NULL,
  `motionNature` int(11) DEFAULT NULL,
  `motionValue` int(11) DEFAULT NULL,
  `motionRela` varchar(45) DEFAULT NULL,
  `motionCreditX` int(11) DEFAULT NULL,
  `motionCreditY` int(11) DEFAULT NULL,
  `motionScore` int(11) DEFAULT NULL,
  `motionWeight` int(11) DEFAULT NULL,
  `motionCallBack` varchar(45) DEFAULT '1|2|3',
  PRIMARY KEY (`MotionID`),
  UNIQUE KEY `motionName_UNIQUE` (`motionName`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `motion`
--

LOCK TABLES `motion` WRITE;
/*!40000 ALTER TABLE `motion` DISABLE KEYS */;
INSERT INTO `motion` VALUES (1,'不予干涉',0,30,'5,0',10,0,2,0,'1|2|3'),(2,'出兵',0,30,'5,-5',10,-5,10,22,'1|2|3'),(3,'使者调停',1,40,'10,-10',-5,5,5,15,'1|2|3'),(4,'临近驻军干涉',4,40,'20,-5',5,10,5,18,'1|2|3'),(5,'宣战',0,30,'0,-20',10,-5,10,20,'1|2|3'),(6,'人事任命',1,40,'0,-10',15,15,5,14,'1|2|3'),(7,'建立行省',0,45,'10,0',20,0,10,4,'1|2|3'),(8,'交由占卜决定',0,50,'2,2',10,0,0,1,'1|2|3'),(9,'维持自治',0,60,'15,0',5,0,10,2,'1|2|3'),(10,'拒绝',0,35,'0,-5',10,5,5,3,'1|2|3'),(11,'准许',1,35,'5,0',10,-5,5,7,'1|2|3'),(12,'要求同盟出兵',7,42,'5,0',4,-2,0,8,'1|2|3'),(13,'要求人质',1,0,'0,0',0,0,0,9,'1|2|3'),(14,'要求赔偿',0,0,'0,0',0,0,0,10,'1|2|3'),(15,'小麦补偿',3,60,'5,0',-5,15,5,11,'1|2|3'),(16,'金钱补偿',5,60,'15,-10',-15,30,5,12,'1|2|3'),(17,'人员补偿',0,60,'10,0',10,-15,5,13,'1|2|3'),(18,'权限任命',1,0,'0,0',0,0,0,24,'1|2|3'),(19,'同意提案',0,1,'3,3',5,5,2,21,'1|2|3'),(20,'派系支持',6,45,'5,-5',10,0,2,6,'1|2|3'),(21,'隐忍',0,20,'0,-15',0,-20,2,5,'1|2|3'),(22,'再战',1,35,'10,-15',-10,30,2,17,'1|2|3'),(23,'征伐',8,20,'5,-15',-2,30,10,23,'1|2|3'),(24,'总督特使',4,35,'10,-4',5,5,3,19,'1|2|3');
/*!40000 ALTER TABLE `motion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `PersonId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) NOT NULL,
  `Force` int(11) NOT NULL,
  `Brain` int(11) NOT NULL,
  `Charm` int(11) NOT NULL,
  `Age` int(11) NOT NULL,
  `Idle` int(11) NOT NULL,
  `FamilyID` int(11) NOT NULL,
  PRIMARY KEY (`PersonId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pointroad`
--

DROP TABLE IF EXISTS `pointroad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pointroad` (
  `idPointRoad` int(11) NOT NULL AUTO_INCREMENT,
  `PointRoadFirst` int(11) DEFAULT NULL,
  `PointRoadSecond` int(11) DEFAULT NULL,
  `PointRoadWeight` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPointRoad`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pointroad`
--

LOCK TABLES `pointroad` WRITE;
/*!40000 ALTER TABLE `pointroad` DISABLE KEYS */;
INSERT INTO `pointroad` VALUES (1,1,23,1),(2,2,24,1),(3,2,25,1),(4,2,26,1),(5,2,3,1),(6,3,2,1),(7,3,26,1),(8,3,6,1),(9,3,7,2),(10,4,5,1),(11,4,23,2),(12,4,24,1),(13,5,4,1),(14,5,6,1),(15,6,3,1),(16,6,5,1),(17,6,7,2),(18,7,3,2),(19,7,6,2),(20,7,8,1),(21,8,7,1),(22,8,9,1),(23,8,10,2),(24,8,13,2),(25,8,16,2),(26,9,8,1),(27,9,10,1),(28,9,16,2),(29,10,9,1),(30,10,13,1),(31,10,16,2),(32,10,17,2),(33,11,12,1),(34,11,10,1),(35,12,11,1),(36,12,14,1),(37,13,10,1),(38,13,16,1),(39,13,17,2),(40,14,12,1),(41,14,15,1),(42,14,19,2),(43,15,14,1),(44,15,16,1),(45,16,17,1),(46,16,13,1),(47,16,8,2),(48,16,9,2),(49,16,10,2),(50,16,15,1),(51,16,19,1),(52,16,18,1),(53,17,26,2),(54,17,18,1),(55,17,16,1),(56,17,10,2),(57,17,8,3),(58,18,26,1),(59,18,27,1),(60,18,28,1),(61,18,19,1),(62,18,16,1),(63,19,18,1),(64,19,28,1),(65,19,16,1),(66,19,14,2),(67,20,28,1),(68,20,21,1),(69,21,27,1),(70,21,20,1),(71,21,25,2),(72,21,23,1),(73,22,23,1),(74,23,1,1),(75,23,22,1),(76,23,4,2),(77,23,24,1),(78,23,21,1),(79,24,4,1),(80,24,23,1),(81,24,25,2),(82,24,2,1),(83,24,26,1),(84,25,24,2),(85,25,21,2),(86,25,27,1),(87,25,26,1),(88,25,2,1),(89,26,3,1),(90,26,24,1),(91,26,25,1),(92,26,27,1),(93,26,18,1),(94,26,17,2),(95,26,2,1),(96,27,18,1),(97,27,28,1),(98,27,21,1),(99,27,25,1),(100,27,26,1),(101,28,27,1),(102,28,20,1),(103,28,19,1),(104,8,17,3),(105,13,8,2),(106,10,8,2),(107,7,17,2),(108,17,7,2);
/*!40000 ALTER TABLE `pointroad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relation`
--

DROP TABLE IF EXISTS `relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relation` (
  `relationid` int(11) NOT NULL AUTO_INCREMENT,
  `firstfamily` int(11) NOT NULL,
  `secondfamily` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `Coefficient` double DEFAULT '0',
  PRIMARY KEY (`relationid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relation`
--

LOCK TABLES `relation` WRITE;
/*!40000 ALTER TABLE `relation` DISABLE KEYS */;
INSERT INTO `relation` VALUES (1,2,8,-5,-0.2),(2,5,6,-5,-0.1),(3,3,9,-5,-0.5),(4,4,7,-5,-0.1),(5,2,9,20,0.1),(6,2,6,5,0),(7,3,4,20,0),(8,5,8,15,0.5);
/*!40000 ALTER TABLE `relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `singlevalue`
--

DROP TABLE IF EXISTS `singlevalue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `singlevalue` (
  `SingleValueID` int(11) NOT NULL AUTO_INCREMENT,
  `SingleValueName` varchar(60) DEFAULT NULL,
  `SingleValueString` varchar(60) DEFAULT '_',
  `SingleValueINT` int(11) DEFAULT '0',
  `SingleValueDec` varchar(45) DEFAULT '__',
  PRIMARY KEY (`SingleValueID`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `singlevalue`
--

LOCK TABLES `singlevalue` WRITE;
/*!40000 ALTER TABLE `singlevalue` DISABLE KEYS */;
INSERT INTO `singlevalue` VALUES (1,'ElectionStartMonth','_',6,'竞选开始月'),(2,'ElectionEndMonth','_',9,'竞选结束月'),(3,'FamilyCashDrainOfRound','_',-100,'家族每回合的金钱损耗'),(4,'FamilyCreditXDrainOfRound','_',-5,'家族不处理政治的元老声望惩罚'),(5,'ElectionSeniorCreditYCost','_',30,'竞选元老所需要的公民声望'),(6,'ElectionUmpireCreditYCost','_',55,'竞选裁判官所需要的公民声望'),(7,'MaxNearDistance','_',2,'临近地理位置判断的最大距离'),(8,'TradeValueChangeMonthCD','_',3,'贸易浮动时间CD'),(9,'CropAccountMonth','_',7,'地产结账月份'),(10,'WarWastagePercentWinner','_',30,'战争胜利者折损率'),(11,'WarWastagePercentLoser','_',60,'战争失败者折损率'),(12,'WarWastagePercentWinnerRndMin','_',10,'战争胜利者折损随机区间，最小值'),(13,'WarWastagePercentWinnerRndMax','_',20,'战争胜利者折损随机区间，最大值'),(14,'WarWastagePercentLoserRndMin','_',25,'战争胜利者折损随机区间，最小值'),(15,'WarWastagePercentLoserRndMax','_',35,'战争胜利者折损随机区间，最大值'),(16,'WarArmyNullPercent','_',30,'战争军队胜负存活率'),(17,'StrMessageBoxAssartUpdateTitle','地产更新',0,'消息盒子中的地产更新信息的标题'),(19,'StrMessageBoxAssartUpdate','地产已开垦完毕',0,'消息盒子中的地产更新信息的内容'),(20,'StrMessageBoxEcoCrisisTitle','经济危机',0,'消息盒子中,出现经济危机时的信息标题'),(21,'StrMessageBoxEcoCrisis','经济出现危机，开始变卖地产',0,'消息盒子中,出现经济危机时的信息内容'),(22,'StrPanelNoteEcoCrisis','经济出现危机,点击地产选择售卖',0,'当出现经济危机时,经济面板中的地产通知信息'),(23,'StrMessageBoxElecRes','竞选结果',0,'竞选结束，消息盒子标题'),(24,'StrMessageBoxElecResSuccess','，竞选成功了',0,'竞选结束，成功结果消息，逗号前为人名，和职务'),(25,'StrMessageBoxElecResFailed','，竞选失败了',0,'竞选结束，失败结果消息，逗号前为人名，和职务'),(26,'EcoCountryTaxMonth','_',9,'收税月'),(27,'MessageSmallBuyCreditYNoMoney','没钱收买',0,'收买公民声望,钱不足的提示信息'),(28,'MessageSmallBuyCreditY200','请群众吃了顿好的，公民好感显著提升了',0,'花费200,购买公民声望'),(29,'MessageSmallPickCreditY','假惺惺的做了些事，稍微提升了公民的好感',0,'捡声望'),(30,'NotePanelArgueCostNotEnough','费用不够，请重新出牌！',0,'出牌时，费用不够的提示信息'),(31,'SystemNoteMessageTitle','系统消息',0,'系统消息标题'),(32,'SystemNoteMessageChoseSenate','游戏初始,需要选择一名家族成员,成为元老',0,'初始化提示信息内容'),(33,'ProletariatUpdateCd','_',3,'无产阶级数目增加的CD'),(34,'ProletariatUpdateCount','_',1000,'无产阶级每次自增的数目'),(35,'GrainConsumeMonth','_',6,'国家发粮的月份'),(36,'StrMessageBoxCountryNoteTitle','国家消息',0,'国家类通知信息的标题'),(37,'StrMessageBoxGrainConsume','国家按小麦法,对无产游民发放了粮食',0,'粮食消耗的通知信息'),(38,'StrMessageBoxGrainConsumeNotEnough','国家的粮食,不够提供给无产游民',0,'粮食消耗储备不够的信息'),(39,'GrainBuyMonth','_',3,'国家买粮的月份'),(40,'GrainBuyValue','_',1,'国家买粮的单价'),(41,'StrMessageBoxBuyGrain','国家购买了足够的粮食',0,'国家买粮信息'),(42,'StrMessageBoxBuyGrainNoMoney','国家的资金不够买足够的粮食',0,'国家买粮没钱'),(43,'ArmyMilitaryPayMonth','_',12,'国家发军饷的月份'),(44,'StrMessageBoxPayArmyNoMoney','国家资金不够发军饷',0,'国家发军饷没钱'),(45,'StrMessageBoxPayArmy','国家发给了所有军队军饷',0,'国家发军饷信息'),(46,'ShipConsumeMonth','_',12,'__'),(47,'ShipConsumeValue','_',1000,'船队每年的损失金额'),(48,'StrMessageBoxPayShipNoMoney','国家没有闲钱发给船队',0,'国家没钱给船队'),(49,'StrMessageBoxPayShip','国家为船队支付了船队消耗',0,'国家支付船队消耗信息'),(50,'ElectionConsualCreditYCost','_',80,'竞选执政官所需的公众声望'),(51,'StrMessageBoxNewOfficialWork','新的官员开始上任工作',0,'新一届的官员上台提示信息'),(52,'StrMessageBoxElecNote','每年一度的，元老竞选开始了，每个竞选人有3个月的准备时间！',0,'竞选周期,提示'),(53,'LabelTextMotionNote','选择支持已提出的提案，或者提出自己的提案',0,'提案界面的提示信息'),(54,'LabelTextPassByConsul','是否使用20点政治声望，强行通过法案',0,'执政官强制通过的提示信息'),(55,'PassByConsulCreditXValue','_',20,'执政官强行通过法案的政治声望消耗'),(56,'MediationCharmValue','_',1,'使者调停谈判，魅力相关系数'),(57,'MediationFullSuccessedValue','_',1,'使者调停谈判，国家完全成功系数'),(58,'StrMessageBoxMediationSuccessed',',谈判成功了',0,'谈判成功信息,逗号前为地区名,使者名'),(59,'StrMessageBoxMediationFailDead',',,谈判失败了,使者死亡了',0,'谈判失败死亡信息,逗号前为地区名,使者名'),(60,'StrMessageBoxMediationFail',',,谈判失败了',0,'谈判失败信息,逗号前为地区名,使者名'),(61,'StrMessageBoxOtherCountryChangeUmpire',',在争夺中，获得胜利，成为了国王。',0,'皇嗣争夺，胜利者信息，逗号前为胜利者名'),(62,'StrMessageBoxOtherCountryStayUmpire','争斗平息，该地区，势力未发生变化',0,'皇嗣争夺，调停成功'),(63,'FoundMediationPercent','_',70,'触发请求调停的兵力存活率'),(64,'StrMessageBoxOtherSenNoteTitle','国家决策',0,'处理事件的提示标题'),(65,'LabelTextMainInterfaceMember','成员',0,'__'),(66,'LabelTextMainInterfaceCX','元老声望',0,'__'),(67,'LabelTextMainInterfaceCY','公众声望',0,'__'),(68,'LabelTextFurBarFTotal','竞选',0,'__'),(69,'LabelTextFurBarFEconomy','经济管理',0,'__'),(70,'LabelTextFurBarFMember','人事管理',0,'__'),(71,'LabelTextFurBarFSenate','政治',0,'__'),(72,'LabelTextFurBarFContact','家族交际',0,'__'),(73,'LabelTextFurBarFBoardcast','消息箱',0,'__'),(74,'LabelTextUIWarName','国家',0,'__'),(75,'LabelTextUIWarLeader','指挥官',0,'__'),(76,'LabelTextUIWarForce','魄力',0,'__'),(113,'LabelTextUIWarArmysCount','军团数',0,'__'),(114,'LabelTextUIWarTotalCount','总兵力',0,'__'),(115,'LabelTextUIWarCurrentCount','现存兵力',0,'__'),(116,'LabelTextUIWarLabel','本回合结算',0,'__'),(117,'LabelTextUIWarAttackValue','总攻击力',0,'__'),(118,'LabelTextUIWarDead','战死',0,'__'),(119,'LabelTextUIWarNullArmy','损失军队',0,'__'),(120,'LabelTextUIWarState','优势',0,'__'),(121,'LabelTextUIS_TotalNote','当前不是竞选时间',0,'__'),(122,'LabelTextUIS_TotalLabelTip','选择一名成员，参与竞选。',0,'__'),(123,'LabelTextUIS_TotalLabelTipLabel','选择长老，则竞选裁判官。\\n\n否则为竞选长老',0,'__'),(124,'LabelTextUIS_TotalDashBuy1','买公民声望',0,'__'),(125,'LabelTextUIS_TotalDashPick1','捡公民声望',0,'__'),(126,'LabelTextUIS_EconomyTrading','贸易',0,'__'),(127,'LabelTextUIS_EconomyEstate','地产',0,'__'),(128,'LabelTextUIS_MemberNoteChioseSenior','选择一名成员，成为元老！',0,'__'),(129,'LabelTextUIS_MemberNoteChioseSeniorChild','家族中有一名[00eeee]睿智[-]且具有[00ff00]魅力[-]的人来作为元老参与元老院的政事',0,'__'),(130,'LabelTextUIS_SenateP1','选择一个事件进行处理。',0,'__'),(131,'LabelTextUIS_SenateP2','选择，您家族的一名元老参与\n提案。',0,'__'),(132,'StrMessageBoxTradeComplete','贸易成功',0,'贸易结束消息'),(133,'SlaveOfBattleArmyCountMax','_',5,'奴隶暴动军团数上限'),(134,'SlaveOfBattleArmyCountMin','_',3,'奴隶暴动军团数下限'),(135,'TriggerSlaveOfBattleProb','_',15,'自然灾害触发奴隶暴动的概率'),(136,'TriggerInfringementProb','_',10,'倾向不明触发侵犯公民的概率'),(137,'LabelTextUIS_SenateP3RelAdd','关系好感增益:',0,'__'),(138,'LabelTextUIS_SenateP3CYadd','平民声望增益:',0,'__'),(139,'LabelTextUIS_SenateP3ScoreAdd','政治功绩增益:',0,'__'),(140,'LabelTextUIS_SenateP3CXadd','长老声望增益:',0,'__'),(141,'LabelTextUIS_SenateP3Agree','同意提案',0,'__'),(142,'LawNpoOfHandleValue','_',50,'立案基础的好感度'),(143,'LabelTextUIS_SenateP4EventOneNpo','好感度',0,'__'),(144,'LabelTextUIS_SenateP4EventOneName','提案：',0,'__'),(145,'LabelTextUIS_SenateP4EventOnePerson','人物：',0,'__'),(146,'LabelTextUIS_SenateP4EventOneNature','提案倾向：',0,'__'),(147,'LabelTextUIS_SenateP4EventOneValue','提案难度：',0,'__'),(148,'LabelTextUIS_SenateP4EventOneBrain','智力：',0,'__'),(149,'LabelTextUIS_SenateP4EventOneCharm','魅力：',0,'__'),(150,'LabelTextUIS_SenateP4EventOneHealthTiao','提案难度：',0,'__'),(151,'LabelTextUIS_SenateP4Title','元老会：',0,'__');
/*!40000 ALTER TABLE `singlevalue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `situation`
--

DROP TABLE IF EXISTS `situation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `situation` (
  `SituationId` int(11) NOT NULL AUTO_INCREMENT,
  `SituationName` varchar(45) NOT NULL,
  `AreaIDs` varchar(80) DEFAULT NULL,
  `MotionIDs` varchar(45) DEFAULT NULL,
  `SituationKind` int(11) DEFAULT '0',
  `SituationTrig` tinyint(4) DEFAULT '0',
  `SituationEffect` varchar(45) DEFAULT NULL,
  `SituationWeight` int(11) DEFAULT NULL,
  PRIMARY KEY (`SituationId`),
  UNIQUE KEY `SituationName_UNIQUE` (`SituationName`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `situation`
--

LOCK TABLES `situation` WRITE;
/*!40000 ALTER TABLE `situation` DISABLE KEYS */;
INSERT INTO `situation` VALUES (1,'自然灾害','2,3,17,18,24,25','1,16',1,1,'4|10',6),(2,'叛乱','5,7,8,10,16','1',1,0,'12|5',9),(3,'倾向不明','5,7,8,10,16','1,3,24',1,0,'9',130),(4,'侵犯公民','5,7,8,9,10,11,12,13,14,15,16,19,20,21,23,27','3,23',1,0,'11|5',15),(5,'领土争端','1,5,7,8,9,10,11,12,13,14,15,16,19,20,21,22,23,27,28','1,3,4',1,1,'7',8),(6,'皇嗣纠纷','5,7,8,9,10,12,14,16,20','1,20',1,0,'1|5',10),(7,'贸易封锁','8,11,13,16,27','1,2,3,4',1,1,'5',12),(8,'船队损失','26','1,16',1,1,'0',11),(9,'奴隶暴动','2,3,4,6,24','1,2,4',1,0,'6|10',18),(10,'入侵','9,11,12,14,15,20,21,22,23','1,4,12,23',1,0,'10',20),(11,'蛮族入侵','20,21,22,23','1,4,12,23',1,0,'8|10',19),(12,'请求调停','','10',0,0,'0',7),(13,'对不支持者的政策','','2',0,0,'0',17),(14,'谋杀',NULL,'2,21',0,0,'0',14),(15,'将军战死',NULL,'2,21',0,0,'0',16),(16,'关系上升',NULL,'0',-1,0,'2',5),(17,'关系下降',NULL,'0',-1,0,'3',4),(18,'请求援军',NULL,'10,11',0,0,'0',3),(19,'深度干预',NULL,'1,2,4',0,0,'0',2),(20,'领土处置',NULL,'7,9',0,0,'13',1),(21,'国外军事行动','9,11,12,14,15,20,21,22,23','0',-3,0,'8',0);
/*!40000 ALTER TABLE `situation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specialty`
--

DROP TABLE IF EXISTS `specialty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specialty` (
  `SpecialtyId` int(11) NOT NULL AUTO_INCREMENT,
  `SpecialtyName` varchar(45) NOT NULL,
  `SpecialtyArea` int(11) NOT NULL,
  `SpecialtyValue` varchar(45) DEFAULT NULL,
  `SpecialtyRnd` int(11) NOT NULL,
  `more` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`SpecialtyId`),
  UNIQUE KEY `SpecialtyName_UNIQUE` (`SpecialtyName`),
  UNIQUE KEY `SpecialtyArea_UNIQUE` (`SpecialtyArea`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specialty`
--

LOCK TABLES `specialty` WRITE;
/*!40000 ALTER TABLE `specialty` DISABLE KEYS */;
INSERT INTO `specialty` VALUES (1,'铁矿',3,'60|100',3,NULL),(2,'染料',9,'80|120',1,NULL),(3,'草纸',8,'100|150',3,NULL),(4,'大理石',10,'100|130',0,NULL),(5,'粮食',6,'40|180',2,NULL),(6,'丝绸',11,'200|400',5,NULL);
/*!40000 ALTER TABLE `specialty` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-05 18:20:07
