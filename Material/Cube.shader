﻿Shader "Custom/Cube" {
	Properties {
		_Color("Color Tint",Color)=(1.0,1.0,1.0,1.0)
		_Line("Line Tint",Color)=(0,0,0,0)
	}
	SubShader {
		PASS{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			fixed4 _Color;
			fixed4 _Line;
			struct v2f{
				float4 vertex:POSITION;
				float4 NdotV:COLOR;
			};
			struct a2v{
				float4 ve:POSITION;
				float4 n:NORMAL;
			};
			v2f vert(a2v AV){
				v2f o;
				o.vertex=mul(UNITY_MATRIX_MVP,AV.ve);
				float3 V=WorldSpaceViewDir(AV.ve);
				o.NdotV.x=saturate(dot(AV.n,normalize(V)));
				return o;
			}
			fixed4 frag(v2f INP):SV_Target{
				fixed4 c=(1.0,1.0,1.0,1.0);
				c.rgb*=_Color.rgb;
				c.rgb+=pow((1-INP.NdotV.x),1)*_Line.rgb;
				return c;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
